package com.bmi.datafix.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.*;
import com.bmi.datafix.repository.*;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/kecamatan/longlat")
public class KecamatanLongLat {
    @Autowired
    private KecamatanLongLatRepository kecamatanLongLatRepository;

    @GetMapping("/byidkecamatan/{idkecamatan}")
    public List<com.bmi.datafix.entity.KecamatanLongLat> getOneIdkecamatanLongLat(@PathVariable String idkecamatan) {
        return kecamatanLongLatRepository.findByIdkecamatan(idkecamatan);
    }
}
