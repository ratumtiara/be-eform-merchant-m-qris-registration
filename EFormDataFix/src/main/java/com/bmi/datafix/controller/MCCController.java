package com.bmi.datafix.controller;

import com.bmi.datafix.entity.*;
import com.bmi.datafix.repository.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/mqris/datafix")
public class MCCController {

    @Autowired
    private MCCRepository mccRepository;

    @GetMapping("/mcc/getAll")
    public ResponseEntity<List<MCC>> getAllmccs(@RequestParam(required = false) String ListMcc) {
        try {
            List<MCC> mccs = new ArrayList<MCC>();
            mccRepository.findAll().forEach(mccs::add);
            return new ResponseEntity<>(mccs, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}