package com.bmi.datafix.entity;

import javax.persistence.*;
@Entity
@Table(name ="data_kecamatan_longlat")
public class KecamatanLongLat {
    @Id
    @Column(name = "idkecamatan")
    private String idkecamatan;

    @Column(name = "alt_name")
    private String alt_name;

    @Column(name = "latitude")
    private String latitude;

    @Column(name = "longitude")
    private String longitude;

    @Column(name = "name")
    private String name;

    @Column(name = "regency_id")
    private String regency_id;

    public String getIdkecamatan() {
        return this.idkecamatan;
    }

    public void setIdkecamatan(String id) {
        this.idkecamatan = id;
    }

    public String getAlt_name() {
        return this.alt_name;
    }

    public void setAlt_name(String alt_name) {
        this.alt_name = alt_name;
    }

    public String getLatitude() {
        return this.latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return this.longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegency_id() {
        return this.regency_id;
    }

    public void setRegency_id(String regency_id) {
        this.regency_id = regency_id;
    }

    public KecamatanLongLat() {
    }

    public KecamatanLongLat(String idkecamatan, String alt_name, String latitude, String longitude, String name, String regency_id) {
        this.idkecamatan = idkecamatan;
        this.alt_name = alt_name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
        this.regency_id = regency_id;
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getIdkecamatan() + "'" +
            ", alt_name='" + getAlt_name() + "'" +
            ", latitude='" + getLatitude() + "'" +
            ", longitude='" + getLongitude() + "'" +
            ", name='" + getName() + "'" +
            ", regency_id='" + getRegency_id() + "'" +
            "}";
    }

}