package com.bmi.datafix.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "form_mcc")
public class MCC {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "kode_mcc")
    private String kode_mcc;

    @Column(name = "keterangan_mcc")
    private String keterangan_mcc;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKode_mcc() {
        return this.kode_mcc;
    }

    public void setKode_mcc(String kode_mcc) {
        this.kode_mcc = kode_mcc;
    }

    public String getKeterangan_mcc() {
        return this.keterangan_mcc;
    }

    public void setKeterangan_mcc(String keterangan_mcc) {
        this.keterangan_mcc = keterangan_mcc;
    }

    public MCC() {
    }

    public MCC(Long id, String kode_mcc, String keterangan_mcc) {
        this.id = id;
        this.kode_mcc = kode_mcc;
        this.keterangan_mcc = keterangan_mcc;
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", kode_mcc='" + getKode_mcc() + "'" +
            ", keterangan_mcc='" + getKeterangan_mcc() + "'" +
            "}";
    }

}
