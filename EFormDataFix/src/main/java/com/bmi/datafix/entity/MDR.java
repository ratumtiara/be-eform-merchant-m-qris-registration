package com.bmi.datafix.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "form_mdr")
public class MDR {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "klasifikasi")
    private String klasifikasi;

    @Column(name = "mdr")
    private String mdr;


    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKlasifikasi() {
        return this.klasifikasi;
    }

    public void setKlasifikasi(String klasifikasi) {
        this.klasifikasi = klasifikasi;
    }

    public String getMdr() {
        return this.mdr;
    }

    public void setMdr(String mdr) {
        this.mdr = mdr;
    }

    public MDR() {
    }

    public MDR(Long id, String klasifikasi, String mdr) {
        this.id = id;
        this.klasifikasi = klasifikasi;
        this.mdr = mdr;
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", klasifikasi='" + getKlasifikasi() + "'" +
            ", mdr='" + getMdr() + "'" +
            "}";
    }
 

}
