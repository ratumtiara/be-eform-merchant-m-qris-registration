package com.bmi.datafix.repository;

import java.util.List;

import com.bmi.datafix.entity.KecamatanLongLat;

import org.springframework.data.jpa.repository.JpaRepository;

public interface KecamatanLongLatRepository extends JpaRepository<KecamatanLongLat, Long> {
    List<KecamatanLongLat> findByIdkecamatan(String idkecamatan);
}
