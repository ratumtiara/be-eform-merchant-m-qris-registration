package com.bmi.datafix.repository;

import com.bmi.datafix.entity.*;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MCCRepository extends JpaRepository<MCC, Long> {

}
