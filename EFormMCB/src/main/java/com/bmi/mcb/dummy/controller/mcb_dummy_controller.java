package com.bmi.mcb.dummy.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.*;

import com.bmi.mcb.dummy.entity.*;
import com.bmi.mcb.dummy.repository.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/mqris/mcb")
public class mcb_dummy_controller {
    @Autowired
    private sttm_cust_account_repo Sttm_cust_account_repo;
    @Autowired
    private sttm_cust_personal_repo Sttm_cust_personal_repo;
    @Autowired
    private sttm_cust_customer_repo Sttm_customer_repo;

    @GetMapping("/norek/{id}")
    public List<sttm_cust_account> getOneNoRek(@PathVariable String id) {
        return Sttm_cust_account_repo.findByNoRek(id);
    }

    @GetMapping("/nocif/{id}")
    public List<sttm_cust_personal> getOneCif(@PathVariable String id) {
        return Sttm_cust_personal_repo.findByCif(id);
    }

    @GetMapping("/nocif_dua/{id}")
    public List<sttm_customer> getOneCifdua(@PathVariable String id) {
        return Sttm_customer_repo.findByCif(id);
    }
}