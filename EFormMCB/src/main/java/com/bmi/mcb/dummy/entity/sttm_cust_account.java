package com.bmi.mcb.dummy.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import java.math.BigDecimal;

@Entity
@Table(name = "sttm_cust_account",schema = "public")
public class sttm_cust_account {
  @Id
  private String cust_ac_no;
  
  private String branch_code;
  private String ac_desc;
  private String cust_no;
  private String ccy;
  private String account_class;
  private Character ac_stat_no_dr;
  private Character ac_stat_no_cr;
  private Character ac_stat_block;
  private  Character ac_stat_stop_pay;
  private  Character ac_stat_dormant;
  private  Character joint_ac_indicator;
  private Date ac_open_date;
  private BigDecimal ac_stmt_day;
  private  Character ac_stmt_cycle;
  private String alt_ac_no;
  private  Character cheque_book_facility;
  private  Character atm_facility;
  private  Character passbook_facility;
  private  Character ac_stmt_type;
  private String dr_ho_line;
  private String cr_ho_line;
  private String cr_cb_line;
  private String dr_cb_line;
  private BigDecimal sublimit;
  private BigDecimal uncoll_funds_limit;
  private  Character ac_stat_frozen;
  private Date previous_statement_date;
  private BigDecimal previous_statement_balance;
  private BigDecimal previous_statement_no;
  private Date tod_limit_start_date;
  private Date tod_limit_end_date;
  private BigDecimal tod_limit;
  private String nominee1;
  private String nominee2;
  private String dr_gl;
  private String cr_gl;
  private  Character record_stat;
  private  Character auth_stat;
  private BigDecimal mod_no;
  private String maker_id;
  private Date maker_dt_stamp;
  private String checker_id;
  private Date checker_dt_stamp;
  private  Character once_auth;
  private String limit_ccy;
  private  Character line_id;
  private BigDecimal offline_limit;
  private  Character cas_account;
  private BigDecimal acy_opening_bal;
  private BigDecimal lcy_opening_bal;
  private BigDecimal acy_today_tover_dr;
  private BigDecimal lcy_today_tover_dr;
  private BigDecimal acy_today_tover_cr;
  private BigDecimal lcy_today_tover_cr;
  private BigDecimal acy_tank_cr;
  private BigDecimal acy_tank_dr;
  private BigDecimal lcy_tank_cr;
  private BigDecimal lcy_tank_dr;
  private BigDecimal acy_tover_cr;
  private BigDecimal lcy_tover_cr;
  private BigDecimal acy_tank_uncollected;
  private BigDecimal acy_curr_balance;
  private BigDecimal lcy_curr_balance;
  private BigDecimal acy_blocked_amount;
  private BigDecimal acy_avl_bal;
  private BigDecimal acy_unauth_dr;
  private BigDecimal acy_unauth_tank_dr;
  private BigDecimal acy_unauth_cr;
  private BigDecimal acy_unauth_tank_cr;
  private BigDecimal acy_unauth_uncollected;
  private BigDecimal acy_unauth_tank_uncollected;
  private BigDecimal acy_mtd_tover_dr;
  private BigDecimal lcy_mtd_tover_dr;
  private BigDecimal acy_mtd_tover_cr;
  private BigDecimal lcy_mtd_tover_cr;
  private BigDecimal acy_accrued_dr_ic;
  private BigDecimal acy_accrued_cr_ic;
  private Date date_last_cr_activity;
  private Date date_last_dr_activity;
  private Date date_last_dr;
  private Date date_last_cr;
  private BigDecimal acy_uncollected;
  private Date tod_start_date;
  private Date tod_end_date;
  private Date dormancy_date;
  private BigDecimal dormancy_days;
  private  Character has_tov;
  private Date last_ccy_conv_date;
  private String address1;
  private String address2;
  private String address3;
  private String address4;
  private String type_of_chq;
  private String atm_cust_ac_no;
  private BigDecimal atm_dly_amt_limit;
  private BigDecimal atm_dly_count_limit;
  private  Character gen_stmt_only_on_mvmt;
  private  Character ac_stat_de_post;
  private String display_iban_in_advices;
  private String clearing_bank_code;
  private String clearing_ac_no;
  private String iban_ac_no;
  private String reg_cc_availability;
  private BigDecimal reg_cc_available_funds;
  private BigDecimal prev_ac_srno_printed_in_pbk;
  private BigDecimal latest_srno_submitted;
  private BigDecimal prev_runbal_printed_in_pbk;
  private BigDecimal latest_runbal_submmited;
  private BigDecimal prev_page_no;
  private BigDecimal prev_line_no;
  private String mt210_reqd;
  private  Character acc_stmt_type2;
  private BigDecimal acc_stmt_day2;
  private  Character ac_stmt_cycle2;
  private Date previous_statement_date2;
  private BigDecimal previous_statement_balance2;
  private BigDecimal previous_statement_no2;
  private  Character gen_stmt_only_on_mvmt2;
  private  Character acc_stmt_type3;
  private BigDecimal acc_stmt_day3;
  private  Character ac_stmt_cycle3;
  private Date previous_statement_date3;
  private BigDecimal previous_statement_balance3;
  private BigDecimal previous_statement_no3;
  private  Character gen_stmt_only_on_mvmt3;
  private BigDecimal sweep_type;
  private String master_account_no;
  private BigDecimal auto_deposits_bal;
  private String cas_customer;
  private String account_type;
  private BigDecimal min_reqd_bal;
  private  Character positive_pay_ac;
  private BigDecimal stale_days;
  private BigDecimal cr_auto_ex_rate_lmt;
  private BigDecimal dr_auto_ex_rate_lmt;
  private String track_receivable;
  private BigDecimal receivable_amount;
  private String product_list;
  private String txn_code_list;
  private String special_condition_product;
  private String special_condition_txncode;
  private  Character reg_d_applicable;
  private String regd_periodicity;
  private Date regd_start_date;
  private Date regd_end_date;
  private  Character td_cert_printed;
  private String checkbook_name_1;
  private String checkbook_name_2;
  private  Character auto_reorder_check_required;
  private BigDecimal auto_reorder_check_level;
  private BigDecimal auto_reorder_check_leaves;
  private String netting_required;
  private String referral_required;
  private String lodgement_book_facility;
  private String acc_status;
  private Date status_since;
  private String inherit_reporting;
  private Date overdraft_since;
  private Date prev_ovd_date;
  private String status_change_automatic;
  private Date overline_od_since;
  private Date tod_since;
  private Date prev_tod_since;
  private String dormant_param;
  private BigDecimal dr_int_due;
  private String excl_sameday_rvrtrns_fm_stmt;
  private  Character allow_back_period_entry;
  private String auto_prov_reqd;
  private String exposure_category;
  private BigDecimal risk_free_exp_amount;
  private BigDecimal provision_amount;
  private BigDecimal credit_txn_limit;
  private Date cr_lm_start_date;
  private Date cr_lm_rev_date;
  private String statement_account;
  private String account_derived_status;
  private String prov_ccy_type;
  private BigDecimal chg_due;
  private BigDecimal withdrawable_uncolled_fund;
  private String defer_recon;
  private String consolidation_reqd;
  private String funding;
  private String funding_branch;
  private String funding_account;
  private String mod9_validation_reqd;
  private BigDecimal validation_digit;
  //private String location;
  private String media;
  private String acc_tanked_stat;
  private String gen_interim_stmt;
  private String gen_interim_stmt_on_mvmt;
  private String gen_balance_report;
  private String interim_report_since;
  private String interim_report_type;
  private String balance_report_since;
  private String balance_report_type;
  private BigDecimal interim_debit_amt;
  private BigDecimal interim_credit_amt;
  private BigDecimal interim_stmt_day_count;
  private BigDecimal interim_stmt_ytd_count;
  private  Character mode_of_operation;
  private BigDecimal inf_acc_opening_amt;
  private String inf_pay_in_option;
  private String inf_offset_branch;
  private String inf_offset_account;
  private String inf_waive_acc_open_charge;
  private BigDecimal daylight_limit_amount;
  private String trnover_lmt_code;
  private BigDecimal passbook_numeric;
  private String country_code;
  private String consol_chg_acc;
  private  Character escrow_transfer;
  private String escrow_branch_code;
  private String escrow_ac_no;
  private BigDecimal escrow_percentage;
  private BigDecimal sod_notification_percent;
  private String salary_account;
  private  Character repl_cust_sig;
  private BigDecimal max_no_cheque_rejections;
  private BigDecimal no_cheque_rejections;
  private String fund_id;
  private String linked_dep_branch;
  private String linked_dep_acc;
  private String mudarabah_accounts;
  private String zakat_exemption;
  private String account_auto_closed;
  private String consol_chg_brn;
  private Date no_of_chq_rej_reset_on;
  private String crs_stat_reqd;
  private  Character contribute_to_pdm;
  private  Character exclude_from_distribution;
  private  Character ac_set_close;
  private Date ac_set_close_date;
  private String project_account;
  private BigDecimal acy_sweep_ineligible;
  private String mt110_recon_reqd;
  private String sweep_required;
  private String auto_deposit;
  private String sweep_out;
  private String auto_create_pool;
  private Date last_purge_date;
  private String default_waiver;
  private  Character auto_dc_request;
  private  Character auto_chqbk_request;
  private  Character nsf_blacklist_status;
  private String nsf_auto_update;
  private String intermediary_required;

    public String getCust_ac_no() {
        return this.cust_ac_no;
    }

    public void setCust_ac_no(String cust_ac_no) {
        this.cust_ac_no = cust_ac_no;
    }

    public String getBranch_code() {
        return this.branch_code;
    }

    public void setBranch_code(String branch_code) {
        this.branch_code = branch_code;
    }

    public String getAc_desc() {
        return this.ac_desc;
    }

    public void setAc_desc(String ac_desc) {
        this.ac_desc = ac_desc;
    }

    public String getCust_no() {
        return this.cust_no;
    }

    public void setCust_no(String cust_no) {
        this.cust_no = cust_no;
    }

    public String getCcy() {
        return this.ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getAccount_class() {
        return this.account_class;
    }

    public void setAccount_class(String account_class) {
        this.account_class = account_class;
    }

    public Character getAc_stat_no_dr() {
        return this.ac_stat_no_dr;
    }

    public void setAc_stat_no_dr(Character ac_stat_no_dr) {
        this.ac_stat_no_dr = ac_stat_no_dr;
    }

    public Character getAc_stat_no_cr() {
        return this.ac_stat_no_cr;
    }

    public void setAc_stat_no_cr(Character ac_stat_no_cr) {
        this.ac_stat_no_cr = ac_stat_no_cr;
    }

    public Character getAc_stat_block() {
        return this.ac_stat_block;
    }

    public void setAc_stat_block(Character ac_stat_block) {
        this.ac_stat_block = ac_stat_block;
    }

    public Character getAc_stat_stop_pay() {
        return this.ac_stat_stop_pay;
    }

    public void setAc_stat_stop_pay(Character ac_stat_stop_pay) {
        this.ac_stat_stop_pay = ac_stat_stop_pay;
    }

    public Character getAc_stat_dormant() {
        return this.ac_stat_dormant;
    }

    public void setAc_stat_dormant(Character ac_stat_dormant) {
        this.ac_stat_dormant = ac_stat_dormant;
    }

    public Character getJoint_ac_indicator() {
        return this.joint_ac_indicator;
    }

    public void setJoint_ac_indicator(Character joint_ac_indicator) {
        this.joint_ac_indicator = joint_ac_indicator;
    }

    public Date getAc_open_date() {
        return this.ac_open_date;
    }

    public void setAc_open_date(Date ac_open_date) {
        this.ac_open_date = ac_open_date;
    }

    public BigDecimal getAc_stmt_day() {
        return this.ac_stmt_day;
    }

    public void setAc_stmt_day(BigDecimal ac_stmt_day) {
        this.ac_stmt_day = ac_stmt_day;
    }

    public Character getAc_stmt_cycle() {
        return this.ac_stmt_cycle;
    }

    public void setAc_stmt_cycle(Character ac_stmt_cycle) {
        this.ac_stmt_cycle = ac_stmt_cycle;
    }

    public String getAlt_ac_no() {
        return this.alt_ac_no;
    }

    public void setAlt_ac_no(String alt_ac_no) {
        this.alt_ac_no = alt_ac_no;
    }

    public Character getCheque_book_facility() {
        return this.cheque_book_facility;
    }

    public void setCheque_book_facility(Character cheque_book_facility) {
        this.cheque_book_facility = cheque_book_facility;
    }

    public Character getAtm_facility() {
        return this.atm_facility;
    }

    public void setAtm_facility(Character atm_facility) {
        this.atm_facility = atm_facility;
    }

    public Character getPassbook_facility() {
        return this.passbook_facility;
    }

    public void setPassbook_facility(Character passbook_facility) {
        this.passbook_facility = passbook_facility;
    }

    public Character getAc_stmt_type() {
        return this.ac_stmt_type;
    }

    public void setAc_stmt_type(Character ac_stmt_type) {
        this.ac_stmt_type = ac_stmt_type;
    }

    public String getDr_ho_line() {
        return this.dr_ho_line;
    }

    public void setDr_ho_line(String dr_ho_line) {
        this.dr_ho_line = dr_ho_line;
    }

    public String getCr_ho_line() {
        return this.cr_ho_line;
    }

    public void setCr_ho_line(String cr_ho_line) {
        this.cr_ho_line = cr_ho_line;
    }

    public String getCr_cb_line() {
        return this.cr_cb_line;
    }

    public void setCr_cb_line(String cr_cb_line) {
        this.cr_cb_line = cr_cb_line;
    }

    public String getDr_cb_line() {
        return this.dr_cb_line;
    }

    public void setDr_cb_line(String dr_cb_line) {
        this.dr_cb_line = dr_cb_line;
    }

    public BigDecimal getSublimit() {
        return this.sublimit;
    }

    public void setSublimit(BigDecimal sublimit) {
        this.sublimit = sublimit;
    }

    public BigDecimal getUncoll_funds_limit() {
        return this.uncoll_funds_limit;
    }

    public void setUncoll_funds_limit(BigDecimal uncoll_funds_limit) {
        this.uncoll_funds_limit = uncoll_funds_limit;
    }

    public Character getAc_stat_frozen() {
        return this.ac_stat_frozen;
    }

    public void setAc_stat_frozen(Character ac_stat_frozen) {
        this.ac_stat_frozen = ac_stat_frozen;
    }

    public Date getPrevious_statement_date() {
        return this.previous_statement_date;
    }

    public void setPrevious_statement_date(Date previous_statement_date) {
        this.previous_statement_date = previous_statement_date;
    }

    public BigDecimal getPrevious_statement_balance() {
        return this.previous_statement_balance;
    }

    public void setPrevious_statement_balance(BigDecimal previous_statement_balance) {
        this.previous_statement_balance = previous_statement_balance;
    }

    public BigDecimal getPrevious_statement_no() {
        return this.previous_statement_no;
    }

    public void setPrevious_statement_no(BigDecimal previous_statement_no) {
        this.previous_statement_no = previous_statement_no;
    }

    public Date getTod_limit_start_date() {
        return this.tod_limit_start_date;
    }

    public void setTod_limit_start_date(Date tod_limit_start_date) {
        this.tod_limit_start_date = tod_limit_start_date;
    }

    public Date getTod_limit_end_date() {
        return this.tod_limit_end_date;
    }

    public void setTod_limit_end_date(Date tod_limit_end_date) {
        this.tod_limit_end_date = tod_limit_end_date;
    }

    public BigDecimal getTod_limit() {
        return this.tod_limit;
    }

    public void setTod_limit(BigDecimal tod_limit) {
        this.tod_limit = tod_limit;
    }

    public String getNominee1() {
        return this.nominee1;
    }

    public void setNominee1(String nominee1) {
        this.nominee1 = nominee1;
    }

    public String getNominee2() {
        return this.nominee2;
    }

    public void setNominee2(String nominee2) {
        this.nominee2 = nominee2;
    }

    public String getDr_gl() {
        return this.dr_gl;
    }

    public void setDr_gl(String dr_gl) {
        this.dr_gl = dr_gl;
    }

    public String getCr_gl() {
        return this.cr_gl;
    }

    public void setCr_gl(String cr_gl) {
        this.cr_gl = cr_gl;
    }

    public Character getRecord_stat() {
        return this.record_stat;
    }

    public void setRecord_stat(Character record_stat) {
        this.record_stat = record_stat;
    }

    public Character getAuth_stat() {
        return this.auth_stat;
    }

    public void setAuth_stat(Character auth_stat) {
        this.auth_stat = auth_stat;
    }

    public BigDecimal getMod_no() {
        return this.mod_no;
    }

    public void setMod_no(BigDecimal mod_no) {
        this.mod_no = mod_no;
    }

    public String getMaker_id() {
        return this.maker_id;
    }

    public void setMaker_id(String maker_id) {
        this.maker_id = maker_id;
    }

    public Date getMaker_dt_stamp() {
        return this.maker_dt_stamp;
    }

    public void setMaker_dt_stamp(Date maker_dt_stamp) {
        this.maker_dt_stamp = maker_dt_stamp;
    }

    public String getChecker_id() {
        return this.checker_id;
    }

    public void setChecker_id(String checker_id) {
        this.checker_id = checker_id;
    }

    public Date getChecker_dt_stamp() {
        return this.checker_dt_stamp;
    }

    public void setChecker_dt_stamp(Date checker_dt_stamp) {
        this.checker_dt_stamp = checker_dt_stamp;
    }

    public Character getOnce_auth() {
        return this.once_auth;
    }

    public void setOnce_auth(Character once_auth) {
        this.once_auth = once_auth;
    }

    public String getLimit_ccy() {
        return this.limit_ccy;
    }

    public void setLimit_ccy(String limit_ccy) {
        this.limit_ccy = limit_ccy;
    }

    public Character getLine_id() {
        return this.line_id;
    }

    public void setLine_id(Character line_id) {
        this.line_id = line_id;
    }

    public BigDecimal getOffline_limit() {
        return this.offline_limit;
    }

    public void setOffline_limit(BigDecimal offline_limit) {
        this.offline_limit = offline_limit;
    }

    public Character getCas_account() {
        return this.cas_account;
    }

    public void setCas_account(Character cas_account) {
        this.cas_account = cas_account;
    }

    public BigDecimal getAcy_opening_bal() {
        return this.acy_opening_bal;
    }

    public void setAcy_opening_bal(BigDecimal acy_opening_bal) {
        this.acy_opening_bal = acy_opening_bal;
    }

    public BigDecimal getLcy_opening_bal() {
        return this.lcy_opening_bal;
    }

    public void setLcy_opening_bal(BigDecimal lcy_opening_bal) {
        this.lcy_opening_bal = lcy_opening_bal;
    }

    public BigDecimal getAcy_today_tover_dr() {
        return this.acy_today_tover_dr;
    }

    public void setAcy_today_tover_dr(BigDecimal acy_today_tover_dr) {
        this.acy_today_tover_dr = acy_today_tover_dr;
    }

    public BigDecimal getLcy_today_tover_dr() {
        return this.lcy_today_tover_dr;
    }

    public void setLcy_today_tover_dr(BigDecimal lcy_today_tover_dr) {
        this.lcy_today_tover_dr = lcy_today_tover_dr;
    }

    public BigDecimal getAcy_today_tover_cr() {
        return this.acy_today_tover_cr;
    }

    public void setAcy_today_tover_cr(BigDecimal acy_today_tover_cr) {
        this.acy_today_tover_cr = acy_today_tover_cr;
    }

    public BigDecimal getLcy_today_tover_cr() {
        return this.lcy_today_tover_cr;
    }

    public void setLcy_today_tover_cr(BigDecimal lcy_today_tover_cr) {
        this.lcy_today_tover_cr = lcy_today_tover_cr;
    }

    public BigDecimal getAcy_tank_cr() {
        return this.acy_tank_cr;
    }

    public void setAcy_tank_cr(BigDecimal acy_tank_cr) {
        this.acy_tank_cr = acy_tank_cr;
    }

    public BigDecimal getAcy_tank_dr() {
        return this.acy_tank_dr;
    }

    public void setAcy_tank_dr(BigDecimal acy_tank_dr) {
        this.acy_tank_dr = acy_tank_dr;
    }

    public BigDecimal getLcy_tank_cr() {
        return this.lcy_tank_cr;
    }

    public void setLcy_tank_cr(BigDecimal lcy_tank_cr) {
        this.lcy_tank_cr = lcy_tank_cr;
    }

    public BigDecimal getLcy_tank_dr() {
        return this.lcy_tank_dr;
    }

    public void setLcy_tank_dr(BigDecimal lcy_tank_dr) {
        this.lcy_tank_dr = lcy_tank_dr;
    }

    public BigDecimal getAcy_tover_cr() {
        return this.acy_tover_cr;
    }

    public void setAcy_tover_cr(BigDecimal acy_tover_cr) {
        this.acy_tover_cr = acy_tover_cr;
    }

    public BigDecimal getLcy_tover_cr() {
        return this.lcy_tover_cr;
    }

    public void setLcy_tover_cr(BigDecimal lcy_tover_cr) {
        this.lcy_tover_cr = lcy_tover_cr;
    }

    public BigDecimal getAcy_tank_uncollected() {
        return this.acy_tank_uncollected;
    }

    public void setAcy_tank_uncollected(BigDecimal acy_tank_uncollected) {
        this.acy_tank_uncollected = acy_tank_uncollected;
    }

    public BigDecimal getAcy_curr_balance() {
        return this.acy_curr_balance;
    }

    public void setAcy_curr_balance(BigDecimal acy_curr_balance) {
        this.acy_curr_balance = acy_curr_balance;
    }

    public BigDecimal getLcy_curr_balance() {
        return this.lcy_curr_balance;
    }

    public void setLcy_curr_balance(BigDecimal lcy_curr_balance) {
        this.lcy_curr_balance = lcy_curr_balance;
    }

    public BigDecimal getAcy_blocked_amount() {
        return this.acy_blocked_amount;
    }

    public void setAcy_blocked_amount(BigDecimal acy_blocked_amount) {
        this.acy_blocked_amount = acy_blocked_amount;
    }

    public BigDecimal getAcy_avl_bal() {
        return this.acy_avl_bal;
    }

    public void setAcy_avl_bal(BigDecimal acy_avl_bal) {
        this.acy_avl_bal = acy_avl_bal;
    }

    public BigDecimal getAcy_unauth_dr() {
        return this.acy_unauth_dr;
    }

    public void setAcy_unauth_dr(BigDecimal acy_unauth_dr) {
        this.acy_unauth_dr = acy_unauth_dr;
    }

    public BigDecimal getAcy_unauth_tank_dr() {
        return this.acy_unauth_tank_dr;
    }

    public void setAcy_unauth_tank_dr(BigDecimal acy_unauth_tank_dr) {
        this.acy_unauth_tank_dr = acy_unauth_tank_dr;
    }

    public BigDecimal getAcy_unauth_cr() {
        return this.acy_unauth_cr;
    }

    public void setAcy_unauth_cr(BigDecimal acy_unauth_cr) {
        this.acy_unauth_cr = acy_unauth_cr;
    }

    public BigDecimal getAcy_unauth_tank_cr() {
        return this.acy_unauth_tank_cr;
    }

    public void setAcy_unauth_tank_cr(BigDecimal acy_unauth_tank_cr) {
        this.acy_unauth_tank_cr = acy_unauth_tank_cr;
    }

    public BigDecimal getAcy_unauth_uncollected() {
        return this.acy_unauth_uncollected;
    }

    public void setAcy_unauth_uncollected(BigDecimal acy_unauth_uncollected) {
        this.acy_unauth_uncollected = acy_unauth_uncollected;
    }

    public BigDecimal getAcy_unauth_tank_uncollected() {
        return this.acy_unauth_tank_uncollected;
    }

    public void setAcy_unauth_tank_uncollected(BigDecimal acy_unauth_tank_uncollected) {
        this.acy_unauth_tank_uncollected = acy_unauth_tank_uncollected;
    }

    public BigDecimal getAcy_mtd_tover_dr() {
        return this.acy_mtd_tover_dr;
    }

    public void setAcy_mtd_tover_dr(BigDecimal acy_mtd_tover_dr) {
        this.acy_mtd_tover_dr = acy_mtd_tover_dr;
    }

    public BigDecimal getLcy_mtd_tover_dr() {
        return this.lcy_mtd_tover_dr;
    }

    public void setLcy_mtd_tover_dr(BigDecimal lcy_mtd_tover_dr) {
        this.lcy_mtd_tover_dr = lcy_mtd_tover_dr;
    }

    public BigDecimal getAcy_mtd_tover_cr() {
        return this.acy_mtd_tover_cr;
    }

    public void setAcy_mtd_tover_cr(BigDecimal acy_mtd_tover_cr) {
        this.acy_mtd_tover_cr = acy_mtd_tover_cr;
    }

    public BigDecimal getLcy_mtd_tover_cr() {
        return this.lcy_mtd_tover_cr;
    }

    public void setLcy_mtd_tover_cr(BigDecimal lcy_mtd_tover_cr) {
        this.lcy_mtd_tover_cr = lcy_mtd_tover_cr;
    }

    public BigDecimal getAcy_accrued_dr_ic() {
        return this.acy_accrued_dr_ic;
    }

    public void setAcy_accrued_dr_ic(BigDecimal acy_accrued_dr_ic) {
        this.acy_accrued_dr_ic = acy_accrued_dr_ic;
    }

    public BigDecimal getAcy_accrued_cr_ic() {
        return this.acy_accrued_cr_ic;
    }

    public void setAcy_accrued_cr_ic(BigDecimal acy_accrued_cr_ic) {
        this.acy_accrued_cr_ic = acy_accrued_cr_ic;
    }

    public Date getDate_last_cr_activity() {
        return this.date_last_cr_activity;
    }

    public void setDate_last_cr_activity(Date date_last_cr_activity) {
        this.date_last_cr_activity = date_last_cr_activity;
    }

    public Date getDate_last_dr_activity() {
        return this.date_last_dr_activity;
    }

    public void setDate_last_dr_activity(Date date_last_dr_activity) {
        this.date_last_dr_activity = date_last_dr_activity;
    }

    public Date getDate_last_dr() {
        return this.date_last_dr;
    }

    public void setDate_last_dr(Date date_last_dr) {
        this.date_last_dr = date_last_dr;
    }

    public Date getDate_last_cr() {
        return this.date_last_cr;
    }

    public void setDate_last_cr(Date date_last_cr) {
        this.date_last_cr = date_last_cr;
    }

    public BigDecimal getAcy_uncollected() {
        return this.acy_uncollected;
    }

    public void setAcy_uncollected(BigDecimal acy_uncollected) {
        this.acy_uncollected = acy_uncollected;
    }

    public Date getTod_start_date() {
        return this.tod_start_date;
    }

    public void setTod_start_date(Date tod_start_date) {
        this.tod_start_date = tod_start_date;
    }

    public Date getTod_end_date() {
        return this.tod_end_date;
    }

    public void setTod_end_date(Date tod_end_date) {
        this.tod_end_date = tod_end_date;
    }

    public Date getDormancy_date() {
        return this.dormancy_date;
    }

    public void setDormancy_date(Date dormancy_date) {
        this.dormancy_date = dormancy_date;
    }

    public BigDecimal getDormancy_days() {
        return this.dormancy_days;
    }

    public void setDormancy_days(BigDecimal dormancy_days) {
        this.dormancy_days = dormancy_days;
    }

    public Character getHas_tov() {
        return this.has_tov;
    }

    public void setHas_tov(Character has_tov) {
        this.has_tov = has_tov;
    }

    public Date getLast_ccy_conv_date() {
        return this.last_ccy_conv_date;
    }

    public void setLast_ccy_conv_date(Date last_ccy_conv_date) {
        this.last_ccy_conv_date = last_ccy_conv_date;
    }

    public String getAddress1() {
        return this.address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return this.address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return this.address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getAddress4() {
        return this.address4;
    }

    public void setAddress4(String address4) {
        this.address4 = address4;
    }

    public String getType_of_chq() {
        return this.type_of_chq;
    }

    public void setType_of_chq(String type_of_chq) {
        this.type_of_chq = type_of_chq;
    }

    public String getAtm_cust_ac_no() {
        return this.atm_cust_ac_no;
    }

    public void setAtm_cust_ac_no(String atm_cust_ac_no) {
        this.atm_cust_ac_no = atm_cust_ac_no;
    }

    public BigDecimal getAtm_dly_amt_limit() {
        return this.atm_dly_amt_limit;
    }

    public void setAtm_dly_amt_limit(BigDecimal atm_dly_amt_limit) {
        this.atm_dly_amt_limit = atm_dly_amt_limit;
    }

    public BigDecimal getAtm_dly_count_limit() {
        return this.atm_dly_count_limit;
    }

    public void setAtm_dly_count_limit(BigDecimal atm_dly_count_limit) {
        this.atm_dly_count_limit = atm_dly_count_limit;
    }

    public Character getGen_stmt_only_on_mvmt() {
        return this.gen_stmt_only_on_mvmt;
    }

    public void setGen_stmt_only_on_mvmt(Character gen_stmt_only_on_mvmt) {
        this.gen_stmt_only_on_mvmt = gen_stmt_only_on_mvmt;
    }

    public Character getAc_stat_de_post() {
        return this.ac_stat_de_post;
    }

    public void setAc_stat_de_post(Character ac_stat_de_post) {
        this.ac_stat_de_post = ac_stat_de_post;
    }

    public String getDisplay_iban_in_advices() {
        return this.display_iban_in_advices;
    }

    public void setDisplay_iban_in_advices(String display_iban_in_advices) {
        this.display_iban_in_advices = display_iban_in_advices;
    }

    public String getClearing_bank_code() {
        return this.clearing_bank_code;
    }

    public void setClearing_bank_code(String clearing_bank_code) {
        this.clearing_bank_code = clearing_bank_code;
    }

    public String getClearing_ac_no() {
        return this.clearing_ac_no;
    }

    public void setClearing_ac_no(String clearing_ac_no) {
        this.clearing_ac_no = clearing_ac_no;
    }

    public String getIban_ac_no() {
        return this.iban_ac_no;
    }

    public void setIban_ac_no(String iban_ac_no) {
        this.iban_ac_no = iban_ac_no;
    }

    public String getReg_cc_availability() {
        return this.reg_cc_availability;
    }

    public void setReg_cc_availability(String reg_cc_availability) {
        this.reg_cc_availability = reg_cc_availability;
    }

    public BigDecimal getReg_cc_available_funds() {
        return this.reg_cc_available_funds;
    }

    public void setReg_cc_available_funds(BigDecimal reg_cc_available_funds) {
        this.reg_cc_available_funds = reg_cc_available_funds;
    }

    public BigDecimal getPrev_ac_srno_printed_in_pbk() {
        return this.prev_ac_srno_printed_in_pbk;
    }

    public void setPrev_ac_srno_printed_in_pbk(BigDecimal prev_ac_srno_printed_in_pbk) {
        this.prev_ac_srno_printed_in_pbk = prev_ac_srno_printed_in_pbk;
    }

    public BigDecimal getLatest_srno_submitted() {
        return this.latest_srno_submitted;
    }

    public void setLatest_srno_submitted(BigDecimal latest_srno_submitted) {
        this.latest_srno_submitted = latest_srno_submitted;
    }

    public BigDecimal getPrev_runbal_printed_in_pbk() {
        return this.prev_runbal_printed_in_pbk;
    }

    public void setPrev_runbal_printed_in_pbk(BigDecimal prev_runbal_printed_in_pbk) {
        this.prev_runbal_printed_in_pbk = prev_runbal_printed_in_pbk;
    }

    public BigDecimal getLatest_runbal_submmited() {
        return this.latest_runbal_submmited;
    }

    public void setLatest_runbal_submmited(BigDecimal latest_runbal_submmited) {
        this.latest_runbal_submmited = latest_runbal_submmited;
    }

    public BigDecimal getPrev_page_no() {
        return this.prev_page_no;
    }

    public void setPrev_page_no(BigDecimal prev_page_no) {
        this.prev_page_no = prev_page_no;
    }

    public BigDecimal getPrev_line_no() {
        return this.prev_line_no;
    }

    public void setPrev_line_no(BigDecimal prev_line_no) {
        this.prev_line_no = prev_line_no;
    }

    public String getMt210_reqd() {
        return this.mt210_reqd;
    }

    public void setMt210_reqd(String mt210_reqd) {
        this.mt210_reqd = mt210_reqd;
    }

    public Character getAcc_stmt_type2() {
        return this.acc_stmt_type2;
    }

    public void setAcc_stmt_type2(Character acc_stmt_type2) {
        this.acc_stmt_type2 = acc_stmt_type2;
    }

    public BigDecimal getAcc_stmt_day2() {
        return this.acc_stmt_day2;
    }

    public void setAcc_stmt_day2(BigDecimal acc_stmt_day2) {
        this.acc_stmt_day2 = acc_stmt_day2;
    }

    public Character getAc_stmt_cycle2() {
        return this.ac_stmt_cycle2;
    }

    public void setAc_stmt_cycle2(Character ac_stmt_cycle2) {
        this.ac_stmt_cycle2 = ac_stmt_cycle2;
    }

    public Date getPrevious_statement_date2() {
        return this.previous_statement_date2;
    }

    public void setPrevious_statement_date2(Date previous_statement_date2) {
        this.previous_statement_date2 = previous_statement_date2;
    }

    public BigDecimal getPrevious_statement_balance2() {
        return this.previous_statement_balance2;
    }

    public void setPrevious_statement_balance2(BigDecimal previous_statement_balance2) {
        this.previous_statement_balance2 = previous_statement_balance2;
    }

    public BigDecimal getPrevious_statement_no2() {
        return this.previous_statement_no2;
    }

    public void setPrevious_statement_no2(BigDecimal previous_statement_no2) {
        this.previous_statement_no2 = previous_statement_no2;
    }

    public Character getGen_stmt_only_on_mvmt2() {
        return this.gen_stmt_only_on_mvmt2;
    }

    public void setGen_stmt_only_on_mvmt2(Character gen_stmt_only_on_mvmt2) {
        this.gen_stmt_only_on_mvmt2 = gen_stmt_only_on_mvmt2;
    }

    public Character getAcc_stmt_type3() {
        return this.acc_stmt_type3;
    }

    public void setAcc_stmt_type3(Character acc_stmt_type3) {
        this.acc_stmt_type3 = acc_stmt_type3;
    }

    public BigDecimal getAcc_stmt_day3() {
        return this.acc_stmt_day3;
    }

    public void setAcc_stmt_day3(BigDecimal acc_stmt_day3) {
        this.acc_stmt_day3 = acc_stmt_day3;
    }

    public Character getAc_stmt_cycle3() {
        return this.ac_stmt_cycle3;
    }

    public void setAc_stmt_cycle3(Character ac_stmt_cycle3) {
        this.ac_stmt_cycle3 = ac_stmt_cycle3;
    }

    public Date getPrevious_statement_date3() {
        return this.previous_statement_date3;
    }

    public void setPrevious_statement_date3(Date previous_statement_date3) {
        this.previous_statement_date3 = previous_statement_date3;
    }

    public BigDecimal getPrevious_statement_balance3() {
        return this.previous_statement_balance3;
    }

    public void setPrevious_statement_balance3(BigDecimal previous_statement_balance3) {
        this.previous_statement_balance3 = previous_statement_balance3;
    }

    public BigDecimal getPrevious_statement_no3() {
        return this.previous_statement_no3;
    }

    public void setPrevious_statement_no3(BigDecimal previous_statement_no3) {
        this.previous_statement_no3 = previous_statement_no3;
    }

    public Character getGen_stmt_only_on_mvmt3() {
        return this.gen_stmt_only_on_mvmt3;
    }

    public void setGen_stmt_only_on_mvmt3(Character gen_stmt_only_on_mvmt3) {
        this.gen_stmt_only_on_mvmt3 = gen_stmt_only_on_mvmt3;
    }

    public BigDecimal getSweep_type() {
        return this.sweep_type;
    }

    public void setSweep_type(BigDecimal sweep_type) {
        this.sweep_type = sweep_type;
    }

    public String getMaster_account_no() {
        return this.master_account_no;
    }

    public void setMaster_account_no(String master_account_no) {
        this.master_account_no = master_account_no;
    }

    public BigDecimal getAuto_deposits_bal() {
        return this.auto_deposits_bal;
    }

    public void setAuto_deposits_bal(BigDecimal auto_deposits_bal) {
        this.auto_deposits_bal = auto_deposits_bal;
    }

    public String getCas_customer() {
        return this.cas_customer;
    }

    public void setCas_customer(String cas_customer) {
        this.cas_customer = cas_customer;
    }

    public String getAccount_type() {
        return this.account_type;
    }

    public void setAccount_type(String account_type) {
        this.account_type = account_type;
    }

    public BigDecimal getMin_reqd_bal() {
        return this.min_reqd_bal;
    }

    public void setMin_reqd_bal(BigDecimal min_reqd_bal) {
        this.min_reqd_bal = min_reqd_bal;
    }

    public Character getPositive_pay_ac() {
        return this.positive_pay_ac;
    }

    public void setPositive_pay_ac(Character positive_pay_ac) {
        this.positive_pay_ac = positive_pay_ac;
    }

    public BigDecimal getStale_days() {
        return this.stale_days;
    }

    public void setStale_days(BigDecimal stale_days) {
        this.stale_days = stale_days;
    }

    public BigDecimal getCr_auto_ex_rate_lmt() {
        return this.cr_auto_ex_rate_lmt;
    }

    public void setCr_auto_ex_rate_lmt(BigDecimal cr_auto_ex_rate_lmt) {
        this.cr_auto_ex_rate_lmt = cr_auto_ex_rate_lmt;
    }

    public BigDecimal getDr_auto_ex_rate_lmt() {
        return this.dr_auto_ex_rate_lmt;
    }

    public void setDr_auto_ex_rate_lmt(BigDecimal dr_auto_ex_rate_lmt) {
        this.dr_auto_ex_rate_lmt = dr_auto_ex_rate_lmt;
    }

    public String getTrack_receivable() {
        return this.track_receivable;
    }

    public void setTrack_receivable(String track_receivable) {
        this.track_receivable = track_receivable;
    }

    public BigDecimal getReceivable_amount() {
        return this.receivable_amount;
    }

    public void setReceivable_amount(BigDecimal receivable_amount) {
        this.receivable_amount = receivable_amount;
    }

    public String getProduct_list() {
        return this.product_list;
    }

    public void setProduct_list(String product_list) {
        this.product_list = product_list;
    }

    public String getTxn_code_list() {
        return this.txn_code_list;
    }

    public void setTxn_code_list(String txn_code_list) {
        this.txn_code_list = txn_code_list;
    }

    public String getSpecial_condition_product() {
        return this.special_condition_product;
    }

    public void setSpecial_condition_product(String special_condition_product) {
        this.special_condition_product = special_condition_product;
    }

    public String getSpecial_condition_txncode() {
        return this.special_condition_txncode;
    }

    public void setSpecial_condition_txncode(String special_condition_txncode) {
        this.special_condition_txncode = special_condition_txncode;
    }

    public Character getReg_d_applicable() {
        return this.reg_d_applicable;
    }

    public void setReg_d_applicable(Character reg_d_applicable) {
        this.reg_d_applicable = reg_d_applicable;
    }

    public String getRegd_periodicity() {
        return this.regd_periodicity;
    }

    public void setRegd_periodicity(String regd_periodicity) {
        this.regd_periodicity = regd_periodicity;
    }

    public Date getRegd_start_date() {
        return this.regd_start_date;
    }

    public void setRegd_start_date(Date regd_start_date) {
        this.regd_start_date = regd_start_date;
    }

    public Date getRegd_end_date() {
        return this.regd_end_date;
    }

    public void setRegd_end_date(Date regd_end_date) {
        this.regd_end_date = regd_end_date;
    }

    public Character getTd_cert_printed() {
        return this.td_cert_printed;
    }

    public void setTd_cert_printed(Character td_cert_printed) {
        this.td_cert_printed = td_cert_printed;
    }

    public String getCheckbook_name_1() {
        return this.checkbook_name_1;
    }

    public void setCheckbook_name_1(String checkbook_name_1) {
        this.checkbook_name_1 = checkbook_name_1;
    }

    public String getCheckbook_name_2() {
        return this.checkbook_name_2;
    }

    public void setCheckbook_name_2(String checkbook_name_2) {
        this.checkbook_name_2 = checkbook_name_2;
    }

    public Character getAuto_reorder_check_required() {
        return this.auto_reorder_check_required;
    }

    public void setAuto_reorder_check_required(Character auto_reorder_check_required) {
        this.auto_reorder_check_required = auto_reorder_check_required;
    }

    public BigDecimal getAuto_reorder_check_level() {
        return this.auto_reorder_check_level;
    }

    public void setAuto_reorder_check_level(BigDecimal auto_reorder_check_level) {
        this.auto_reorder_check_level = auto_reorder_check_level;
    }

    public BigDecimal getAuto_reorder_check_leaves() {
        return this.auto_reorder_check_leaves;
    }

    public void setAuto_reorder_check_leaves(BigDecimal auto_reorder_check_leaves) {
        this.auto_reorder_check_leaves = auto_reorder_check_leaves;
    }

    public String getNetting_required() {
        return this.netting_required;
    }

    public void setNetting_required(String netting_required) {
        this.netting_required = netting_required;
    }

    public String getReferral_required() {
        return this.referral_required;
    }

    public void setReferral_required(String referral_required) {
        this.referral_required = referral_required;
    }

    public String getLodgement_book_facility() {
        return this.lodgement_book_facility;
    }

    public void setLodgement_book_facility(String lodgement_book_facility) {
        this.lodgement_book_facility = lodgement_book_facility;
    }

    public String getAcc_status() {
        return this.acc_status;
    }

    public void setAcc_status(String acc_status) {
        this.acc_status = acc_status;
    }

    public Date getStatus_since() {
        return this.status_since;
    }

    public void setStatus_since(Date status_since) {
        this.status_since = status_since;
    }

    public String getInherit_reporting() {
        return this.inherit_reporting;
    }

    public void setInherit_reporting(String inherit_reporting) {
        this.inherit_reporting = inherit_reporting;
    }

    public Date getOverdraft_since() {
        return this.overdraft_since;
    }

    public void setOverdraft_since(Date overdraft_since) {
        this.overdraft_since = overdraft_since;
    }

    public Date getPrev_ovd_date() {
        return this.prev_ovd_date;
    }

    public void setPrev_ovd_date(Date prev_ovd_date) {
        this.prev_ovd_date = prev_ovd_date;
    }

    public String getStatus_change_automatic() {
        return this.status_change_automatic;
    }

    public void setStatus_change_automatic(String status_change_automatic) {
        this.status_change_automatic = status_change_automatic;
    }

    public Date getOverline_od_since() {
        return this.overline_od_since;
    }

    public void setOverline_od_since(Date overline_od_since) {
        this.overline_od_since = overline_od_since;
    }

    public Date getTod_since() {
        return this.tod_since;
    }

    public void setTod_since(Date tod_since) {
        this.tod_since = tod_since;
    }

    public Date getPrev_tod_since() {
        return this.prev_tod_since;
    }

    public void setPrev_tod_since(Date prev_tod_since) {
        this.prev_tod_since = prev_tod_since;
    }

    public String getDormant_param() {
        return this.dormant_param;
    }

    public void setDormant_param(String dormant_param) {
        this.dormant_param = dormant_param;
    }

    public BigDecimal getDr_int_due() {
        return this.dr_int_due;
    }

    public void setDr_int_due(BigDecimal dr_int_due) {
        this.dr_int_due = dr_int_due;
    }

    public String getExcl_sameday_rvrtrns_fm_stmt() {
        return this.excl_sameday_rvrtrns_fm_stmt;
    }

    public void setExcl_sameday_rvrtrns_fm_stmt(String excl_sameday_rvrtrns_fm_stmt) {
        this.excl_sameday_rvrtrns_fm_stmt = excl_sameday_rvrtrns_fm_stmt;
    }

    public Character getAllow_back_period_entry() {
        return this.allow_back_period_entry;
    }

    public void setAllow_back_period_entry(Character allow_back_period_entry) {
        this.allow_back_period_entry = allow_back_period_entry;
    }

    public String getAuto_prov_reqd() {
        return this.auto_prov_reqd;
    }

    public void setAuto_prov_reqd(String auto_prov_reqd) {
        this.auto_prov_reqd = auto_prov_reqd;
    }

    public String getExposure_category() {
        return this.exposure_category;
    }

    public void setExposure_category(String exposure_category) {
        this.exposure_category = exposure_category;
    }

    public BigDecimal getRisk_free_exp_amount() {
        return this.risk_free_exp_amount;
    }

    public void setRisk_free_exp_amount(BigDecimal risk_free_exp_amount) {
        this.risk_free_exp_amount = risk_free_exp_amount;
    }

    public BigDecimal getProvision_amount() {
        return this.provision_amount;
    }

    public void setProvision_amount(BigDecimal provision_amount) {
        this.provision_amount = provision_amount;
    }

    public BigDecimal getCredit_txn_limit() {
        return this.credit_txn_limit;
    }

    public void setCredit_txn_limit(BigDecimal credit_txn_limit) {
        this.credit_txn_limit = credit_txn_limit;
    }

    public Date getCr_lm_start_date() {
        return this.cr_lm_start_date;
    }

    public void setCr_lm_start_date(Date cr_lm_start_date) {
        this.cr_lm_start_date = cr_lm_start_date;
    }

    public Date getCr_lm_rev_date() {
        return this.cr_lm_rev_date;
    }

    public void setCr_lm_rev_date(Date cr_lm_rev_date) {
        this.cr_lm_rev_date = cr_lm_rev_date;
    }

    public String getStatement_account() {
        return this.statement_account;
    }

    public void setStatement_account(String statement_account) {
        this.statement_account = statement_account;
    }

    public String getAccount_derived_status() {
        return this.account_derived_status;
    }

    public void setAccount_derived_status(String account_derived_status) {
        this.account_derived_status = account_derived_status;
    }

    public String getProv_ccy_type() {
        return this.prov_ccy_type;
    }

    public void setProv_ccy_type(String prov_ccy_type) {
        this.prov_ccy_type = prov_ccy_type;
    }

    public BigDecimal getChg_due() {
        return this.chg_due;
    }

    public void setChg_due(BigDecimal chg_due) {
        this.chg_due = chg_due;
    }

    public BigDecimal getWithdrawable_uncolled_fund() {
        return this.withdrawable_uncolled_fund;
    }

    public void setWithdrawable_uncolled_fund(BigDecimal withdrawable_uncolled_fund) {
        this.withdrawable_uncolled_fund = withdrawable_uncolled_fund;
    }

    public String getDefer_recon() {
        return this.defer_recon;
    }

    public void setDefer_recon(String defer_recon) {
        this.defer_recon = defer_recon;
    }

    public String getConsolidation_reqd() {
        return this.consolidation_reqd;
    }

    public void setConsolidation_reqd(String consolidation_reqd) {
        this.consolidation_reqd = consolidation_reqd;
    }

    public String getFunding() {
        return this.funding;
    }

    public void setFunding(String funding) {
        this.funding = funding;
    }

    public String getFunding_branch() {
        return this.funding_branch;
    }

    public void setFunding_branch(String funding_branch) {
        this.funding_branch = funding_branch;
    }

    public String getFunding_account() {
        return this.funding_account;
    }

    public void setFunding_account(String funding_account) {
        this.funding_account = funding_account;
    }

    public String getMod9_validation_reqd() {
        return this.mod9_validation_reqd;
    }

    public void setMod9_validation_reqd(String mod9_validation_reqd) {
        this.mod9_validation_reqd = mod9_validation_reqd;
    }

    public BigDecimal getValidation_digit() {
        return this.validation_digit;
    }

    public void setValidation_digit(BigDecimal validation_digit) {
        this.validation_digit = validation_digit;
    }

    public String getMedia() {
        return this.media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public String getAcc_tanked_stat() {
        return this.acc_tanked_stat;
    }

    public void setAcc_tanked_stat(String acc_tanked_stat) {
        this.acc_tanked_stat = acc_tanked_stat;
    }

    public String getGen_interim_stmt() {
        return this.gen_interim_stmt;
    }

    public void setGen_interim_stmt(String gen_interim_stmt) {
        this.gen_interim_stmt = gen_interim_stmt;
    }

    public String getGen_interim_stmt_on_mvmt() {
        return this.gen_interim_stmt_on_mvmt;
    }

    public void setGen_interim_stmt_on_mvmt(String gen_interim_stmt_on_mvmt) {
        this.gen_interim_stmt_on_mvmt = gen_interim_stmt_on_mvmt;
    }

    public String getGen_balance_report() {
        return this.gen_balance_report;
    }

    public void setGen_balance_report(String gen_balance_report) {
        this.gen_balance_report = gen_balance_report;
    }

    public String getInterim_report_since() {
        return this.interim_report_since;
    }

    public void setInterim_report_since(String interim_report_since) {
        this.interim_report_since = interim_report_since;
    }

    public String getInterim_report_type() {
        return this.interim_report_type;
    }

    public void setInterim_report_type(String interim_report_type) {
        this.interim_report_type = interim_report_type;
    }

    public String getBalance_report_since() {
        return this.balance_report_since;
    }

    public void setBalance_report_since(String balance_report_since) {
        this.balance_report_since = balance_report_since;
    }

    public String getBalance_report_type() {
        return this.balance_report_type;
    }

    public void setBalance_report_type(String balance_report_type) {
        this.balance_report_type = balance_report_type;
    }

    public BigDecimal getInterim_debit_amt() {
        return this.interim_debit_amt;
    }

    public void setInterim_debit_amt(BigDecimal interim_debit_amt) {
        this.interim_debit_amt = interim_debit_amt;
    }

    public BigDecimal getInterim_credit_amt() {
        return this.interim_credit_amt;
    }

    public void setInterim_credit_amt(BigDecimal interim_credit_amt) {
        this.interim_credit_amt = interim_credit_amt;
    }

    public BigDecimal getInterim_stmt_day_count() {
        return this.interim_stmt_day_count;
    }

    public void setInterim_stmt_day_count(BigDecimal interim_stmt_day_count) {
        this.interim_stmt_day_count = interim_stmt_day_count;
    }

    public BigDecimal getInterim_stmt_ytd_count() {
        return this.interim_stmt_ytd_count;
    }

    public void setInterim_stmt_ytd_count(BigDecimal interim_stmt_ytd_count) {
        this.interim_stmt_ytd_count = interim_stmt_ytd_count;
    }

    public Character getMode_of_operation() {
        return this.mode_of_operation;
    }

    public void setMode_of_operation(Character mode_of_operation) {
        this.mode_of_operation = mode_of_operation;
    }

    public BigDecimal getInf_acc_opening_amt() {
        return this.inf_acc_opening_amt;
    }

    public void setInf_acc_opening_amt(BigDecimal inf_acc_opening_amt) {
        this.inf_acc_opening_amt = inf_acc_opening_amt;
    }

    public String getInf_pay_in_option() {
        return this.inf_pay_in_option;
    }

    public void setInf_pay_in_option(String inf_pay_in_option) {
        this.inf_pay_in_option = inf_pay_in_option;
    }

    public String getInf_offset_branch() {
        return this.inf_offset_branch;
    }

    public void setInf_offset_branch(String inf_offset_branch) {
        this.inf_offset_branch = inf_offset_branch;
    }

    public String getInf_offset_account() {
        return this.inf_offset_account;
    }

    public void setInf_offset_account(String inf_offset_account) {
        this.inf_offset_account = inf_offset_account;
    }

    public String getInf_waive_acc_open_charge() {
        return this.inf_waive_acc_open_charge;
    }

    public void setInf_waive_acc_open_charge(String inf_waive_acc_open_charge) {
        this.inf_waive_acc_open_charge = inf_waive_acc_open_charge;
    }

    public BigDecimal getDaylight_limit_amount() {
        return this.daylight_limit_amount;
    }

    public void setDaylight_limit_amount(BigDecimal daylight_limit_amount) {
        this.daylight_limit_amount = daylight_limit_amount;
    }

    public String getTrnover_lmt_code() {
        return this.trnover_lmt_code;
    }

    public void setTrnover_lmt_code(String trnover_lmt_code) {
        this.trnover_lmt_code = trnover_lmt_code;
    }

    public BigDecimal getPassbook_numeric() {
        return this.passbook_numeric;
    }

    public void setPassbook_numeric(BigDecimal passbook_numeric) {
        this.passbook_numeric = passbook_numeric;
    }

    public String getCountry_code() {
        return this.country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getConsol_chg_acc() {
        return this.consol_chg_acc;
    }

    public void setConsol_chg_acc(String consol_chg_acc) {
        this.consol_chg_acc = consol_chg_acc;
    }

    public Character getEscrow_transfer() {
        return this.escrow_transfer;
    }

    public void setEscrow_transfer(Character escrow_transfer) {
        this.escrow_transfer = escrow_transfer;
    }

    public String getEscrow_branch_code() {
        return this.escrow_branch_code;
    }

    public void setEscrow_branch_code(String escrow_branch_code) {
        this.escrow_branch_code = escrow_branch_code;
    }

    public String getEscrow_ac_no() {
        return this.escrow_ac_no;
    }

    public void setEscrow_ac_no(String escrow_ac_no) {
        this.escrow_ac_no = escrow_ac_no;
    }

    public BigDecimal getEscrow_percentage() {
        return this.escrow_percentage;
    }

    public void setEscrow_percentage(BigDecimal escrow_percentage) {
        this.escrow_percentage = escrow_percentage;
    }

    public BigDecimal getSod_notification_percent() {
        return this.sod_notification_percent;
    }

    public void setSod_notification_percent(BigDecimal sod_notification_percent) {
        this.sod_notification_percent = sod_notification_percent;
    }

    public String getSalary_account() {
        return this.salary_account;
    }

    public void setSalary_account(String salary_account) {
        this.salary_account = salary_account;
    }

    public Character getRepl_cust_sig() {
        return this.repl_cust_sig;
    }

    public void setRepl_cust_sig(Character repl_cust_sig) {
        this.repl_cust_sig = repl_cust_sig;
    }

    public BigDecimal getMax_no_cheque_rejections() {
        return this.max_no_cheque_rejections;
    }

    public void setMax_no_cheque_rejections(BigDecimal max_no_cheque_rejections) {
        this.max_no_cheque_rejections = max_no_cheque_rejections;
    }

    public BigDecimal getNo_cheque_rejections() {
        return this.no_cheque_rejections;
    }

    public void setNo_cheque_rejections(BigDecimal no_cheque_rejections) {
        this.no_cheque_rejections = no_cheque_rejections;
    }

    public String getFund_id() {
        return this.fund_id;
    }

    public void setFund_id(String fund_id) {
        this.fund_id = fund_id;
    }

    public String getLinked_dep_branch() {
        return this.linked_dep_branch;
    }

    public void setLinked_dep_branch(String linked_dep_branch) {
        this.linked_dep_branch = linked_dep_branch;
    }

    public String getLinked_dep_acc() {
        return this.linked_dep_acc;
    }

    public void setLinked_dep_acc(String linked_dep_acc) {
        this.linked_dep_acc = linked_dep_acc;
    }

    public String getMudarabah_accounts() {
        return this.mudarabah_accounts;
    }

    public void setMudarabah_accounts(String mudarabah_accounts) {
        this.mudarabah_accounts = mudarabah_accounts;
    }

    public String getZakat_exemption() {
        return this.zakat_exemption;
    }

    public void setZakat_exemption(String zakat_exemption) {
        this.zakat_exemption = zakat_exemption;
    }

    public String getAccount_auto_closed() {
        return this.account_auto_closed;
    }

    public void setAccount_auto_closed(String account_auto_closed) {
        this.account_auto_closed = account_auto_closed;
    }

    public String getConsol_chg_brn() {
        return this.consol_chg_brn;
    }

    public void setConsol_chg_brn(String consol_chg_brn) {
        this.consol_chg_brn = consol_chg_brn;
    }

    public Date getNo_of_chq_rej_reset_on() {
        return this.no_of_chq_rej_reset_on;
    }

    public void setNo_of_chq_rej_reset_on(Date no_of_chq_rej_reset_on) {
        this.no_of_chq_rej_reset_on = no_of_chq_rej_reset_on;
    }

    public String getCrs_stat_reqd() {
        return this.crs_stat_reqd;
    }

    public void setCrs_stat_reqd(String crs_stat_reqd) {
        this.crs_stat_reqd = crs_stat_reqd;
    }

    public Character getContribute_to_pdm() {
        return this.contribute_to_pdm;
    }

    public void setContribute_to_pdm(Character contribute_to_pdm) {
        this.contribute_to_pdm = contribute_to_pdm;
    }

    public Character getExclude_from_distribution() {
        return this.exclude_from_distribution;
    }

    public void setExclude_from_distribution(Character exclude_from_distribution) {
        this.exclude_from_distribution = exclude_from_distribution;
    }

    public Character getAc_set_close() {
        return this.ac_set_close;
    }

    public void setAc_set_close(Character ac_set_close) {
        this.ac_set_close = ac_set_close;
    }

    public Date getAc_set_close_date() {
        return this.ac_set_close_date;
    }

    public void setAc_set_close_date(Date ac_set_close_date) {
        this.ac_set_close_date = ac_set_close_date;
    }

    public String getProject_account() {
        return this.project_account;
    }

    public void setProject_account(String project_account) {
        this.project_account = project_account;
    }

    public BigDecimal getAcy_sweep_ineligible() {
        return this.acy_sweep_ineligible;
    }

    public void setAcy_sweep_ineligible(BigDecimal acy_sweep_ineligible) {
        this.acy_sweep_ineligible = acy_sweep_ineligible;
    }

    public String getMt110_recon_reqd() {
        return this.mt110_recon_reqd;
    }

    public void setMt110_recon_reqd(String mt110_recon_reqd) {
        this.mt110_recon_reqd = mt110_recon_reqd;
    }

    public String getSweep_required() {
        return this.sweep_required;
    }

    public void setSweep_required(String sweep_required) {
        this.sweep_required = sweep_required;
    }

    public String getAuto_deposit() {
        return this.auto_deposit;
    }

    public void setAuto_deposit(String auto_deposit) {
        this.auto_deposit = auto_deposit;
    }

    public String getSweep_out() {
        return this.sweep_out;
    }

    public void setSweep_out(String sweep_out) {
        this.sweep_out = sweep_out;
    }

    public String getAuto_create_pool() {
        return this.auto_create_pool;
    }

    public void setAuto_create_pool(String auto_create_pool) {
        this.auto_create_pool = auto_create_pool;
    }

    public Date getLast_purge_date() {
        return this.last_purge_date;
    }

    public void setLast_purge_date(Date last_purge_date) {
        this.last_purge_date = last_purge_date;
    }

    public String getDefault_waiver() {
        return this.default_waiver;
    }

    public void setDefault_waiver(String default_waiver) {
        this.default_waiver = default_waiver;
    }

    public Character getAuto_dc_request() {
        return this.auto_dc_request;
    }

    public void setAuto_dc_request(Character auto_dc_request) {
        this.auto_dc_request = auto_dc_request;
    }

    public Character getAuto_chqbk_request() {
        return this.auto_chqbk_request;
    }

    public void setAuto_chqbk_request(Character auto_chqbk_request) {
        this.auto_chqbk_request = auto_chqbk_request;
    }

    public Character getNsf_blacklist_status() {
        return this.nsf_blacklist_status;
    }

    public void setNsf_blacklist_status(Character nsf_blacklist_status) {
        this.nsf_blacklist_status = nsf_blacklist_status;
    }

    public String getNsf_auto_update() {
        return this.nsf_auto_update;
    }

    public void setNsf_auto_update(String nsf_auto_update) {
        this.nsf_auto_update = nsf_auto_update;
    }

    public String getIntermediary_required() {
        return this.intermediary_required;
    }

    public void setIntermediary_required(String intermediary_required) {
        this.intermediary_required = intermediary_required;
    }

    public sttm_cust_account() {
    }

    public sttm_cust_account(String cust_ac_no, String branch_code, String ac_desc, String cust_no, String ccy, String account_class, Character ac_stat_no_dr, Character ac_stat_no_cr, Character ac_stat_block, Character ac_stat_stop_pay, Character ac_stat_dormant, Character joint_ac_indicator, Date ac_open_date, BigDecimal ac_stmt_day, Character ac_stmt_cycle, String alt_ac_no, Character cheque_book_facility, Character atm_facility, Character passbook_facility, Character ac_stmt_type, String dr_ho_line, String cr_ho_line, String cr_cb_line, String dr_cb_line, BigDecimal sublimit, BigDecimal uncoll_funds_limit, Character ac_stat_frozen, Date previous_statement_date, BigDecimal previous_statement_balance, BigDecimal previous_statement_no, Date tod_limit_start_date, Date tod_limit_end_date, BigDecimal tod_limit, String nominee1, String nominee2, String dr_gl, String cr_gl, Character record_stat, Character auth_stat, BigDecimal mod_no, String maker_id, Date maker_dt_stamp, String checker_id, Date checker_dt_stamp, Character once_auth, String limit_ccy, Character line_id, BigDecimal offline_limit, Character cas_account, BigDecimal acy_opening_bal, BigDecimal lcy_opening_bal, BigDecimal acy_today_tover_dr, BigDecimal lcy_today_tover_dr, BigDecimal acy_today_tover_cr, BigDecimal lcy_today_tover_cr, BigDecimal acy_tank_cr, BigDecimal acy_tank_dr, BigDecimal lcy_tank_cr, BigDecimal lcy_tank_dr, BigDecimal acy_tover_cr, BigDecimal lcy_tover_cr, BigDecimal acy_tank_uncollected, BigDecimal acy_curr_balance, BigDecimal lcy_curr_balance, BigDecimal acy_blocked_amount, BigDecimal acy_avl_bal, BigDecimal acy_unauth_dr, BigDecimal acy_unauth_tank_dr, BigDecimal acy_unauth_cr, BigDecimal acy_unauth_tank_cr, BigDecimal acy_unauth_uncollected, BigDecimal acy_unauth_tank_uncollected, BigDecimal acy_mtd_tover_dr, BigDecimal lcy_mtd_tover_dr, BigDecimal acy_mtd_tover_cr, BigDecimal lcy_mtd_tover_cr, BigDecimal acy_accrued_dr_ic, BigDecimal acy_accrued_cr_ic, Date date_last_cr_activity, Date date_last_dr_activity, Date date_last_dr, Date date_last_cr, BigDecimal acy_uncollected, Date tod_start_date, Date tod_end_date, Date dormancy_date, BigDecimal dormancy_days, Character has_tov, Date last_ccy_conv_date, String address1, String address2, String address3, String address4, String type_of_chq, String atm_cust_ac_no, BigDecimal atm_dly_amt_limit, BigDecimal atm_dly_count_limit, Character gen_stmt_only_on_mvmt, Character ac_stat_de_post, String display_iban_in_advices, String clearing_bank_code, String clearing_ac_no, String iban_ac_no, String reg_cc_availability, BigDecimal reg_cc_available_funds, BigDecimal prev_ac_srno_printed_in_pbk, BigDecimal latest_srno_submitted, BigDecimal prev_runbal_printed_in_pbk, BigDecimal latest_runbal_submmited, BigDecimal prev_page_no, BigDecimal prev_line_no, String mt210_reqd, Character acc_stmt_type2, BigDecimal acc_stmt_day2, Character ac_stmt_cycle2, Date previous_statement_date2, BigDecimal previous_statement_balance2, BigDecimal previous_statement_no2, Character gen_stmt_only_on_mvmt2, Character acc_stmt_type3, BigDecimal acc_stmt_day3, Character ac_stmt_cycle3, Date previous_statement_date3, BigDecimal previous_statement_balance3, BigDecimal previous_statement_no3, Character gen_stmt_only_on_mvmt3, BigDecimal sweep_type, String master_account_no, BigDecimal auto_deposits_bal, String cas_customer, String account_type, BigDecimal min_reqd_bal, Character positive_pay_ac, BigDecimal stale_days, BigDecimal cr_auto_ex_rate_lmt, BigDecimal dr_auto_ex_rate_lmt, String track_receivable, BigDecimal receivable_amount, String product_list, String txn_code_list, String special_condition_product, String special_condition_txncode, Character reg_d_applicable, String regd_periodicity, Date regd_start_date, Date regd_end_date, Character td_cert_printed, String checkbook_name_1, String checkbook_name_2, Character auto_reorder_check_required, BigDecimal auto_reorder_check_level, BigDecimal auto_reorder_check_leaves, String netting_required, String referral_required, String lodgement_book_facility, String acc_status, Date status_since, String inherit_reporting, Date overdraft_since, Date prev_ovd_date, String status_change_automatic, Date overline_od_since, Date tod_since, Date prev_tod_since, String dormant_param, BigDecimal dr_int_due, String excl_sameday_rvrtrns_fm_stmt, Character allow_back_period_entry, String auto_prov_reqd, String exposure_category, BigDecimal risk_free_exp_amount, BigDecimal provision_amount, BigDecimal credit_txn_limit, Date cr_lm_start_date, Date cr_lm_rev_date, String statement_account, String account_derived_status, String prov_ccy_type, BigDecimal chg_due, BigDecimal withdrawable_uncolled_fund, String defer_recon, String consolidation_reqd, String funding, String funding_branch, String funding_account, String mod9_validation_reqd, BigDecimal validation_digit, String media, String acc_tanked_stat, String gen_interim_stmt, String gen_interim_stmt_on_mvmt, String gen_balance_report, String interim_report_since, String interim_report_type, String balance_report_since, String balance_report_type, BigDecimal interim_debit_amt, BigDecimal interim_credit_amt, BigDecimal interim_stmt_day_count, BigDecimal interim_stmt_ytd_count, Character mode_of_operation, BigDecimal inf_acc_opening_amt, String inf_pay_in_option, String inf_offset_branch, String inf_offset_account, String inf_waive_acc_open_charge, BigDecimal daylight_limit_amount, String trnover_lmt_code, BigDecimal passbook_numeric, String country_code, String consol_chg_acc, Character escrow_transfer, String escrow_branch_code, String escrow_ac_no, BigDecimal escrow_percentage, BigDecimal sod_notification_percent, String salary_account, Character repl_cust_sig, BigDecimal max_no_cheque_rejections, BigDecimal no_cheque_rejections, String fund_id, String linked_dep_branch, String linked_dep_acc, String mudarabah_accounts, String zakat_exemption, String account_auto_closed, String consol_chg_brn, Date no_of_chq_rej_reset_on, String crs_stat_reqd, Character contribute_to_pdm, Character exclude_from_distribution, Character ac_set_close, Date ac_set_close_date, String project_account, BigDecimal acy_sweep_ineligible, String mt110_recon_reqd, String sweep_required, String auto_deposit, String sweep_out, String auto_create_pool, Date last_purge_date, String default_waiver, Character auto_dc_request, Character auto_chqbk_request, Character nsf_blacklist_status, String nsf_auto_update, String intermediary_required) {
        this.cust_ac_no = cust_ac_no;
        this.branch_code = branch_code;
        this.ac_desc = ac_desc;
        this.cust_no = cust_no;
        this.ccy = ccy;
        this.account_class = account_class;
        this.ac_stat_no_dr = ac_stat_no_dr;
        this.ac_stat_no_cr = ac_stat_no_cr;
        this.ac_stat_block = ac_stat_block;
        this.ac_stat_stop_pay = ac_stat_stop_pay;
        this.ac_stat_dormant = ac_stat_dormant;
        this.joint_ac_indicator = joint_ac_indicator;
        this.ac_open_date = ac_open_date;
        this.ac_stmt_day = ac_stmt_day;
        this.ac_stmt_cycle = ac_stmt_cycle;
        this.alt_ac_no = alt_ac_no;
        this.cheque_book_facility = cheque_book_facility;
        this.atm_facility = atm_facility;
        this.passbook_facility = passbook_facility;
        this.ac_stmt_type = ac_stmt_type;
        this.dr_ho_line = dr_ho_line;
        this.cr_ho_line = cr_ho_line;
        this.cr_cb_line = cr_cb_line;
        this.dr_cb_line = dr_cb_line;
        this.sublimit = sublimit;
        this.uncoll_funds_limit = uncoll_funds_limit;
        this.ac_stat_frozen = ac_stat_frozen;
        this.previous_statement_date = previous_statement_date;
        this.previous_statement_balance = previous_statement_balance;
        this.previous_statement_no = previous_statement_no;
        this.tod_limit_start_date = tod_limit_start_date;
        this.tod_limit_end_date = tod_limit_end_date;
        this.tod_limit = tod_limit;
        this.nominee1 = nominee1;
        this.nominee2 = nominee2;
        this.dr_gl = dr_gl;
        this.cr_gl = cr_gl;
        this.record_stat = record_stat;
        this.auth_stat = auth_stat;
        this.mod_no = mod_no;
        this.maker_id = maker_id;
        this.maker_dt_stamp = maker_dt_stamp;
        this.checker_id = checker_id;
        this.checker_dt_stamp = checker_dt_stamp;
        this.once_auth = once_auth;
        this.limit_ccy = limit_ccy;
        this.line_id = line_id;
        this.offline_limit = offline_limit;
        this.cas_account = cas_account;
        this.acy_opening_bal = acy_opening_bal;
        this.lcy_opening_bal = lcy_opening_bal;
        this.acy_today_tover_dr = acy_today_tover_dr;
        this.lcy_today_tover_dr = lcy_today_tover_dr;
        this.acy_today_tover_cr = acy_today_tover_cr;
        this.lcy_today_tover_cr = lcy_today_tover_cr;
        this.acy_tank_cr = acy_tank_cr;
        this.acy_tank_dr = acy_tank_dr;
        this.lcy_tank_cr = lcy_tank_cr;
        this.lcy_tank_dr = lcy_tank_dr;
        this.acy_tover_cr = acy_tover_cr;
        this.lcy_tover_cr = lcy_tover_cr;
        this.acy_tank_uncollected = acy_tank_uncollected;
        this.acy_curr_balance = acy_curr_balance;
        this.lcy_curr_balance = lcy_curr_balance;
        this.acy_blocked_amount = acy_blocked_amount;
        this.acy_avl_bal = acy_avl_bal;
        this.acy_unauth_dr = acy_unauth_dr;
        this.acy_unauth_tank_dr = acy_unauth_tank_dr;
        this.acy_unauth_cr = acy_unauth_cr;
        this.acy_unauth_tank_cr = acy_unauth_tank_cr;
        this.acy_unauth_uncollected = acy_unauth_uncollected;
        this.acy_unauth_tank_uncollected = acy_unauth_tank_uncollected;
        this.acy_mtd_tover_dr = acy_mtd_tover_dr;
        this.lcy_mtd_tover_dr = lcy_mtd_tover_dr;
        this.acy_mtd_tover_cr = acy_mtd_tover_cr;
        this.lcy_mtd_tover_cr = lcy_mtd_tover_cr;
        this.acy_accrued_dr_ic = acy_accrued_dr_ic;
        this.acy_accrued_cr_ic = acy_accrued_cr_ic;
        this.date_last_cr_activity = date_last_cr_activity;
        this.date_last_dr_activity = date_last_dr_activity;
        this.date_last_dr = date_last_dr;
        this.date_last_cr = date_last_cr;
        this.acy_uncollected = acy_uncollected;
        this.tod_start_date = tod_start_date;
        this.tod_end_date = tod_end_date;
        this.dormancy_date = dormancy_date;
        this.dormancy_days = dormancy_days;
        this.has_tov = has_tov;
        this.last_ccy_conv_date = last_ccy_conv_date;
        this.address1 = address1;
        this.address2 = address2;
        this.address3 = address3;
        this.address4 = address4;
        this.type_of_chq = type_of_chq;
        this.atm_cust_ac_no = atm_cust_ac_no;
        this.atm_dly_amt_limit = atm_dly_amt_limit;
        this.atm_dly_count_limit = atm_dly_count_limit;
        this.gen_stmt_only_on_mvmt = gen_stmt_only_on_mvmt;
        this.ac_stat_de_post = ac_stat_de_post;
        this.display_iban_in_advices = display_iban_in_advices;
        this.clearing_bank_code = clearing_bank_code;
        this.clearing_ac_no = clearing_ac_no;
        this.iban_ac_no = iban_ac_no;
        this.reg_cc_availability = reg_cc_availability;
        this.reg_cc_available_funds = reg_cc_available_funds;
        this.prev_ac_srno_printed_in_pbk = prev_ac_srno_printed_in_pbk;
        this.latest_srno_submitted = latest_srno_submitted;
        this.prev_runbal_printed_in_pbk = prev_runbal_printed_in_pbk;
        this.latest_runbal_submmited = latest_runbal_submmited;
        this.prev_page_no = prev_page_no;
        this.prev_line_no = prev_line_no;
        this.mt210_reqd = mt210_reqd;
        this.acc_stmt_type2 = acc_stmt_type2;
        this.acc_stmt_day2 = acc_stmt_day2;
        this.ac_stmt_cycle2 = ac_stmt_cycle2;
        this.previous_statement_date2 = previous_statement_date2;
        this.previous_statement_balance2 = previous_statement_balance2;
        this.previous_statement_no2 = previous_statement_no2;
        this.gen_stmt_only_on_mvmt2 = gen_stmt_only_on_mvmt2;
        this.acc_stmt_type3 = acc_stmt_type3;
        this.acc_stmt_day3 = acc_stmt_day3;
        this.ac_stmt_cycle3 = ac_stmt_cycle3;
        this.previous_statement_date3 = previous_statement_date3;
        this.previous_statement_balance3 = previous_statement_balance3;
        this.previous_statement_no3 = previous_statement_no3;
        this.gen_stmt_only_on_mvmt3 = gen_stmt_only_on_mvmt3;
        this.sweep_type = sweep_type;
        this.master_account_no = master_account_no;
        this.auto_deposits_bal = auto_deposits_bal;
        this.cas_customer = cas_customer;
        this.account_type = account_type;
        this.min_reqd_bal = min_reqd_bal;
        this.positive_pay_ac = positive_pay_ac;
        this.stale_days = stale_days;
        this.cr_auto_ex_rate_lmt = cr_auto_ex_rate_lmt;
        this.dr_auto_ex_rate_lmt = dr_auto_ex_rate_lmt;
        this.track_receivable = track_receivable;
        this.receivable_amount = receivable_amount;
        this.product_list = product_list;
        this.txn_code_list = txn_code_list;
        this.special_condition_product = special_condition_product;
        this.special_condition_txncode = special_condition_txncode;
        this.reg_d_applicable = reg_d_applicable;
        this.regd_periodicity = regd_periodicity;
        this.regd_start_date = regd_start_date;
        this.regd_end_date = regd_end_date;
        this.td_cert_printed = td_cert_printed;
        this.checkbook_name_1 = checkbook_name_1;
        this.checkbook_name_2 = checkbook_name_2;
        this.auto_reorder_check_required = auto_reorder_check_required;
        this.auto_reorder_check_level = auto_reorder_check_level;
        this.auto_reorder_check_leaves = auto_reorder_check_leaves;
        this.netting_required = netting_required;
        this.referral_required = referral_required;
        this.lodgement_book_facility = lodgement_book_facility;
        this.acc_status = acc_status;
        this.status_since = status_since;
        this.inherit_reporting = inherit_reporting;
        this.overdraft_since = overdraft_since;
        this.prev_ovd_date = prev_ovd_date;
        this.status_change_automatic = status_change_automatic;
        this.overline_od_since = overline_od_since;
        this.tod_since = tod_since;
        this.prev_tod_since = prev_tod_since;
        this.dormant_param = dormant_param;
        this.dr_int_due = dr_int_due;
        this.excl_sameday_rvrtrns_fm_stmt = excl_sameday_rvrtrns_fm_stmt;
        this.allow_back_period_entry = allow_back_period_entry;
        this.auto_prov_reqd = auto_prov_reqd;
        this.exposure_category = exposure_category;
        this.risk_free_exp_amount = risk_free_exp_amount;
        this.provision_amount = provision_amount;
        this.credit_txn_limit = credit_txn_limit;
        this.cr_lm_start_date = cr_lm_start_date;
        this.cr_lm_rev_date = cr_lm_rev_date;
        this.statement_account = statement_account;
        this.account_derived_status = account_derived_status;
        this.prov_ccy_type = prov_ccy_type;
        this.chg_due = chg_due;
        this.withdrawable_uncolled_fund = withdrawable_uncolled_fund;
        this.defer_recon = defer_recon;
        this.consolidation_reqd = consolidation_reqd;
        this.funding = funding;
        this.funding_branch = funding_branch;
        this.funding_account = funding_account;
        this.mod9_validation_reqd = mod9_validation_reqd;
        this.validation_digit = validation_digit;
        this.media = media;
        this.acc_tanked_stat = acc_tanked_stat;
        this.gen_interim_stmt = gen_interim_stmt;
        this.gen_interim_stmt_on_mvmt = gen_interim_stmt_on_mvmt;
        this.gen_balance_report = gen_balance_report;
        this.interim_report_since = interim_report_since;
        this.interim_report_type = interim_report_type;
        this.balance_report_since = balance_report_since;
        this.balance_report_type = balance_report_type;
        this.interim_debit_amt = interim_debit_amt;
        this.interim_credit_amt = interim_credit_amt;
        this.interim_stmt_day_count = interim_stmt_day_count;
        this.interim_stmt_ytd_count = interim_stmt_ytd_count;
        this.mode_of_operation = mode_of_operation;
        this.inf_acc_opening_amt = inf_acc_opening_amt;
        this.inf_pay_in_option = inf_pay_in_option;
        this.inf_offset_branch = inf_offset_branch;
        this.inf_offset_account = inf_offset_account;
        this.inf_waive_acc_open_charge = inf_waive_acc_open_charge;
        this.daylight_limit_amount = daylight_limit_amount;
        this.trnover_lmt_code = trnover_lmt_code;
        this.passbook_numeric = passbook_numeric;
        this.country_code = country_code;
        this.consol_chg_acc = consol_chg_acc;
        this.escrow_transfer = escrow_transfer;
        this.escrow_branch_code = escrow_branch_code;
        this.escrow_ac_no = escrow_ac_no;
        this.escrow_percentage = escrow_percentage;
        this.sod_notification_percent = sod_notification_percent;
        this.salary_account = salary_account;
        this.repl_cust_sig = repl_cust_sig;
        this.max_no_cheque_rejections = max_no_cheque_rejections;
        this.no_cheque_rejections = no_cheque_rejections;
        this.fund_id = fund_id;
        this.linked_dep_branch = linked_dep_branch;
        this.linked_dep_acc = linked_dep_acc;
        this.mudarabah_accounts = mudarabah_accounts;
        this.zakat_exemption = zakat_exemption;
        this.account_auto_closed = account_auto_closed;
        this.consol_chg_brn = consol_chg_brn;
        this.no_of_chq_rej_reset_on = no_of_chq_rej_reset_on;
        this.crs_stat_reqd = crs_stat_reqd;
        this.contribute_to_pdm = contribute_to_pdm;
        this.exclude_from_distribution = exclude_from_distribution;
        this.ac_set_close = ac_set_close;
        this.ac_set_close_date = ac_set_close_date;
        this.project_account = project_account;
        this.acy_sweep_ineligible = acy_sweep_ineligible;
        this.mt110_recon_reqd = mt110_recon_reqd;
        this.sweep_required = sweep_required;
        this.auto_deposit = auto_deposit;
        this.sweep_out = sweep_out;
        this.auto_create_pool = auto_create_pool;
        this.last_purge_date = last_purge_date;
        this.default_waiver = default_waiver;
        this.auto_dc_request = auto_dc_request;
        this.auto_chqbk_request = auto_chqbk_request;
        this.nsf_blacklist_status = nsf_blacklist_status;
        this.nsf_auto_update = nsf_auto_update;
        this.intermediary_required = intermediary_required;
    }

    @Override
    public String toString() {
        return "{" +
            " cust_ac_no='" + getCust_ac_no() + "'" +
            ", branch_code='" + getBranch_code() + "'" +
            ", ac_desc='" + getAc_desc() + "'" +
            ", cust_no='" + getCust_no() + "'" +
            ", ccy='" + getCcy() + "'" +
            ", account_class='" + getAccount_class() + "'" +
            ", ac_stat_no_dr='" + getAc_stat_no_dr() + "'" +
            ", ac_stat_no_cr='" + getAc_stat_no_cr() + "'" +
            ", ac_stat_block='" + getAc_stat_block() + "'" +
            ", ac_stat_stop_pay='" + getAc_stat_stop_pay() + "'" +
            ", ac_stat_dormant='" + getAc_stat_dormant() + "'" +
            ", joint_ac_indicator='" + getJoint_ac_indicator() + "'" +
            ", ac_open_date='" + getAc_open_date() + "'" +
            ", ac_stmt_day='" + getAc_stmt_day() + "'" +
            ", ac_stmt_cycle='" + getAc_stmt_cycle() + "'" +
            ", alt_ac_no='" + getAlt_ac_no() + "'" +
            ", cheque_book_facility='" + getCheque_book_facility() + "'" +
            ", atm_facility='" + getAtm_facility() + "'" +
            ", passbook_facility='" + getPassbook_facility() + "'" +
            ", ac_stmt_type='" + getAc_stmt_type() + "'" +
            ", dr_ho_line='" + getDr_ho_line() + "'" +
            ", cr_ho_line='" + getCr_ho_line() + "'" +
            ", cr_cb_line='" + getCr_cb_line() + "'" +
            ", dr_cb_line='" + getDr_cb_line() + "'" +
            ", sublimit='" + getSublimit() + "'" +
            ", uncoll_funds_limit='" + getUncoll_funds_limit() + "'" +
            ", ac_stat_frozen='" + getAc_stat_frozen() + "'" +
            ", previous_statement_date='" + getPrevious_statement_date() + "'" +
            ", previous_statement_balance='" + getPrevious_statement_balance() + "'" +
            ", previous_statement_no='" + getPrevious_statement_no() + "'" +
            ", tod_limit_start_date='" + getTod_limit_start_date() + "'" +
            ", tod_limit_end_date='" + getTod_limit_end_date() + "'" +
            ", tod_limit='" + getTod_limit() + "'" +
            ", nominee1='" + getNominee1() + "'" +
            ", nominee2='" + getNominee2() + "'" +
            ", dr_gl='" + getDr_gl() + "'" +
            ", cr_gl='" + getCr_gl() + "'" +
            ", record_stat='" + getRecord_stat() + "'" +
            ", auth_stat='" + getAuth_stat() + "'" +
            ", mod_no='" + getMod_no() + "'" +
            ", maker_id='" + getMaker_id() + "'" +
            ", maker_dt_stamp='" + getMaker_dt_stamp() + "'" +
            ", checker_id='" + getChecker_id() + "'" +
            ", checker_dt_stamp='" + getChecker_dt_stamp() + "'" +
            ", once_auth='" + getOnce_auth() + "'" +
            ", limit_ccy='" + getLimit_ccy() + "'" +
            ", line_id='" + getLine_id() + "'" +
            ", offline_limit='" + getOffline_limit() + "'" +
            ", cas_account='" + getCas_account() + "'" +
            ", acy_opening_bal='" + getAcy_opening_bal() + "'" +
            ", lcy_opening_bal='" + getLcy_opening_bal() + "'" +
            ", acy_today_tover_dr='" + getAcy_today_tover_dr() + "'" +
            ", lcy_today_tover_dr='" + getLcy_today_tover_dr() + "'" +
            ", acy_today_tover_cr='" + getAcy_today_tover_cr() + "'" +
            ", lcy_today_tover_cr='" + getLcy_today_tover_cr() + "'" +
            ", acy_tank_cr='" + getAcy_tank_cr() + "'" +
            ", acy_tank_dr='" + getAcy_tank_dr() + "'" +
            ", lcy_tank_cr='" + getLcy_tank_cr() + "'" +
            ", lcy_tank_dr='" + getLcy_tank_dr() + "'" +
            ", acy_tover_cr='" + getAcy_tover_cr() + "'" +
            ", lcy_tover_cr='" + getLcy_tover_cr() + "'" +
            ", acy_tank_uncollected='" + getAcy_tank_uncollected() + "'" +
            ", acy_curr_balance='" + getAcy_curr_balance() + "'" +
            ", lcy_curr_balance='" + getLcy_curr_balance() + "'" +
            ", acy_blocked_amount='" + getAcy_blocked_amount() + "'" +
            ", acy_avl_bal='" + getAcy_avl_bal() + "'" +
            ", acy_unauth_dr='" + getAcy_unauth_dr() + "'" +
            ", acy_unauth_tank_dr='" + getAcy_unauth_tank_dr() + "'" +
            ", acy_unauth_cr='" + getAcy_unauth_cr() + "'" +
            ", acy_unauth_tank_cr='" + getAcy_unauth_tank_cr() + "'" +
            ", acy_unauth_uncollected='" + getAcy_unauth_uncollected() + "'" +
            ", acy_unauth_tank_uncollected='" + getAcy_unauth_tank_uncollected() + "'" +
            ", acy_mtd_tover_dr='" + getAcy_mtd_tover_dr() + "'" +
            ", lcy_mtd_tover_dr='" + getLcy_mtd_tover_dr() + "'" +
            ", acy_mtd_tover_cr='" + getAcy_mtd_tover_cr() + "'" +
            ", lcy_mtd_tover_cr='" + getLcy_mtd_tover_cr() + "'" +
            ", acy_accrued_dr_ic='" + getAcy_accrued_dr_ic() + "'" +
            ", acy_accrued_cr_ic='" + getAcy_accrued_cr_ic() + "'" +
            ", date_last_cr_activity='" + getDate_last_cr_activity() + "'" +
            ", date_last_dr_activity='" + getDate_last_dr_activity() + "'" +
            ", date_last_dr='" + getDate_last_dr() + "'" +
            ", date_last_cr='" + getDate_last_cr() + "'" +
            ", acy_uncollected='" + getAcy_uncollected() + "'" +
            ", tod_start_date='" + getTod_start_date() + "'" +
            ", tod_end_date='" + getTod_end_date() + "'" +
            ", dormancy_date='" + getDormancy_date() + "'" +
            ", dormancy_days='" + getDormancy_days() + "'" +
            ", has_tov='" + getHas_tov() + "'" +
            ", last_ccy_conv_date='" + getLast_ccy_conv_date() + "'" +
            ", address1='" + getAddress1() + "'" +
            ", address2='" + getAddress2() + "'" +
            ", address3='" + getAddress3() + "'" +
            ", address4='" + getAddress4() + "'" +
            ", type_of_chq='" + getType_of_chq() + "'" +
            ", atm_cust_ac_no='" + getAtm_cust_ac_no() + "'" +
            ", atm_dly_amt_limit='" + getAtm_dly_amt_limit() + "'" +
            ", atm_dly_count_limit='" + getAtm_dly_count_limit() + "'" +
            ", gen_stmt_only_on_mvmt='" + getGen_stmt_only_on_mvmt() + "'" +
            ", ac_stat_de_post='" + getAc_stat_de_post() + "'" +
            ", display_iban_in_advices='" + getDisplay_iban_in_advices() + "'" +
            ", clearing_bank_code='" + getClearing_bank_code() + "'" +
            ", clearing_ac_no='" + getClearing_ac_no() + "'" +
            ", iban_ac_no='" + getIban_ac_no() + "'" +
            ", reg_cc_availability='" + getReg_cc_availability() + "'" +
            ", reg_cc_available_funds='" + getReg_cc_available_funds() + "'" +
            ", prev_ac_srno_printed_in_pbk='" + getPrev_ac_srno_printed_in_pbk() + "'" +
            ", latest_srno_submitted='" + getLatest_srno_submitted() + "'" +
            ", prev_runbal_printed_in_pbk='" + getPrev_runbal_printed_in_pbk() + "'" +
            ", latest_runbal_submmited='" + getLatest_runbal_submmited() + "'" +
            ", prev_page_no='" + getPrev_page_no() + "'" +
            ", prev_line_no='" + getPrev_line_no() + "'" +
            ", mt210_reqd='" + getMt210_reqd() + "'" +
            ", acc_stmt_type2='" + getAcc_stmt_type2() + "'" +
            ", acc_stmt_day2='" + getAcc_stmt_day2() + "'" +
            ", ac_stmt_cycle2='" + getAc_stmt_cycle2() + "'" +
            ", previous_statement_date2='" + getPrevious_statement_date2() + "'" +
            ", previous_statement_balance2='" + getPrevious_statement_balance2() + "'" +
            ", previous_statement_no2='" + getPrevious_statement_no2() + "'" +
            ", gen_stmt_only_on_mvmt2='" + getGen_stmt_only_on_mvmt2() + "'" +
            ", acc_stmt_type3='" + getAcc_stmt_type3() + "'" +
            ", acc_stmt_day3='" + getAcc_stmt_day3() + "'" +
            ", ac_stmt_cycle3='" + getAc_stmt_cycle3() + "'" +
            ", previous_statement_date3='" + getPrevious_statement_date3() + "'" +
            ", previous_statement_balance3='" + getPrevious_statement_balance3() + "'" +
            ", previous_statement_no3='" + getPrevious_statement_no3() + "'" +
            ", gen_stmt_only_on_mvmt3='" + getGen_stmt_only_on_mvmt3() + "'" +
            ", sweep_type='" + getSweep_type() + "'" +
            ", master_account_no='" + getMaster_account_no() + "'" +
            ", auto_deposits_bal='" + getAuto_deposits_bal() + "'" +
            ", cas_customer='" + getCas_customer() + "'" +
            ", account_type='" + getAccount_type() + "'" +
            ", min_reqd_bal='" + getMin_reqd_bal() + "'" +
            ", positive_pay_ac='" + getPositive_pay_ac() + "'" +
            ", stale_days='" + getStale_days() + "'" +
            ", cr_auto_ex_rate_lmt='" + getCr_auto_ex_rate_lmt() + "'" +
            ", dr_auto_ex_rate_lmt='" + getDr_auto_ex_rate_lmt() + "'" +
            ", track_receivable='" + getTrack_receivable() + "'" +
            ", receivable_amount='" + getReceivable_amount() + "'" +
            ", product_list='" + getProduct_list() + "'" +
            ", txn_code_list='" + getTxn_code_list() + "'" +
            ", special_condition_product='" + getSpecial_condition_product() + "'" +
            ", special_condition_txncode='" + getSpecial_condition_txncode() + "'" +
            ", reg_d_applicable='" + getReg_d_applicable() + "'" +
            ", regd_periodicity='" + getRegd_periodicity() + "'" +
            ", regd_start_date='" + getRegd_start_date() + "'" +
            ", regd_end_date='" + getRegd_end_date() + "'" +
            ", td_cert_printed='" + getTd_cert_printed() + "'" +
            ", checkbook_name_1='" + getCheckbook_name_1() + "'" +
            ", checkbook_name_2='" + getCheckbook_name_2() + "'" +
            ", auto_reorder_check_required='" + getAuto_reorder_check_required() + "'" +
            ", auto_reorder_check_level='" + getAuto_reorder_check_level() + "'" +
            ", auto_reorder_check_leaves='" + getAuto_reorder_check_leaves() + "'" +
            ", netting_required='" + getNetting_required() + "'" +
            ", referral_required='" + getReferral_required() + "'" +
            ", lodgement_book_facility='" + getLodgement_book_facility() + "'" +
            ", acc_status='" + getAcc_status() + "'" +
            ", status_since='" + getStatus_since() + "'" +
            ", inherit_reporting='" + getInherit_reporting() + "'" +
            ", overdraft_since='" + getOverdraft_since() + "'" +
            ", prev_ovd_date='" + getPrev_ovd_date() + "'" +
            ", status_change_automatic='" + getStatus_change_automatic() + "'" +
            ", overline_od_since='" + getOverline_od_since() + "'" +
            ", tod_since='" + getTod_since() + "'" +
            ", prev_tod_since='" + getPrev_tod_since() + "'" +
            ", dormant_param='" + getDormant_param() + "'" +
            ", dr_int_due='" + getDr_int_due() + "'" +
            ", excl_sameday_rvrtrns_fm_stmt='" + getExcl_sameday_rvrtrns_fm_stmt() + "'" +
            ", allow_back_period_entry='" + getAllow_back_period_entry() + "'" +
            ", auto_prov_reqd='" + getAuto_prov_reqd() + "'" +
            ", exposure_category='" + getExposure_category() + "'" +
            ", risk_free_exp_amount='" + getRisk_free_exp_amount() + "'" +
            ", provision_amount='" + getProvision_amount() + "'" +
            ", credit_txn_limit='" + getCredit_txn_limit() + "'" +
            ", cr_lm_start_date='" + getCr_lm_start_date() + "'" +
            ", cr_lm_rev_date='" + getCr_lm_rev_date() + "'" +
            ", statement_account='" + getStatement_account() + "'" +
            ", account_derived_status='" + getAccount_derived_status() + "'" +
            ", prov_ccy_type='" + getProv_ccy_type() + "'" +
            ", chg_due='" + getChg_due() + "'" +
            ", withdrawable_uncolled_fund='" + getWithdrawable_uncolled_fund() + "'" +
            ", defer_recon='" + getDefer_recon() + "'" +
            ", consolidation_reqd='" + getConsolidation_reqd() + "'" +
            ", funding='" + getFunding() + "'" +
            ", funding_branch='" + getFunding_branch() + "'" +
            ", funding_account='" + getFunding_account() + "'" +
            ", mod9_validation_reqd='" + getMod9_validation_reqd() + "'" +
            ", validation_digit='" + getValidation_digit() + "'" +
            ", media='" + getMedia() + "'" +
            ", acc_tanked_stat='" + getAcc_tanked_stat() + "'" +
            ", gen_interim_stmt='" + getGen_interim_stmt() + "'" +
            ", gen_interim_stmt_on_mvmt='" + getGen_interim_stmt_on_mvmt() + "'" +
            ", gen_balance_report='" + getGen_balance_report() + "'" +
            ", interim_report_since='" + getInterim_report_since() + "'" +
            ", interim_report_type='" + getInterim_report_type() + "'" +
            ", balance_report_since='" + getBalance_report_since() + "'" +
            ", balance_report_type='" + getBalance_report_type() + "'" +
            ", interim_debit_amt='" + getInterim_debit_amt() + "'" +
            ", interim_credit_amt='" + getInterim_credit_amt() + "'" +
            ", interim_stmt_day_count='" + getInterim_stmt_day_count() + "'" +
            ", interim_stmt_ytd_count='" + getInterim_stmt_ytd_count() + "'" +
            ", mode_of_operation='" + getMode_of_operation() + "'" +
            ", inf_acc_opening_amt='" + getInf_acc_opening_amt() + "'" +
            ", inf_pay_in_option='" + getInf_pay_in_option() + "'" +
            ", inf_offset_branch='" + getInf_offset_branch() + "'" +
            ", inf_offset_account='" + getInf_offset_account() + "'" +
            ", inf_waive_acc_open_charge='" + getInf_waive_acc_open_charge() + "'" +
            ", daylight_limit_amount='" + getDaylight_limit_amount() + "'" +
            ", trnover_lmt_code='" + getTrnover_lmt_code() + "'" +
            ", passbook_numeric='" + getPassbook_numeric() + "'" +
            ", country_code='" + getCountry_code() + "'" +
            ", consol_chg_acc='" + getConsol_chg_acc() + "'" +
            ", escrow_transfer='" + getEscrow_transfer() + "'" +
            ", escrow_branch_code='" + getEscrow_branch_code() + "'" +
            ", escrow_ac_no='" + getEscrow_ac_no() + "'" +
            ", escrow_percentage='" + getEscrow_percentage() + "'" +
            ", sod_notification_percent='" + getSod_notification_percent() + "'" +
            ", salary_account='" + getSalary_account() + "'" +
            ", repl_cust_sig='" + getRepl_cust_sig() + "'" +
            ", max_no_cheque_rejections='" + getMax_no_cheque_rejections() + "'" +
            ", no_cheque_rejections='" + getNo_cheque_rejections() + "'" +
            ", fund_id='" + getFund_id() + "'" +
            ", linked_dep_branch='" + getLinked_dep_branch() + "'" +
            ", linked_dep_acc='" + getLinked_dep_acc() + "'" +
            ", mudarabah_accounts='" + getMudarabah_accounts() + "'" +
            ", zakat_exemption='" + getZakat_exemption() + "'" +
            ", account_auto_closed='" + getAccount_auto_closed() + "'" +
            ", consol_chg_brn='" + getConsol_chg_brn() + "'" +
            ", no_of_chq_rej_reset_on='" + getNo_of_chq_rej_reset_on() + "'" +
            ", crs_stat_reqd='" + getCrs_stat_reqd() + "'" +
            ", contribute_to_pdm='" + getContribute_to_pdm() + "'" +
            ", exclude_from_distribution='" + getExclude_from_distribution() + "'" +
            ", ac_set_close='" + getAc_set_close() + "'" +
            ", ac_set_close_date='" + getAc_set_close_date() + "'" +
            ", project_account='" + getProject_account() + "'" +
            ", acy_sweep_ineligible='" + getAcy_sweep_ineligible() + "'" +
            ", mt110_recon_reqd='" + getMt110_recon_reqd() + "'" +
            ", sweep_required='" + getSweep_required() + "'" +
            ", auto_deposit='" + getAuto_deposit() + "'" +
            ", sweep_out='" + getSweep_out() + "'" +
            ", auto_create_pool='" + getAuto_create_pool() + "'" +
            ", last_purge_date='" + getLast_purge_date() + "'" +
            ", default_waiver='" + getDefault_waiver() + "'" +
            ", auto_dc_request='" + getAuto_dc_request() + "'" +
            ", auto_chqbk_request='" + getAuto_chqbk_request() + "'" +
            ", nsf_blacklist_status='" + getNsf_blacklist_status() + "'" +
            ", nsf_auto_update='" + getNsf_auto_update() + "'" +
            ", intermediary_required='" + getIntermediary_required() + "'" +
            "}";
    }

}
