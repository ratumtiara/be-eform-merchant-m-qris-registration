package com.bmi.mcb.dummy.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "sttm_cust_personal",schema = "public")
public class sttm_cust_personal {
    @Id
    private String customer_no;

    private String customer_prefix;
    private String first_name;
    private String middle_name;
    private String last_name;
    private Date date_of_birth;
    private String legal_guardian;
    private char minor;
    private char sex;
    private String p_national_id;
    private String passport_no;
    private Date ppt_iss_date;
    private Date ppt_exp_date;
    private String d_address1;
    private String d_address2;
    private String d_address3;
    private String telephone;
    private String fax;
    private String e_mail;
    private String p_address1;
    private String p_address3;
    private String p_address2;
    private String d_country;
    private String p_country;
    private char resident_status;
    private String customer_prefix1;
    private String customer_prefix2;
    private String mobile_number;
    private String age_proof_submitted;
    private String cust_comm_mode;

    public String getCustomer_no() {
        return this.customer_no;
    }

    public void setCustomer_no(String customer_no) {
        this.customer_no = customer_no;
    }

    public String getCustomer_prefix() {
        return this.customer_prefix;
    }

    public void setCustomer_prefix(String customer_prefix) {
        this.customer_prefix = customer_prefix;
    }

    public String getFirst_name() {
        return this.first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getMiddle_name() {
        return this.middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public String getLast_name() {
        return this.last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public Date getDate_of_birth() {
        return this.date_of_birth;
    }

    public void setDate_of_birth(Date date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getLegal_guardian() {
        return this.legal_guardian;
    }

    public void setLegal_guardian(String legal_guardian) {
        this.legal_guardian = legal_guardian;
    }

    public char getMinor() {
        return this.minor;
    }

    public void setMinor(char minor) {
        this.minor = minor;
    }

    public char getSex() {
        return this.sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public String getP_national_id() {
        return this.p_national_id;
    }

    public void setP_national_id(String p_national_id) {
        this.p_national_id = p_national_id;
    }

    public String getPassport_no() {
        return this.passport_no;
    }

    public void setPassport_no(String passport_no) {
        this.passport_no = passport_no;
    }

    public Date getPpt_iss_date() {
        return this.ppt_iss_date;
    }

    public void setPpt_iss_date(Date ppt_iss_date) {
        this.ppt_iss_date = ppt_iss_date;
    }

    public Date getPpt_exp_date() {
        return this.ppt_exp_date;
    }

    public void setPpt_exp_date(Date ppt_exp_date) {
        this.ppt_exp_date = ppt_exp_date;
    }

    public String getD_address1() {
        return this.d_address1;
    }

    public void setD_address1(String d_address1) {
        this.d_address1 = d_address1;
    }

    public String getD_address2() {
        return this.d_address2;
    }

    public void setD_address2(String d_address2) {
        this.d_address2 = d_address2;
    }

    public String getD_address3() {
        return this.d_address3;
    }

    public void setD_address3(String d_address3) {
        this.d_address3 = d_address3;
    }

    public String getTelephone() {
        return this.telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getFax() {
        return this.fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getE_mail() {
        return this.e_mail;
    }

    public void setE_mail(String e_mail) {
        this.e_mail = e_mail;
    }

    public String getP_address1() {
        return this.p_address1;
    }

    public void setP_address1(String p_address1) {
        this.p_address1 = p_address1;
    }

    public String getP_address3() {
        return this.p_address3;
    }

    public void setP_address3(String p_address3) {
        this.p_address3 = p_address3;
    }

    public String getP_address2() {
        return this.p_address2;
    }

    public void setP_address2(String p_address2) {
        this.p_address2 = p_address2;
    }

    public String getD_country() {
        return this.d_country;
    }

    public void setD_country(String d_country) {
        this.d_country = d_country;
    }

    public String getP_country() {
        return this.p_country;
    }

    public void setP_country(String p_country) {
        this.p_country = p_country;
    }

    public char getResident_status() {
        return this.resident_status;
    }

    public void setResident_status(char resident_status) {
        this.resident_status = resident_status;
    }

    public String getCustomer_prefix1() {
        return this.customer_prefix1;
    }

    public void setCustomer_prefix1(String customer_prefix1) {
        this.customer_prefix1 = customer_prefix1;
    }

    public String getCustomer_prefix2() {
        return this.customer_prefix2;
    }

    public void setCustomer_prefix2(String customer_prefix2) {
        this.customer_prefix2 = customer_prefix2;
    }

    public String getMobile_number() {
        return this.mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getAge_proof_submitted() {
        return this.age_proof_submitted;
    }

    public void setAge_proof_submitted(String age_proof_submitted) {
        this.age_proof_submitted = age_proof_submitted;
    }

    public String getCust_comm_mode() {
        return this.cust_comm_mode;
    }

    public void setCust_comm_mode(String cust_comm_mode) {
        this.cust_comm_mode = cust_comm_mode;
    }

    public sttm_cust_personal() {
    }

    public sttm_cust_personal(String customer_no, String customer_prefix, String first_name, String middle_name, String last_name, Date date_of_birth, String legal_guardian, char minor, char sex, String p_national_id, String passport_no, Date ppt_iss_date, Date ppt_exp_date, String d_address1, String d_address2, String d_address3, String telephone, String fax, String e_mail, String p_address1, String p_address3, String p_address2, String d_country, String p_country, char resident_status, String customer_prefix1, String customer_prefix2, String mobile_number, String age_proof_submitted, String cust_comm_mode) {
        this.customer_no = customer_no;
        this.customer_prefix = customer_prefix;
        this.first_name = first_name;
        this.middle_name = middle_name;
        this.last_name = last_name;
        this.date_of_birth = date_of_birth;
        this.legal_guardian = legal_guardian;
        this.minor = minor;
        this.sex = sex;
        this.p_national_id = p_national_id;
        this.passport_no = passport_no;
        this.ppt_iss_date = ppt_iss_date;
        this.ppt_exp_date = ppt_exp_date;
        this.d_address1 = d_address1;
        this.d_address2 = d_address2;
        this.d_address3 = d_address3;
        this.telephone = telephone;
        this.fax = fax;
        this.e_mail = e_mail;
        this.p_address1 = p_address1;
        this.p_address3 = p_address3;
        this.p_address2 = p_address2;
        this.d_country = d_country;
        this.p_country = p_country;
        this.resident_status = resident_status;
        this.customer_prefix1 = customer_prefix1;
        this.customer_prefix2 = customer_prefix2;
        this.mobile_number = mobile_number;
        this.age_proof_submitted = age_proof_submitted;
        this.cust_comm_mode = cust_comm_mode;
    }

    @Override
    public String toString() {
        return "{" +
            " customer_no='" + getCustomer_no() + "'" +
            ", customer_prefix='" + getCustomer_prefix() + "'" +
            ", first_name='" + getFirst_name() + "'" +
            ", middle_name='" + getMiddle_name() + "'" +
            ", last_name='" + getLast_name() + "'" +
            ", date_of_birth='" + getDate_of_birth() + "'" +
            ", legal_guardian='" + getLegal_guardian() + "'" +
            ", minor='" + getMinor() + "'" +
            ", sex='" + getSex() + "'" +
            ", p_national_id='" + getP_national_id() + "'" +
            ", passport_no='" + getPassport_no() + "'" +
            ", ppt_iss_date='" + getPpt_iss_date() + "'" +
            ", ppt_exp_date='" + getPpt_exp_date() + "'" +
            ", d_address1='" + getD_address1() + "'" +
            ", d_address2='" + getD_address2() + "'" +
            ", d_address3='" + getD_address3() + "'" +
            ", telephone='" + getTelephone() + "'" +
            ", fax='" + getFax() + "'" +
            ", e_mail='" + getE_mail() + "'" +
            ", p_address1='" + getP_address1() + "'" +
            ", p_address3='" + getP_address3() + "'" +
            ", p_address2='" + getP_address2() + "'" +
            ", d_country='" + getD_country() + "'" +
            ", p_country='" + getP_country() + "'" +
            ", resident_status='" + getResident_status() + "'" +
            ", customer_prefix1='" + getCustomer_prefix1() + "'" +
            ", customer_prefix2='" + getCustomer_prefix2() + "'" +
            ", mobile_number='" + getMobile_number() + "'" +
            ", age_proof_submitted='" + getAge_proof_submitted() + "'" +
            ", cust_comm_mode='" + getCust_comm_mode() + "'" +
            "}";
    }


}
