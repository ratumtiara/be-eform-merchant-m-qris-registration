package com.bmi.mcb.dummy.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import java.math.BigDecimal;
@Entity
@Table(name = "sttm_customer", schema="public")
public class sttm_customer {
@Id
private String customer_no;

private Character customer_type;
private String customer_name1;
private String address_line1;
private String address_line3;
private String address_line2;
private String address_line4;
private String country;
private String short_name;
private String nationality;
//private String language;
private String exposure_country;
private String local_branch;
private String liability_no;
private String unique_id_name;
private String unique_id_value;
private Character frozen;
private Character deceased;
private Character whereabouts_unknown;
private String customer_category;
private String ho_ac_no;
private Character record_stat;
private Character auth_stat;
private BigDecimal mod_no;
private String maker_id;
private Date maker_dt_stamp;
private String checker_id;
private Date checker_dt_stamp;
private Character once_auth;
private BigDecimal fx_cust_clean_risk_limit;
private BigDecimal overall_limit;
private BigDecimal fx_clean_risk_limit;
private String credit_rating;
private Date revision_date;
private String limit_ccy;
private Character cas_cust;
private String liab_node;
private BigDecimal sec_cust_clean_risk_limit;
private BigDecimal sec_clean_risk_limit;
private BigDecimal sec_cust_pstl_risk_limit;
private BigDecimal sec_pstl_risk_limit;
private String liab_br;
private String past_due_flag;
private String default_media;
private String ssn;
private String swift_code;
private String loc_code;
private String short_name2;
private String utility_provider;
private String utility_provider_id;
private String risk_profile;
private String debtor_category;
private String full_name;
private String udf_1;
private String udf_2;
private String udf_3;
private String udf_4;
private String udf_5;
private Character aml_required;
private String aml_customer_grp;
private Character mailers_required;
private String group_code;
private String exposure_category;
private String cust_classification;
private String cif_status;
private Date cif_status_since;
private String charge_group;
private String introducer;
private String cust_clg_group;
private String chk_digit_valid_reqd;
private String alg_id;
private String ft_accting_as_of;
private String unadvised;
private String tax_group;
private String consol_tax_cert_reqd;
private String individual_tax_cert_reqd;
private String cls_ccy_allowed;
private String cls_participant;
private String fx_netting_customer;
private String risk_category;
private String fax_number;
private String ext_ref_no;
private String crm_customer;
private String issuer_customer;
private String treasury_customer;
private Date cif_creation_date;
private BigDecimal wht_pct;
private String rp_customer;
private String generate_mt920;
private String kyc_details;
private String staff;
private String kyc_ref_no;
private String utility_provider_type;
private String joint_venture;
private String jv_limit_tracking;
private String private_customer;
private BigDecimal lc_collateral_pct;
private String elcm_customer;
private String elcm_customer_no;
private Character auto_create_account;
private String auto_cust_ac_no;
private Character track_limits;
private String ar_ap_tracking;
private Date date_of_birth;


    public String getCustomer_no() {
        return this.customer_no;
    }

    public void setCustomer_no(String customer_no) {
        this.customer_no = customer_no;
    }

    public Character getCustomer_type() {
        return this.customer_type;
    }

    public void setCustomer_type(Character customer_type) {
        this.customer_type = customer_type;
    }

    public String getCustomer_name1() {
        return this.customer_name1;
    }

    public void setCustomer_name1(String customer_name1) {
        this.customer_name1 = customer_name1;
    }

    public String getAddress_line1() {
        return this.address_line1;
    }

    public void setAddress_line1(String address_line1) {
        this.address_line1 = address_line1;
    }

    public String getAddress_line3() {
        return this.address_line3;
    }

    public void setAddress_line3(String address_line3) {
        this.address_line3 = address_line3;
    }

    public String getAddress_line2() {
        return this.address_line2;
    }

    public void setAddress_line2(String address_line2) {
        this.address_line2 = address_line2;
    }

    public String getAddress_line4() {
        return this.address_line4;
    }

    public void setAddress_line4(String address_line4) {
        this.address_line4 = address_line4;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getShort_name() {
        return this.short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public String getNationality() {
        return this.nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getExposure_country() {
        return this.exposure_country;
    }

    public void setExposure_country(String exposure_country) {
        this.exposure_country = exposure_country;
    }

    public String getLocal_branch() {
        return this.local_branch;
    }

    public void setLocal_branch(String local_branch) {
        this.local_branch = local_branch;
    }

    public String getLiability_no() {
        return this.liability_no;
    }

    public void setLiability_no(String liability_no) {
        this.liability_no = liability_no;
    }

    public String getUnique_id_name() {
        return this.unique_id_name;
    }

    public void setUnique_id_name(String unique_id_name) {
        this.unique_id_name = unique_id_name;
    }

    public String getUnique_id_value() {
        return this.unique_id_value;
    }

    public void setUnique_id_value(String unique_id_value) {
        this.unique_id_value = unique_id_value;
    }

    public Character getFrozen() {
        return this.frozen;
    }

    public void setFrozen(Character frozen) {
        this.frozen = frozen;
    }

    public Character getDeceased() {
        return this.deceased;
    }

    public void setDeceased(Character deceased) {
        this.deceased = deceased;
    }

    public Character getWhereabouts_unknown() {
        return this.whereabouts_unknown;
    }

    public void setWhereabouts_unknown(Character whereabouts_unknown) {
        this.whereabouts_unknown = whereabouts_unknown;
    }

    public String getCustomer_category() {
        return this.customer_category;
    }

    public void setCustomer_category(String customer_category) {
        this.customer_category = customer_category;
    }

    public String getHo_ac_no() {
        return this.ho_ac_no;
    }

    public void setHo_ac_no(String ho_ac_no) {
        this.ho_ac_no = ho_ac_no;
    }

    public Character getRecord_stat() {
        return this.record_stat;
    }

    public void setRecord_stat(Character record_stat) {
        this.record_stat = record_stat;
    }

    public Character getAuth_stat() {
        return this.auth_stat;
    }

    public void setAuth_stat(Character auth_stat) {
        this.auth_stat = auth_stat;
    }

    public BigDecimal getMod_no() {
        return this.mod_no;
    }

    public void setMod_no(BigDecimal mod_no) {
        this.mod_no = mod_no;
    }

    public String getMaker_id() {
        return this.maker_id;
    }

    public void setMaker_id(String maker_id) {
        this.maker_id = maker_id;
    }

    public Date getMaker_dt_stamp() {
        return this.maker_dt_stamp;
    }

    public void setMaker_dt_stamp(Date maker_dt_stamp) {
        this.maker_dt_stamp = maker_dt_stamp;
    }

    public String getChecker_id() {
        return this.checker_id;
    }

    public void setChecker_id(String checker_id) {
        this.checker_id = checker_id;
    }

    public Date getChecker_dt_stamp() {
        return this.checker_dt_stamp;
    }

    public void setChecker_dt_stamp(Date checker_dt_stamp) {
        this.checker_dt_stamp = checker_dt_stamp;
    }

    public Character getOnce_auth() {
        return this.once_auth;
    }

    public void setOnce_auth(Character once_auth) {
        this.once_auth = once_auth;
    }

    public BigDecimal getFx_cust_clean_risk_limit() {
        return this.fx_cust_clean_risk_limit;
    }

    public void setFx_cust_clean_risk_limit(BigDecimal fx_cust_clean_risk_limit) {
        this.fx_cust_clean_risk_limit = fx_cust_clean_risk_limit;
    }

    public BigDecimal getOverall_limit() {
        return this.overall_limit;
    }

    public void setOverall_limit(BigDecimal overall_limit) {
        this.overall_limit = overall_limit;
    }

    public BigDecimal getFx_clean_risk_limit() {
        return this.fx_clean_risk_limit;
    }

    public void setFx_clean_risk_limit(BigDecimal fx_clean_risk_limit) {
        this.fx_clean_risk_limit = fx_clean_risk_limit;
    }

    public String getCredit_rating() {
        return this.credit_rating;
    }

    public void setCredit_rating(String credit_rating) {
        this.credit_rating = credit_rating;
    }

    public Date getRevision_date() {
        return this.revision_date;
    }

    public void setRevision_date(Date revision_date) {
        this.revision_date = revision_date;
    }

    public String getLimit_ccy() {
        return this.limit_ccy;
    }

    public void setLimit_ccy(String limit_ccy) {
        this.limit_ccy = limit_ccy;
    }

    public Character getCas_cust() {
        return this.cas_cust;
    }

    public void setCas_cust(Character cas_cust) {
        this.cas_cust = cas_cust;
    }

    public String getLiab_node() {
        return this.liab_node;
    }

    public void setLiab_node(String liab_node) {
        this.liab_node = liab_node;
    }

    public BigDecimal getSec_cust_clean_risk_limit() {
        return this.sec_cust_clean_risk_limit;
    }

    public void setSec_cust_clean_risk_limit(BigDecimal sec_cust_clean_risk_limit) {
        this.sec_cust_clean_risk_limit = sec_cust_clean_risk_limit;
    }

    public BigDecimal getSec_clean_risk_limit() {
        return this.sec_clean_risk_limit;
    }

    public void setSec_clean_risk_limit(BigDecimal sec_clean_risk_limit) {
        this.sec_clean_risk_limit = sec_clean_risk_limit;
    }

    public BigDecimal getSec_cust_pstl_risk_limit() {
        return this.sec_cust_pstl_risk_limit;
    }

    public void setSec_cust_pstl_risk_limit(BigDecimal sec_cust_pstl_risk_limit) {
        this.sec_cust_pstl_risk_limit = sec_cust_pstl_risk_limit;
    }

    public BigDecimal getSec_pstl_risk_limit() {
        return this.sec_pstl_risk_limit;
    }

    public void setSec_pstl_risk_limit(BigDecimal sec_pstl_risk_limit) {
        this.sec_pstl_risk_limit = sec_pstl_risk_limit;
    }

    public String getLiab_br() {
        return this.liab_br;
    }

    public void setLiab_br(String liab_br) {
        this.liab_br = liab_br;
    }

    public String getPast_due_flag() {
        return this.past_due_flag;
    }

    public void setPast_due_flag(String past_due_flag) {
        this.past_due_flag = past_due_flag;
    }

    public String getDefault_media() {
        return this.default_media;
    }

    public void setDefault_media(String default_media) {
        this.default_media = default_media;
    }

    public String getSsn() {
        return this.ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getSwift_code() {
        return this.swift_code;
    }

    public void setSwift_code(String swift_code) {
        this.swift_code = swift_code;
    }

    public String getLoc_code() {
        return this.loc_code;
    }

    public void setLoc_code(String loc_code) {
        this.loc_code = loc_code;
    }

    public String getShort_name2() {
        return this.short_name2;
    }

    public void setShort_name2(String short_name2) {
        this.short_name2 = short_name2;
    }

    public String getUtility_provider() {
        return this.utility_provider;
    }

    public void setUtility_provider(String utility_provider) {
        this.utility_provider = utility_provider;
    }

    public String getUtility_provider_id() {
        return this.utility_provider_id;
    }

    public void setUtility_provider_id(String utility_provider_id) {
        this.utility_provider_id = utility_provider_id;
    }

    public String getRisk_profile() {
        return this.risk_profile;
    }

    public void setRisk_profile(String risk_profile) {
        this.risk_profile = risk_profile;
    }

    public String getDebtor_category() {
        return this.debtor_category;
    }

    public void setDebtor_category(String debtor_category) {
        this.debtor_category = debtor_category;
    }

    public String getFull_name() {
        return this.full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getUdf_1() {
        return this.udf_1;
    }

    public void setUdf_1(String udf_1) {
        this.udf_1 = udf_1;
    }

    public String getUdf_2() {
        return this.udf_2;
    }

    public void setUdf_2(String udf_2) {
        this.udf_2 = udf_2;
    }

    public String getUdf_3() {
        return this.udf_3;
    }

    public void setUdf_3(String udf_3) {
        this.udf_3 = udf_3;
    }

    public String getUdf_4() {
        return this.udf_4;
    }

    public void setUdf_4(String udf_4) {
        this.udf_4 = udf_4;
    }

    public String getUdf_5() {
        return this.udf_5;
    }

    public void setUdf_5(String udf_5) {
        this.udf_5 = udf_5;
    }

    public Character getAml_required() {
        return this.aml_required;
    }

    public void setAml_required(Character aml_required) {
        this.aml_required = aml_required;
    }

    public String getAml_customer_grp() {
        return this.aml_customer_grp;
    }

    public void setAml_customer_grp(String aml_customer_grp) {
        this.aml_customer_grp = aml_customer_grp;
    }

    public Character getMailers_required() {
        return this.mailers_required;
    }

    public void setMailers_required(Character mailers_required) {
        this.mailers_required = mailers_required;
    }

    public String getGroup_code() {
        return this.group_code;
    }

    public void setGroup_code(String group_code) {
        this.group_code = group_code;
    }

    public String getExposure_category() {
        return this.exposure_category;
    }

    public void setExposure_category(String exposure_category) {
        this.exposure_category = exposure_category;
    }

    public String getCust_classification() {
        return this.cust_classification;
    }

    public void setCust_classification(String cust_classification) {
        this.cust_classification = cust_classification;
    }

    public String getCif_status() {
        return this.cif_status;
    }

    public void setCif_status(String cif_status) {
        this.cif_status = cif_status;
    }

    public Date getCif_status_since() {
        return this.cif_status_since;
    }

    public void setCif_status_since(Date cif_status_since) {
        this.cif_status_since = cif_status_since;
    }

    public String getCharge_group() {
        return this.charge_group;
    }

    public void setCharge_group(String charge_group) {
        this.charge_group = charge_group;
    }

    public String getIntroducer() {
        return this.introducer;
    }

    public void setIntroducer(String introducer) {
        this.introducer = introducer;
    }

    public String getCust_clg_group() {
        return this.cust_clg_group;
    }

    public void setCust_clg_group(String cust_clg_group) {
        this.cust_clg_group = cust_clg_group;
    }

    public String getChk_digit_valid_reqd() {
        return this.chk_digit_valid_reqd;
    }

    public void setChk_digit_valid_reqd(String chk_digit_valid_reqd) {
        this.chk_digit_valid_reqd = chk_digit_valid_reqd;
    }

    public String getAlg_id() {
        return this.alg_id;
    }

    public void setAlg_id(String alg_id) {
        this.alg_id = alg_id;
    }

    public String getFt_accting_as_of() {
        return this.ft_accting_as_of;
    }

    public void setFt_accting_as_of(String ft_accting_as_of) {
        this.ft_accting_as_of = ft_accting_as_of;
    }

    public String getUnadvised() {
        return this.unadvised;
    }

    public void setUnadvised(String unadvised) {
        this.unadvised = unadvised;
    }

    public String getTax_group() {
        return this.tax_group;
    }

    public void setTax_group(String tax_group) {
        this.tax_group = tax_group;
    }

    public String getConsol_tax_cert_reqd() {
        return this.consol_tax_cert_reqd;
    }

    public void setConsol_tax_cert_reqd(String consol_tax_cert_reqd) {
        this.consol_tax_cert_reqd = consol_tax_cert_reqd;
    }

    public String getIndividual_tax_cert_reqd() {
        return this.individual_tax_cert_reqd;
    }

    public void setIndividual_tax_cert_reqd(String individual_tax_cert_reqd) {
        this.individual_tax_cert_reqd = individual_tax_cert_reqd;
    }

    public String getCls_ccy_allowed() {
        return this.cls_ccy_allowed;
    }

    public void setCls_ccy_allowed(String cls_ccy_allowed) {
        this.cls_ccy_allowed = cls_ccy_allowed;
    }

    public String getCls_participant() {
        return this.cls_participant;
    }

    public void setCls_participant(String cls_participant) {
        this.cls_participant = cls_participant;
    }

    public String getFx_netting_customer() {
        return this.fx_netting_customer;
    }

    public void setFx_netting_customer(String fx_netting_customer) {
        this.fx_netting_customer = fx_netting_customer;
    }

    public String getRisk_category() {
        return this.risk_category;
    }

    public void setRisk_category(String risk_category) {
        this.risk_category = risk_category;
    }

    public String getFax_number() {
        return this.fax_number;
    }

    public void setFax_number(String fax_number) {
        this.fax_number = fax_number;
    }

    public String getExt_ref_no() {
        return this.ext_ref_no;
    }

    public void setExt_ref_no(String ext_ref_no) {
        this.ext_ref_no = ext_ref_no;
    }

    public String getCrm_customer() {
        return this.crm_customer;
    }

    public void setCrm_customer(String crm_customer) {
        this.crm_customer = crm_customer;
    }

    public String getIssuer_customer() {
        return this.issuer_customer;
    }

    public void setIssuer_customer(String issuer_customer) {
        this.issuer_customer = issuer_customer;
    }

    public String getTreasury_customer() {
        return this.treasury_customer;
    }

    public void setTreasury_customer(String treasury_customer) {
        this.treasury_customer = treasury_customer;
    }

    public Date getCif_creation_date() {
        return this.cif_creation_date;
    }

    public void setCif_creation_date(Date cif_creation_date) {
        this.cif_creation_date = cif_creation_date;
    }

    public BigDecimal getWht_pct() {
        return this.wht_pct;
    }

    public void setWht_pct(BigDecimal wht_pct) {
        this.wht_pct = wht_pct;
    }

    public String getRp_customer() {
        return this.rp_customer;
    }

    public void setRp_customer(String rp_customer) {
        this.rp_customer = rp_customer;
    }

    public String getGenerate_mt920() {
        return this.generate_mt920;
    }

    public void setGenerate_mt920(String generate_mt920) {
        this.generate_mt920 = generate_mt920;
    }

    public String getKyc_details() {
        return this.kyc_details;
    }

    public void setKyc_details(String kyc_details) {
        this.kyc_details = kyc_details;
    }

    public String getStaff() {
        return this.staff;
    }

    public void setStaff(String staff) {
        this.staff = staff;
    }

    public String getKyc_ref_no() {
        return this.kyc_ref_no;
    }

    public void setKyc_ref_no(String kyc_ref_no) {
        this.kyc_ref_no = kyc_ref_no;
    }

    public String getUtility_provider_type() {
        return this.utility_provider_type;
    }

    public void setUtility_provider_type(String utility_provider_type) {
        this.utility_provider_type = utility_provider_type;
    }

    public String getJoint_venture() {
        return this.joint_venture;
    }

    public void setJoint_venture(String joint_venture) {
        this.joint_venture = joint_venture;
    }

    public String getJv_limit_tracking() {
        return this.jv_limit_tracking;
    }

    public void setJv_limit_tracking(String jv_limit_tracking) {
        this.jv_limit_tracking = jv_limit_tracking;
    }

    public String getPrivate_customer() {
        return this.private_customer;
    }

    public void setPrivate_customer(String private_customer) {
        this.private_customer = private_customer;
    }

    public BigDecimal getLc_collateral_pct() {
        return this.lc_collateral_pct;
    }

    public void setLc_collateral_pct(BigDecimal lc_collateral_pct) {
        this.lc_collateral_pct = lc_collateral_pct;
    }

    public String getElcm_customer() {
        return this.elcm_customer;
    }

    public void setElcm_customer(String elcm_customer) {
        this.elcm_customer = elcm_customer;
    }

    public String getElcm_customer_no() {
        return this.elcm_customer_no;
    }

    public void setElcm_customer_no(String elcm_customer_no) {
        this.elcm_customer_no = elcm_customer_no;
    }

    public Character getAuto_create_account() {
        return this.auto_create_account;
    }

    public void setAuto_create_account(Character auto_create_account) {
        this.auto_create_account = auto_create_account;
    }

    public String getAuto_cust_ac_no() {
        return this.auto_cust_ac_no;
    }

    public void setAuto_cust_ac_no(String auto_cust_ac_no) {
        this.auto_cust_ac_no = auto_cust_ac_no;
    }

    public Character getTrack_limits() {
        return this.track_limits;
    }

    public void setTrack_limits(Character track_limits) {
        this.track_limits = track_limits;
    }

    public String getAr_ap_tracking() {
        return this.ar_ap_tracking;
    }

    public void setAr_ap_tracking(String ar_ap_tracking) {
        this.ar_ap_tracking = ar_ap_tracking;
    }

    public Date getDate_of_birth() {
        return this.date_of_birth;
    }

    public void setDate_of_birth(Date date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public sttm_customer() {
    }

    public sttm_customer(String customer_no, Character customer_type, String customer_name1, String address_line1, String address_line3, String address_line2, String address_line4, String country, String short_name, String nationality, String exposure_country, String local_branch, String liability_no, String unique_id_name, String unique_id_value, Character frozen, Character deceased, Character whereabouts_unknown, String customer_category, String ho_ac_no, Character record_stat, Character auth_stat, BigDecimal mod_no, String maker_id, Date maker_dt_stamp, String checker_id, Date checker_dt_stamp, Character once_auth, BigDecimal fx_cust_clean_risk_limit, BigDecimal overall_limit, BigDecimal fx_clean_risk_limit, String credit_rating, Date revision_date, String limit_ccy, Character cas_cust, String liab_node, BigDecimal sec_cust_clean_risk_limit, BigDecimal sec_clean_risk_limit, BigDecimal sec_cust_pstl_risk_limit, BigDecimal sec_pstl_risk_limit, String liab_br, String past_due_flag, String default_media, String ssn, String swift_code, String loc_code, String short_name2, String utility_provider, String utility_provider_id, String risk_profile, String debtor_category, String full_name, String udf_1, String udf_2, String udf_3, String udf_4, String udf_5, Character aml_required, String aml_customer_grp, Character mailers_required, String group_code, String exposure_category, String cust_classification, String cif_status, Date cif_status_since, String charge_group, String introducer, String cust_clg_group, String chk_digit_valid_reqd, String alg_id, String ft_accting_as_of, String unadvised, String tax_group, String consol_tax_cert_reqd, String individual_tax_cert_reqd, String cls_ccy_allowed, String cls_participant, String fx_netting_customer, String risk_category, String fax_number, String ext_ref_no, String crm_customer, String issuer_customer, String treasury_customer, Date cif_creation_date, BigDecimal wht_pct, String rp_customer, String generate_mt920, String kyc_details, String staff, String kyc_ref_no, String utility_provider_type, String joint_venture, String jv_limit_tracking, String private_customer, BigDecimal lc_collateral_pct, String elcm_customer, String elcm_customer_no, Character auto_create_account, String auto_cust_ac_no, Character track_limits, String ar_ap_tracking, Date date_of_birth) {
        this.customer_no = customer_no;
        this.customer_type = customer_type;
        this.customer_name1 = customer_name1;
        this.address_line1 = address_line1;
        this.address_line3 = address_line3;
        this.address_line2 = address_line2;
        this.address_line4 = address_line4;
        this.country = country;
        this.short_name = short_name;
        this.nationality = nationality;
        this.exposure_country = exposure_country;
        this.local_branch = local_branch;
        this.liability_no = liability_no;
        this.unique_id_name = unique_id_name;
        this.unique_id_value = unique_id_value;
        this.frozen = frozen;
        this.deceased = deceased;
        this.whereabouts_unknown = whereabouts_unknown;
        this.customer_category = customer_category;
        this.ho_ac_no = ho_ac_no;
        this.record_stat = record_stat;
        this.auth_stat = auth_stat;
        this.mod_no = mod_no;
        this.maker_id = maker_id;
        this.maker_dt_stamp = maker_dt_stamp;
        this.checker_id = checker_id;
        this.checker_dt_stamp = checker_dt_stamp;
        this.once_auth = once_auth;
        this.fx_cust_clean_risk_limit = fx_cust_clean_risk_limit;
        this.overall_limit = overall_limit;
        this.fx_clean_risk_limit = fx_clean_risk_limit;
        this.credit_rating = credit_rating;
        this.revision_date = revision_date;
        this.limit_ccy = limit_ccy;
        this.cas_cust = cas_cust;
        this.liab_node = liab_node;
        this.sec_cust_clean_risk_limit = sec_cust_clean_risk_limit;
        this.sec_clean_risk_limit = sec_clean_risk_limit;
        this.sec_cust_pstl_risk_limit = sec_cust_pstl_risk_limit;
        this.sec_pstl_risk_limit = sec_pstl_risk_limit;
        this.liab_br = liab_br;
        this.past_due_flag = past_due_flag;
        this.default_media = default_media;
        this.ssn = ssn;
        this.swift_code = swift_code;
        this.loc_code = loc_code;
        this.short_name2 = short_name2;
        this.utility_provider = utility_provider;
        this.utility_provider_id = utility_provider_id;
        this.risk_profile = risk_profile;
        this.debtor_category = debtor_category;
        this.full_name = full_name;
        this.udf_1 = udf_1;
        this.udf_2 = udf_2;
        this.udf_3 = udf_3;
        this.udf_4 = udf_4;
        this.udf_5 = udf_5;
        this.aml_required = aml_required;
        this.aml_customer_grp = aml_customer_grp;
        this.mailers_required = mailers_required;
        this.group_code = group_code;
        this.exposure_category = exposure_category;
        this.cust_classification = cust_classification;
        this.cif_status = cif_status;
        this.cif_status_since = cif_status_since;
        this.charge_group = charge_group;
        this.introducer = introducer;
        this.cust_clg_group = cust_clg_group;
        this.chk_digit_valid_reqd = chk_digit_valid_reqd;
        this.alg_id = alg_id;
        this.ft_accting_as_of = ft_accting_as_of;
        this.unadvised = unadvised;
        this.tax_group = tax_group;
        this.consol_tax_cert_reqd = consol_tax_cert_reqd;
        this.individual_tax_cert_reqd = individual_tax_cert_reqd;
        this.cls_ccy_allowed = cls_ccy_allowed;
        this.cls_participant = cls_participant;
        this.fx_netting_customer = fx_netting_customer;
        this.risk_category = risk_category;
        this.fax_number = fax_number;
        this.ext_ref_no = ext_ref_no;
        this.crm_customer = crm_customer;
        this.issuer_customer = issuer_customer;
        this.treasury_customer = treasury_customer;
        this.cif_creation_date = cif_creation_date;
        this.wht_pct = wht_pct;
        this.rp_customer = rp_customer;
        this.generate_mt920 = generate_mt920;
        this.kyc_details = kyc_details;
        this.staff = staff;
        this.kyc_ref_no = kyc_ref_no;
        this.utility_provider_type = utility_provider_type;
        this.joint_venture = joint_venture;
        this.jv_limit_tracking = jv_limit_tracking;
        this.private_customer = private_customer;
        this.lc_collateral_pct = lc_collateral_pct;
        this.elcm_customer = elcm_customer;
        this.elcm_customer_no = elcm_customer_no;
        this.auto_create_account = auto_create_account;
        this.auto_cust_ac_no = auto_cust_ac_no;
        this.track_limits = track_limits;
        this.ar_ap_tracking = ar_ap_tracking;
        this.date_of_birth = date_of_birth;
    }

    @Override
    public String toString() {
        return "{" +
            " customer_no='" + getCustomer_no() + "'" +
            ", customer_type='" + getCustomer_type() + "'" +
            ", customer_name1='" + getCustomer_name1() + "'" +
            ", address_line1='" + getAddress_line1() + "'" +
            ", address_line3='" + getAddress_line3() + "'" +
            ", address_line2='" + getAddress_line2() + "'" +
            ", address_line4='" + getAddress_line4() + "'" +
            ", country='" + getCountry() + "'" +
            ", short_name='" + getShort_name() + "'" +
            ", nationality='" + getNationality() + "'" +
            ", exposure_country='" + getExposure_country() + "'" +
            ", local_branch='" + getLocal_branch() + "'" +
            ", liability_no='" + getLiability_no() + "'" +
            ", unique_id_name='" + getUnique_id_name() + "'" +
            ", unique_id_value='" + getUnique_id_value() + "'" +
            ", frozen='" + getFrozen() + "'" +
            ", deceased='" + getDeceased() + "'" +
            ", whereabouts_unknown='" + getWhereabouts_unknown() + "'" +
            ", customer_category='" + getCustomer_category() + "'" +
            ", ho_ac_no='" + getHo_ac_no() + "'" +
            ", record_stat='" + getRecord_stat() + "'" +
            ", auth_stat='" + getAuth_stat() + "'" +
            ", mod_no='" + getMod_no() + "'" +
            ", maker_id='" + getMaker_id() + "'" +
            ", maker_dt_stamp='" + getMaker_dt_stamp() + "'" +
            ", checker_id='" + getChecker_id() + "'" +
            ", checker_dt_stamp='" + getChecker_dt_stamp() + "'" +
            ", once_auth='" + getOnce_auth() + "'" +
            ", fx_cust_clean_risk_limit='" + getFx_cust_clean_risk_limit() + "'" +
            ", overall_limit='" + getOverall_limit() + "'" +
            ", fx_clean_risk_limit='" + getFx_clean_risk_limit() + "'" +
            ", credit_rating='" + getCredit_rating() + "'" +
            ", revision_date='" + getRevision_date() + "'" +
            ", limit_ccy='" + getLimit_ccy() + "'" +
            ", cas_cust='" + getCas_cust() + "'" +
            ", liab_node='" + getLiab_node() + "'" +
            ", sec_cust_clean_risk_limit='" + getSec_cust_clean_risk_limit() + "'" +
            ", sec_clean_risk_limit='" + getSec_clean_risk_limit() + "'" +
            ", sec_cust_pstl_risk_limit='" + getSec_cust_pstl_risk_limit() + "'" +
            ", sec_pstl_risk_limit='" + getSec_pstl_risk_limit() + "'" +
            ", liab_br='" + getLiab_br() + "'" +
            ", past_due_flag='" + getPast_due_flag() + "'" +
            ", default_media='" + getDefault_media() + "'" +
            ", ssn='" + getSsn() + "'" +
            ", swift_code='" + getSwift_code() + "'" +
            ", loc_code='" + getLoc_code() + "'" +
            ", short_name2='" + getShort_name2() + "'" +
            ", utility_provider='" + getUtility_provider() + "'" +
            ", utility_provider_id='" + getUtility_provider_id() + "'" +
            ", risk_profile='" + getRisk_profile() + "'" +
            ", debtor_category='" + getDebtor_category() + "'" +
            ", full_name='" + getFull_name() + "'" +
            ", udf_1='" + getUdf_1() + "'" +
            ", udf_2='" + getUdf_2() + "'" +
            ", udf_3='" + getUdf_3() + "'" +
            ", udf_4='" + getUdf_4() + "'" +
            ", udf_5='" + getUdf_5() + "'" +
            ", aml_required='" + getAml_required() + "'" +
            ", aml_customer_grp='" + getAml_customer_grp() + "'" +
            ", mailers_required='" + getMailers_required() + "'" +
            ", group_code='" + getGroup_code() + "'" +
            ", exposure_category='" + getExposure_category() + "'" +
            ", cust_classification='" + getCust_classification() + "'" +
            ", cif_status='" + getCif_status() + "'" +
            ", cif_status_since='" + getCif_status_since() + "'" +
            ", charge_group='" + getCharge_group() + "'" +
            ", introducer='" + getIntroducer() + "'" +
            ", cust_clg_group='" + getCust_clg_group() + "'" +
            ", chk_digit_valid_reqd='" + getChk_digit_valid_reqd() + "'" +
            ", alg_id='" + getAlg_id() + "'" +
            ", ft_accting_as_of='" + getFt_accting_as_of() + "'" +
            ", unadvised='" + getUnadvised() + "'" +
            ", tax_group='" + getTax_group() + "'" +
            ", consol_tax_cert_reqd='" + getConsol_tax_cert_reqd() + "'" +
            ", individual_tax_cert_reqd='" + getIndividual_tax_cert_reqd() + "'" +
            ", cls_ccy_allowed='" + getCls_ccy_allowed() + "'" +
            ", cls_participant='" + getCls_participant() + "'" +
            ", fx_netting_customer='" + getFx_netting_customer() + "'" +
            ", risk_category='" + getRisk_category() + "'" +
            ", fax_number='" + getFax_number() + "'" +
            ", ext_ref_no='" + getExt_ref_no() + "'" +
            ", crm_customer='" + getCrm_customer() + "'" +
            ", issuer_customer='" + getIssuer_customer() + "'" +
            ", treasury_customer='" + getTreasury_customer() + "'" +
            ", cif_creation_date='" + getCif_creation_date() + "'" +
            ", wht_pct='" + getWht_pct() + "'" +
            ", rp_customer='" + getRp_customer() + "'" +
            ", generate_mt920='" + getGenerate_mt920() + "'" +
            ", kyc_details='" + getKyc_details() + "'" +
            ", staff='" + getStaff() + "'" +
            ", kyc_ref_no='" + getKyc_ref_no() + "'" +
            ", utility_provider_type='" + getUtility_provider_type() + "'" +
            ", joint_venture='" + getJoint_venture() + "'" +
            ", jv_limit_tracking='" + getJv_limit_tracking() + "'" +
            ", private_customer='" + getPrivate_customer() + "'" +
            ", lc_collateral_pct='" + getLc_collateral_pct() + "'" +
            ", elcm_customer='" + getElcm_customer() + "'" +
            ", elcm_customer_no='" + getElcm_customer_no() + "'" +
            ", auto_create_account='" + getAuto_create_account() + "'" +
            ", auto_cust_ac_no='" + getAuto_cust_ac_no() + "'" +
            ", track_limits='" + getTrack_limits() + "'" +
            ", ar_ap_tracking='" + getAr_ap_tracking() + "'" +
            ", date_of_birth='" + getDate_of_birth() + "'" +
            "}";
    }

}
