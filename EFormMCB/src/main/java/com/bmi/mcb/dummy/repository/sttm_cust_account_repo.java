package com.bmi.mcb.dummy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;
import com.bmi.mcb.dummy.entity.*;

@Repository
public interface sttm_cust_account_repo extends JpaRepository<sttm_cust_account, String> {

    @Query("select a from sttm_cust_account a where a.cust_ac_no=?1")
    List<sttm_cust_account> findByNoRek(String cust_ac_no);
}
