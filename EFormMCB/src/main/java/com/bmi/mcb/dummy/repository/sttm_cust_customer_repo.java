package com.bmi.mcb.dummy.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

import com.bmi.mcb.dummy.entity.*;
public interface sttm_cust_customer_repo extends JpaRepository<sttm_customer, String>{
    @Query("select u from  sttm_customer u where u.customer_no=?1")
    List<sttm_customer> findByCif(String customer_no);
}
