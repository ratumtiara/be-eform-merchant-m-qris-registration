package com.bmi.mcb.dummy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
import com.bmi.mcb.dummy.entity.*;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;

@Repository
public interface sttm_cust_personal_repo extends JpaRepository<sttm_cust_personal, String> {
    @Query("select u from  sttm_cust_personal u where u.customer_no=?1")
    List<sttm_cust_personal> findByCif(String customer_no);
}
