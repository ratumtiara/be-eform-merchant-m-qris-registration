import React, { } from "react";
import './App.css';

import UserForm from './pages/UserForm'
import Login from "./modal/Login";
import ChangePassword from "./modal/ChangePassword"

function App() {
  let isLoginUser = localStorage.getItem('isLoginUser')
  isLoginUser = JSON.parse(isLoginUser)
  let isChangePassword = localStorage.getItem('isChangePassword')
  isChangePassword = JSON.parse(isChangePassword)

  if (isLoginUser == null && isChangePassword == null) {
    return (
      <Login />
    )
  } else if (isChangePassword !== null) {
    return (
      <ChangePassword />
    )
  }

  return (
    <UserForm />)

}

export default App;