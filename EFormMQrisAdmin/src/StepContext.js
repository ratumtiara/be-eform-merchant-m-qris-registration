import React, { useState } from "react";
import App from "./App";

export const multiStepContext = React.createContext()
const StepContext = () => {
    let currentPage = sessionStorage.getItem('currentPage')

    let lastPage = null
    if (currentPage == null) {
        lastPage = 1
    } else if (currentPage != null) {
        lastPage = parseFloat(currentPage)
    }

    const [currentStep, setStep] = useState(1)
    const [userData, setUserdata] = useState([])
    const [finalData, setFinalData] = useState([])
    const [page, setPage] = useState(lastPage);

    function submitData() {
        setFinalData(finalData => [...finalData, userData]);
        setUserdata('')
    }

    return (
        <div>
            <multiStepContext.Provider
                value={{
                    currentStep,
                    setStep,
                    userData,
                    setUserdata,
                    finalData,
                    setFinalData,
                    submitData,
                    page,
                    setPage
                }}>
                <App page={page} />
            </multiStepContext.Provider>

        </div>
    )
}

export default StepContext