import React, { Component } from "react";
import MainMqrisService from "../services/MainMqrisService";
import InQuiryInstructionKPO from "./InquiryInstructionKPO";
import InQuiryInstructionBranch from "./InquiryInstructionBranch";

export default class InQuiryInstruction extends Component {
   constructor(props) {
      super(props);
      this.state = {
         data: null,
      };
   }

   componentDidMount = async () => {
      const dataLogin = await MainMqrisService.getUsername()
      const userJabatan = dataLogin[0].user_jabatan
      if (userJabatan == '3' || userJabatan == '4') {
         this.setState({ data: true })
      } else {
         this.setState({ data: false })
      }
   }

   render() {
      if (this.state.data == true) {
         return (
            <div>
               <InQuiryInstructionKPO />
            </div>
         )
      } else {
         return (
            <div>
               <InQuiryInstructionBranch />
            </div>
         )
      }
   }
}