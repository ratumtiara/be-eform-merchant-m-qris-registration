import React, { Component } from "react";
import Modal from "react-modal";
import ModalTemplate from "../modal/ModalInquiryBranch";
import ReactPaginate from "react-paginate";
import MainMqrisService from "../services/MainMqrisService";

export default class InQuiryInstructionBranch extends Component {
   constructor(props) {
      super(props);
      this.state = {
         offset: 0,
         open: false,
         perPage: 9,
         currentPage: 0,
         data: [],
         nikSearch: ""
      };
      this.handlePageClick = this.handlePageClick.bind(this);
      this.toggleModal = this.toggleModal.bind(props);
   }

   componentDidMount = async () => {
      let dataDB = await this.getData()
      this.setState({ data: dataDB })
      this.renderDatatable();
   }

   handlePageClick = (e) => {
      const selectedPage = e.selected;
      const offset = selectedPage * this.state.perPage;

      this.setState(
         {
            currentPage: selectedPage,
            offset: offset,
         },
         () => {
            this.renderDatatable();
         }
      );
   };

   toggleModal = (props) => {
      this.setState({ open: !this.state.open });
      localStorage.setItem("appId", props);
      localStorage.setItem("id", props);
   };

   getData = async () => {
      const dataLogin = await MainMqrisService.getUsername()
      const user_branch = dataLogin[0].user_branch
      const username = dataLogin[0].username
      const user_jabatan = dataLogin[0].user_jabatan
      const data = await MainMqrisService.getInquiryBranch({ user_branch, username, user_jabatan })
      return data
   }

   getDataSearch = async (props) => {
      if (props !== "") {
         const dataLogin = await MainMqrisService.getUsername()
         const branch = dataLogin[0].user_branch
         const userJabatan = dataLogin[0].user_jabatan
         const paramValue = props.toLowerCase()
         const dataDB = await MainMqrisService.getInquiryBranchFilter({ branch, userJabatan, paramValue })
         this.setState({ data: dataDB })
         this.renderDatatable()
      } else {
         this.getData()
         this.renderDatatable()
         window.location.reload()
      }
   }

   renderDatatable = () => {
      const slice = this.state.data.slice(
         this.state.offset,
         this.state.offset + this.state.perPage
      );

      const postData = slice.map((user) => {
         let status_code_user = null;
         if (user[7] === 1) {
            status_code_user = "To Be Checked";
         } else if (user[7] === 2) {
            status_code_user = "To Be Approved";
         } else if (user[7] === 3) {
            status_code_user = "Ready for Export";
         } else if (user[7] === 4) {
            status_code_user = "Ready to MBSS";
         } else if (user[7] === 5) {
            status_code_user = "Settled";
         } else if (user[7] === 6) {
            status_code_user = "Reject Checked";
         } else if (user[7] === 7) {
            status_code_user = "Reject Approved";
         } else if (user[7] === 12) {
            status_code_user = "Ready Positioning For Tag PAN";
         }
         return (
            <React.Fragment>
               <tr>
                  <td>{user[0]}</td>
                  <td>{user[1]}</td>
                  <td>{user[2]}</td>
                  <td>{user[3]}</td>
                  <td>{user[4]}</td>
                  <td>{user[5]}</td>
                  <td>{user[6]}</td>
                  <td>{status_code_user}</td>
                  <td>
                     <button
                        type="button"
                        class="btn btn-success"
                        onClick={() => this.toggleModal(user[0])}
                     >
                        <i class="fas fa-search"></i>
                     </button>
                  </td>
               </tr>
            </React.Fragment>
         );
      });
      this.setState({
         pageCount: Math.ceil(this.state.data.length / this.state.perPage),
         postData,
      });
   }

   render() {
      return (
         <div className="container-fluid">
            <Modal
               isOpen={this.state.open}
               onRequestClose={this.toggleModal}
               contentLabel="My dialog"
               className="modal-bg"
            >
               <ModalTemplate />
            </Modal>
            <div class="input-group">
               <div class="form-outline form-search">
                  <input id="search-focus" onChange={(e) => { this.getDataSearch(e.target.value) }} type="search" placeholder="Search" class="form-control" />
               </div>
               <button type="button" class="btn btn-primary">
                  <i class="fas fa-search"></i>
               </button>
            </div>
            <div className="card shadow mb-4">
               <div className="card-header py-3">
                  <h6 className="m-0 font-weight-bold text-primary">
                     Inquiry Instruction Branch
                  </h6>
               </div>
               <div className="card-body">
                  <div className="tableFixHead table-responsive">
                     <table id="myTable" className="table table-striped">
                        <thead className="thead-dark">
                           <tr>
                              <th>AppId</th>
                              <th>Merchant Name</th>
                              <th>Merchant Type</th>
                              <th>Merchant Criteria</th>
                              <th>MDR</th>
                              <th>Kantor Cabang</th>
                              <th>Kode Referensi</th>
                              <th>Status</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>{this.state.postData}</tbody>
                     </table>
                  </div>
                  <ReactPaginate
                     previousLabel={"Prev"}
                     nextLabel={"Next"}
                     breakLabel={"..."}
                     breakClassName={"break-me"}
                     pageCount={this.state.pageCount}
                     marginPagesDisplayed={2}
                     pageRangeDisplayed={5}
                     onPageChange={this.handlePageClick}
                     containerClassName={"pagination"}
                     subContainerClassName={"pages pagination"}
                     activeClassName={"active"} />

               </div>
            </div>
         </div>
      );
   }
}