import React, { Component } from "react";
import Modal from "react-modal";
import ModalTemplate from "../modal/ModalInquiryKPO";
import ReactPaginate from "react-paginate";
import MainMqrisService from "../services/MainMqrisService";
import Swal from 'sweetalert2';

let xlsx = require("json-as-xlsx");

export default class InQuiryInstructionKPO extends Component {
   constructor(props) {
      super(props);
      this.state = {
         offset: 0,
         open: false,
         perPage: 9,
         currentPage: 0,
         data: [],
         dataExport: [],
         nikSearch: "",
         userLogin: null
      };
      this.handlePageClick = this.handlePageClick.bind(this);
      this.toggleModal = this.toggleModal.bind(props);
   }

   componentDidMount = async () => {
      let dataDB = await this.getData()
      this.setState({ data: dataDB })
      this.renderDatatable();
   }

   handlePageClick = (e) => {
      const selectedPage = e.selected;
      const offset = selectedPage * this.state.perPage;

      this.setState(
         {
            currentPage: selectedPage,
            offset: offset,
         },
         () => {
            this.renderDatatable();
         }
      );
   };

   toggleModal = (props) => {
      this.setState({ open: !this.state.open });
      localStorage.setItem("appId", props);
      localStorage.setItem("id", props);
   };

   getData = async () => {
      const data = await MainMqrisService.getInquiryKPO()
      return data
   }

   getDataSearch = async (props) => {
      if (props !== "") {
         const dataLogin = await MainMqrisService.getUsername()
         const branch = dataLogin[0].user_branch
         const userJabatan = dataLogin[0].user_jabatan
         const paramValue = props.toLowerCase()
         const dataDB = await MainMqrisService.getInquiryKPOFilter({ branch, userJabatan, paramValue })
         this.setState({ data: dataDB })
         this.renderDatatable()
      } else {
         this.getData()
         this.renderDatatable()
         window.location.reload()
      }
   }

   renderDatatable = () => {
      const slice = this.state.data.slice(
         this.state.offset,
         this.state.offset + this.state.perPage
      );

      const postData = slice.map((user) => {
         let status_code_user = null;
         if (user[6] === 1) {
            status_code_user = "To Be Checked";
         } else if (user[6] === 2) {
            status_code_user = "To Be Approved";
         } else if (user[6] === 3) {
            status_code_user = "Ready for Export";
         } else if (user[6] === 4) {
            status_code_user = "Ready to MBSS";
         } else if (user[6] === 5) {
            status_code_user = "Settled";
         } else if (user[6] === 5) {
            status_code_user = "Reject Checked";
         } else if (user[6] === 6) {
            status_code_user = "Reject Approved";
         } else if (user[6] === 12) {
            status_code_user = "Ready Positioning For Tag PAN";
         }
         return (
            <React.Fragment>
               <tr>
                  <td>{user[0]}</td>
                  <td>{user[1]}</td>
                  <td>{user[2]}</td>
                  <td>{user[3]}</td>
                  <td>{user[4]}</td>
                  <td>{user[5]}</td>
                  <td>{user[8]}</td>
                  <td>{status_code_user}</td>
                  <td>
                     <button
                        type="button"
                        class="btn btn-success"
                        onClick={() => this.toggleModal(user[0])}
                     >
                        <i class="fas fa-search"></i>
                     </button>
                  </td>
               </tr>
            </React.Fragment>
         );
      });
      this.setState({
         pageCount: Math.ceil(this.state.data.length / this.state.perPage),
         postData,
      });
   }

   exportData = (props) => {
      const { data, tag } = props
      let headerExcel = [
         {
            sheet: "PTEN",
            columns: [
               { label: "NMID", value: "NMID" },
               { label: "NAMA_MERCHANT", value: "Nama_Merchant_50" },
               { label: "NAMA_MERCHANT_QRIS", value: "Nama_Merchant_25" },
               { label: "MPAN", value: "MPAN" },
               { label: "MID", value: "MID" },
               { label: "KOTA", value: "Kota" },
               { label: "KODE_POS", value: "Kodepos" },
               { label: "KRITERIA", value: "Kriteria" },
               { label: "MCC", value: "MCC" },
               { label: "JUMLAH_TERMINAL", value: "Jml_Terminal" },
               { label: "TIPE_MERCHANT", value: "Tipe_Merchant" },
               { label: "NPWP", value: "NPWP" },
               { label: "KTP", value: "KTP" },
            ],
            content: [],
         },
      ];

      Object.keys(data).map((val, i) => {
         let newNpwp = data[i].identitasPemilik.pemilik_npwp
         if (newNpwp === null) {
            newNpwp = ""
         } else if (newNpwp !== null) {
            newNpwp = JSON.stringify(data[i].identitasPemilik.pemilik_npwp)
         }

         headerExcel[0].content.push({
            NMID: data[i].initial.initial_nmid,
            Nama_Merchant_50: data[i].identitasUsaha.usaha_nama50,
            Nama_Merchant_25: data[i].identitasUsaha.usaha_nama25,
            MPAN: data[i].initial.initial_mpan,
            MID: data[i].initial.initial_mid,
            Kota: data[i].alamatNasabah.alamat_kotakab,
            Kodepos: data[i].alamatNasabah.pribadi_kodepos,
            Kriteria: data[i].identitasUsaha.usaha_klasifikasi,
            MCC: data[i].identitasUsaha.usaha_mcc,
            Jml_Terminal: data[i].identitasUsaha.usaha_terminal,
            Tipe_Merchant: data[i].identitasUsaha.usaha_tipemerchant,
            NPWP: newNpwp,
            KTP: JSON.stringify(data[i].identitasPemilik.pemilik_ktp),
         });

         MainMqrisService.masterPut({
            appId: data[i].appId,
            created_date: data[i].created_date,
            username: this.state.userLogin,
            status_code: 4,
            swalSuccess: true
         })
      })

      let d = new Date();
      let month = d.getMonth() + 1;

      if (month < 10) {
         month = "0" + month;
      }

      let settings = {
         fileName: tag + "-" + d.getDate() + month + d.getFullYear(),
         extraLength: 3,
         writeOptions: {},
      };

      xlsx(headerExcel, settings);
      window.location.reload()
   }

   exportDataAll = (props) => {
      const { data, tag } = props
      let headerExcel = [
         {
            sheet: "PTEN",
            columns: [
               { label: "NMID", value: "NMID" },
               { label: "NAMA_MERCHANT", value: "Nama_Merchant_50" },
               { label: "NAMA_MERCHANT_QRIS", value: "Nama_Merchant_25" },
               { label: "MPAN", value: "MPAN" },
               { label: "MID", value: "MID" },
               { label: "KOTA", value: "Kota" },
               { label: "KODE_POS", value: "Kodepos" },
               { label: "KRITERIA", value: "Kriteria" },
               { label: "MCC", value: "MCC" },
               { label: "JUMLAH_TERMINAL", value: "Jml_Terminal" },
               { label: "TIPE_MERCHANT", value: "Tipe_Merchant" },
               { label: "NPWP", value: "NPWP" },
               { label: "KTP", value: "KTP" },
            ],
            content: [],
         },
      ];

      Object.keys(data).map((val, i) => {
         let newNpwp = data[i].identitasPemilik.pemilik_npwp
         if (newNpwp === null) {
            newNpwp = ""
         } else if (newNpwp !== null) {
            newNpwp = JSON.stringify(data[i].identitasPemilik.pemilik_npwp)
         }

         headerExcel[0].content.push({
            NMID: data[i].initial.initial_nmid,
            Nama_Merchant_50: data[i].identitasUsaha.usaha_nama50,
            Nama_Merchant_25: data[i].identitasUsaha.usaha_nama25,
            MPAN: data[i].initial.initial_mpan,
            MID: data[i].initial.initial_mid,
            Kota: data[i].alamatNasabah.alamat_kotakab,
            Kodepos: data[i].alamatNasabah.pribadi_kodepos,
            Kriteria: data[i].identitasUsaha.usaha_klasifikasi,
            MCC: data[i].identitasUsaha.usaha_mcc,
            Jml_Terminal: data[i].identitasUsaha.usaha_terminal,
            Tipe_Merchant: data[i].identitasUsaha.usaha_tipemerchant,
            NPWP: newNpwp,
            KTP: JSON.stringify(data[i].identitasPemilik.pemilik_ktp),
         });
      })

      let d = new Date();
      let month = d.getMonth() + 1;

      if (month < 10) {
         month = "0" + month;
      }

      let settings = {
         fileName: tag + "-" + d.getDate() + month + d.getFullYear(),
         extraLength: 3,
         writeOptions: {},
      };

      xlsx(headerExcel, settings);
      window.location.reload()
   }

   getDataExportKPO = async () => {
      const data = await MainMqrisService.getDataExportKPO()
      let tag = "PTEN"
      this.exportData({ data, tag })
   }

   getDataExportAll = async () => {
      const data = await MainMqrisService.getDataExportAll()
      let tag = "REPORT"
      this.exportDataAll({ data, tag })
   }

   btnExport = async () => {
      const dataLogin = await MainMqrisService.getUsername()
      this.setState({ userLogin: dataLogin[0].username })
      Swal.fire(
         {
            title: 'Export',
            input: 'select',
            inputOptions: {
               'Report': {
                  'reportAll': '* MQRIS MMA REPORT',
               },
               'pten1': '',
               'pten': '*Export For PTEN',
               'pten2': '',
            },
            inputPlaceholder: 'Select',
            showCancelButton: true,
            inputValidator: (value) => {
               return new Promise((resolve) => {
                  if (value === 'pten') {
                     resolve()
                     this.getDataExportKPO()
                  } else if (value === 'reportAll') {
                     resolve()
                     this.getDataExportAll()
                  }
               })
            }
         }
      )
   };

   render() {
      return (
         <div className="container-fluid">
            <Modal
               isOpen={this.state.open}
               onRequestClose={this.toggleModal}
               contentLabel="My dialog"
               className="modal-bg"
            >
               <ModalTemplate />
            </Modal>
            <div class="input-group">
               <div class="form-outline form-search">
                  <input id="search-focus" onChange={(e) => { this.getDataSearch(e.target.value) }} type="search" placeholder="Search" class="form-control" />
               </div>
               <button type="button" class="btn btn-primary">
                  <i class="fas fa-search"></i>
               </button>
            </div>
            <div className="card shadow mb-4">
               <div className="card-header py-3">
                  <h6 className="m-0 font-weight-bold text-primary">
                     Inquiry Instruction KPO
                     <div className='div-export'>
                        <i className="fas fa-download img-export-excel"></i>
                        <a onClick={() => this.btnExport()}>Export</a>
                     </div>
                  </h6>
               </div>
               <div className="card-body">
                  <div className="tableFixHead table-responsive">
                     <table id="myTable" className="table table-striped">
                        <thead className="thead-dark">
                           <tr>
                              <th>AppId</th>
                              <th>Merchant Name</th>
                              <th>Merchant Type</th>
                              <th>Merchant Criteria</th>
                              <th>MDR</th>
                              <th>Kantor Cabang</th>
                              <th>Kode Referensi</th>
                              <th>Status</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>{this.state.postData}</tbody>
                     </table>
                  </div>
                  <ReactPaginate
                     previousLabel={"Prev"}
                     nextLabel={"Next"}
                     breakLabel={"..."}
                     breakClassName={"break-me"}
                     pageCount={this.state.pageCount}
                     marginPagesDisplayed={2}
                     pageRangeDisplayed={5}
                     onPageChange={this.handlePageClick}
                     containerClassName={"pagination"}
                     subContainerClassName={"pages pagination"}
                     activeClassName={"active"} />

               </div>
            </div>
         </div>
      );
   }
}