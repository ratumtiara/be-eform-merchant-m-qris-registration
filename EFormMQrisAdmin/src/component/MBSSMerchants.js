import React, { Component } from "react";
import Modal from "react-modal";
import ModalTemplate from "../modal/ModalMBSSMerchant";
import ReactPaginate from "react-paginate";
import MainMqrisService from "../services/MainMqrisService";

export default class InQuiryInstructionKPO extends Component {
    constructor(props) {
        super(props);
        this.state = {
            offset: 0,
            open: false,
            perPage: 9,
            currentPage: 0,
            data: [],
            nikSearch: ""
        };
        this.handlePageClick = this.handlePageClick.bind(this);
        this.toggleModal = this.toggleModal.bind(props);
    }

    componentDidMount = async () => {
        let dataDB = await this.getData()
        this.setState({ data: dataDB })
        this.renderDatatable();
    }

    getData = async () => {
        const data = await MainMqrisService.getMBSSMerchant()
        return data
    }

    renderDatatable = () => {
        let num = 0
        const slice = this.state.data.slice(
            this.state.offset,
            this.state.offset + this.state.perPage
        );
        const postData = slice.map((user) => {
            num += 1

            let status_code_user = null;
            if (user.status_code === 4) {
                status_code_user = "Ready to MBSS";
            }

            return (
                <React.Fragment>
                    <tr>
                        <td>{num}</td>
                        <td>{user.identitasUsaha.usaha_norekmuamalat}</td>
                        <td>{user.identitasUsaha.usaha_nama50}</td>
                        <td>{user.identitasUsaha.usaha_tipemerchant}</td>
                        <td>BANK MUAMALAT</td>
                        <td>{status_code_user}</td>
                        <td>
                            <button
                                type="button"
                                class="btn btn-success"
                                onClick={() => this.toggleModal(user.appId)}
                            >
                                <i class="fas fa-edit"></i>
                            </button>
                        </td>
                    </tr>
                </React.Fragment>
            );
        });
        this.setState({
            pageCount: Math.ceil(this.state.data.length / this.state.perPage),
            postData,
        });
    }

    handlePageClick = (e) => {
        const selectedPage = e.selected;
        const offset = selectedPage * this.state.perPage;
        this.setState(
            {
                currentPage: selectedPage,
                offset: offset,
            },
            () => {
                this.renderDatatable();
            }
        );
    };

    toggleModal = (props) => {
        this.setState({ open: !this.state.open });
        localStorage.setItem("appId", props);
        localStorage.setItem("id", props);
    };

    closeModal = () => {
        this.setState({ open: false })
    }

    render() {
        return (
            <div className="container-fluid">
                <Modal
                    isOpen={this.state.open}
                    onRequestClose={this.toggleModal}
                    contentLabel="My dialog"
                    className="modal-bg"
                >
                    <ModalTemplate modalClose={this.closeModal} />
                </Modal>
                <div className="card shadow mb-4">
                    <div className="card-header py-3">
                        <h6 className="m-0 font-weight-bold text-primary">
                            MBSS Merchant
                        </h6>
                    </div>
                    <div className="card-body">
                        <div className="tableFixHead table-responsive">
                            <table id="myTable" className="table table-striped">
                                <thead className="thead-dark">
                                    <tr>
                                        <th>No</th>
                                        <th>Account No</th>
                                        <th>Merchant Name</th>
                                        <th>Merchant Type</th>
                                        <th>Acquire Name</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>{this.state.postData}</tbody>
                            </table>
                        </div>
                        <ReactPaginate
                            previousLabel={"Prev"}
                            nextLabel={"Next"}
                            breakLabel={"..."}
                            breakClassName={"break-me"}
                            pageCount={this.state.pageCount}
                            marginPagesDisplayed={2}
                            pageRangeDisplayed={5}
                            onPageChange={this.handlePageClick}
                            containerClassName={"pagination"}
                            subContainerClassName={"pages pagination"}
                            activeClassName={"active"} />
                    </div>
                </div>
            </div>
        );
    }
}