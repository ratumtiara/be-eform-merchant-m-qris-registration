import React, { Component } from "react";
import Modal from "react-modal";
import ReactPaginate from "react-paginate";
import MainMqrisService from "../services/MainMqrisService";
import Swal from 'sweetalert2';
import axios from "axios";

export default class InQuiryInstructionKPO extends Component {
    constructor(props) {
        super(props);
        this.state = {
            offset: 0,
            open: false,
            perPage: 9,
            currentPage: 0,
            data: [],
            nikSearch: ""
        };
        this.handlePageClick = this.handlePageClick.bind(this);
    }

    componentDidMount = async () => {
        let dataDB = await this.getData()
        this.setState({ data: dataDB })
        this.renderDatatable();
    }

    handlePageClick = (e) => {
        const selectedPage = e.selected;
        const offset = selectedPage * this.state.perPage;
        this.setState(
            {
                currentPage: selectedPage,
                offset: offset,
            },
            () => {
                this.renderDatatable();
            }
        );
    };

    getData = async () => {
        const data = await MainMqrisService.getMBSSPan()
        return data
    }

    getDataSearch = async (props) => {
        if (props !== "") {
            const dataLogin = await MainMqrisService.getUsername()
            const branch = dataLogin[0].user_branch
            const userJabatan = dataLogin[0].user_jabatan
            const paramValue = props
            const dataDB = await MainMqrisService.getDataFilterInquiry({ branch, userJabatan, paramValue })
            this.setState({ data: dataDB })
            this.renderDatatable()
        } else {
            this.getData()
            this.renderDatatable()
            window.location.reload()
        }
    }

    renderDatatable = () => {
        const slice = this.state.data.slice(
            this.state.offset,
            this.state.offset + this.state.perPage
        );

        const postData = slice.map((user) => {
            this.setState({ appId: user[0] })
            return (
                <React.Fragment>
                    <tr>
                        <td>{user[0]}</td>
                        <td>{user[1]}</td>
                        <td>{user[2]}</td>
                        <td>{user[3]}</td>
                        <td>{user[4]}</td>
                        <td>{user[5]}</td>
                        <td>{user[6]}</td>
                    </tr>
                </React.Fragment>
            );
        });
        this.setState({
            pageCount: Math.ceil(this.state.data.length / this.state.perPage),
            postData,
        });
    }

    btnEODPan = async () => {
        Swal.fire({
            title: 'TAG 26 & 51 ?',
            showCancelButton: true,
            confirmButtonText: 'Save',
        }).then((result) => {
            if (result.isConfirmed) {
                MainMqrisService.postMBSSPan()
                Swal.fire('Saved!', '', 'success')
                    .then((res) => {
                        if (res.isConfirmed) {
                            window.location.reload()
                        }
                    })
                    .catch((err) => {
                        console.log(err);
                    })
            } else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="card shadow mb-4">
                    <div className="card-header py-3">
                        <h6 className="m-0 font-weight-bold text-primary">
                            MBSS PAN
                            <div className='div-export'>
                                <i class="fas fa-location-arrow img-export-excel"></i>
                                <a onClick={() => this.btnEODPan()}>EOD PAN</a>
                            </div>
                        </h6>
                    </div>
                    <div className="card-body">
                        <div className="tableFixHead table-responsive">
                            <table id="myTable" className="table table-striped">
                                <thead className="thead-dark">
                                    <tr>
                                        <th>No</th>
                                        <th>Account No</th>
                                        <th>Criteria</th>
                                        <th>Merchant Id</th>
                                        <th>Pan No</th>
                                        <th>ACQ Domain</th>
                                        <th>Tag Id</th>
                                    </tr>
                                </thead>
                                <tbody>{this.state.postData}</tbody>
                            </table>
                        </div>
                        <ReactPaginate
                            previousLabel={"Prev"}
                            nextLabel={"Next"}
                            breakLabel={"..."}
                            breakClassName={"break-me"}
                            pageCount={this.state.pageCount}
                            marginPagesDisplayed={2}
                            pageRangeDisplayed={5}
                            onPageChange={this.handlePageClick}
                            containerClassName={"pagination"}
                            subContainerClassName={"pages pagination"}
                            activeClassName={"active"} />
                    </div>
                </div>
            </div>
        );
    }
}