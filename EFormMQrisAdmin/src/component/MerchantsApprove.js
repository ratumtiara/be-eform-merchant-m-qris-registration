import React, { useState, useEffect } from "react";
import Modal from 'react-modal'
import ModalTemplateEdit from '../modal/ModalMerchantsEditApprove'
import MainMqrisService from "../services/MainMqrisService";

export default function MerchantChecker() {
    const [data, setData] = useState()
    const [isOpenPage, setIsOpenPage] = useState()
    const [isOpenEdit, setIsOpenEdit] = useState(false);

    useEffect(() => {
        getAll()
    }, [])

    const getAll = async () => {
        const dataLogin = await MainMqrisService.getUsername()
        const user_branch = dataLogin[0].user_branch
        const dataMerchants = await MainMqrisService.getDataMerchantsApprove({ user_branch })
        const userJabatan = (dataLogin[0].user_jabatan)
        if (userJabatan === '2') {
            setData(dataMerchants)
            setIsOpenPage(true)
        } else {
            setData(null)
            setIsOpenPage(false)
        }
    }

    const toggleModalEdit = (props) => {
        setIsOpenEdit(!isOpenEdit);
        localStorage.setItem('appId', props)
        localStorage.setItem('id', props)
    }

    const renderTableGetAll = () => {
        return data.map((user) => {
            let status_code_user = null
            if (user[7] === 1) {
                status_code_user = "To Be Checked"
            } else if (user[7] === 2) {
                status_code_user = "To Be Approved"
            }
            return (
                <tr>
                    <td>{user[0]}</td>
                    <td>{user[1]}</td>
                    <td>{user[2]}</td>
                    <td>{user[3]}</td>
                    <td>{user[4]}</td>
                    <td>{user[5]}</td>
                    <td>{user[6]}</td>
                    <td>{status_code_user}</td>
                    <td>
                        <button type="button" class="btn btn-success" onClick={() => toggleModalEdit(user[0])}><i class="fas fa-edit"></i></button>
                    </td>
                </tr>
            )
        })
    }

    const renderTableNull = () => {
        return (
            <tr>
                <p className="p-data-not-found">Data Not Found</p>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        )
    }

    return (
        <div className="container-fluid">
            <Modal
                isOpen={isOpenEdit}
                onRequestClose={toggleModalEdit}
                contentLabel="My dialog"
                className="modal-bg"
            >
                <ModalTemplateEdit modalClose={setIsOpenEdit} />
            </Modal>
            <div className="card shadow mb-4">
                <div className="card-header py-3">
                    <h6 className="m-0 font-weight-bold text-primary">Merchant Accounts - Approve</h6>
                </div>

                <div className="card-body">
                    <div className="tableFixHead table-responsive">
                        <table className="table table-striped">
                            <thead className="thead-dark">
                                <tr>
                                    <th>AppId</th>
                                    <th>Merchant Name</th>
                                    <th>Merchant Type</th>
                                    <th>Merchant Criteria</th>
                                    <th>MDR</th>
                                    <th>Kantor Cabang</th>
                                    <th>Kode Referensi</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    isOpenPage ? renderTableGetAll() : renderTableNull()
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    )
}