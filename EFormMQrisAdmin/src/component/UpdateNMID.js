import React, { Component } from "react";
import ReactPaginate from "react-paginate";
import MainMqrisService from "../services/MainMqrisService";
import Swal from 'sweetalert2';

export default class InQuiryInstructionKPO extends Component {
   constructor(props) {
      super(props);
      this.state = {
         offset: 0,
         open: false,
         perPage: 9,
         currentPage: 0,
         data: [],
         nikSearch: "",
         userLogin: null
      };
      this.handlePageClick = this.handlePageClick.bind(this);
   }

   componentDidMount = async () => {
      let dataDB = await this.getData()
      this.setState({ data: dataDB })
      this.renderDatatable();
   }

   handlePageClick = (e) => {
      const selectedPage = e.selected;
      const offset = selectedPage * this.state.perPage;

      this.setState(
         {
            currentPage: selectedPage,
            offset: offset,
         },
         () => {
            this.renderDatatable();
         }
      );
   };

   swalUpdateNMID = () => {
      Swal.fire('Saved!', '', 'success')
         .then((res) => {
            if (res.isConfirmed) {
               window.location.reload()
            }
         })
         .catch((err) => {
            console.log(err);
         })
   }

   toggleUpdateNMID = async (props) => {
      const { appId, panno } = props
      let swalUpdateNMIDNew = this.swalUpdateNMID
      let dataUserLogin = await MainMqrisService.getUsername()
      Swal.fire({
         title: 'Do you want to Update NMID ?',
         showCancelButton: true,
         confirmButtonText: 'Save',
      }).then((result) => {
         if (result.isConfirmed) {
            MainMqrisService.postUpdateNMID({ appId, panno, swalUpdateNMIDNew, dataUserLogin })
         } else if (result.isDenied) {
            Swal.fire('Changes are not saved', '', 'info')
         }
      })
   }

   getData = async () => {
      const data = await MainMqrisService.getUpdateNMID()
      return data
   }

   renderDatatable = () => {
      const slice = this.state.data.slice(
         this.state.offset,
         this.state.offset + this.state.perPage
      );

      const postData = slice.map((user) => {
         let appId = user[0]
         let panno = user[4]
         return (
            <React.Fragment>
               <tr>
                  <td>{user[0]}</td>
                  <td>{user[1]}</td>
                  <td>{user[2]}</td>
                  <td>{user[3]}</td>
                  <td>{user[4]}</td>
                  <td>{user[5]}</td>
                  <td>
                     <button
                        type="button"
                        class="btn btn-success"
                        onClick={() => this.toggleUpdateNMID({ appId, panno })}
                     >
                        <i class="fas fa-edit"></i>
                     </button>
                  </td>
               </tr>
            </React.Fragment>
         );
      });
      this.setState({
         pageCount: Math.ceil(this.state.data.length / this.state.perPage),
         postData,
      });
   }

   render() {
      return (
         <div className="container-fluid">
            <div className="card shadow mb-4">
               <div className="card-header py-3">
                  <h6 className="m-0 font-weight-bold text-primary">
                     Update NMID
                  </h6>
               </div>
               <div className="card-body">
                  <div className="tableFixHead table-responsive">
                     <table id="myTable" className="table table-striped">
                        <thead className="thead-dark">
                           <tr>
                              <th>AppId</th>
                              <th>Merchant Name</th>
                              <th>Kode Referensi</th>
                              <th>MID</th>
                              <th>MPAN</th>
                              <th>NMID</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>{this.state.postData}</tbody>
                     </table>
                  </div>
                  <ReactPaginate
                     previousLabel={"Prev"}
                     nextLabel={"Next"}
                     breakLabel={"..."}
                     breakClassName={"break-me"}
                     pageCount={this.state.pageCount}
                     marginPagesDisplayed={2}
                     pageRangeDisplayed={5}
                     onPageChange={this.handlePageClick}
                     containerClassName={"pagination"}
                     subContainerClassName={"pages pagination"}
                     activeClassName={"active"} />
               </div>
            </div>
         </div>
      );
   }
}