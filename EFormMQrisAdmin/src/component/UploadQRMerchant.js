import React, { Component } from "react";
import Modal from "react-modal";
import ModalTemplate from "../modal/ModalUploadQRMerchant";
import ReactPaginate from "react-paginate";
import MainMqrisService from "../services/MainMqrisService";
import Swal from 'sweetalert2';

export default class InQuiryInstruction extends Component {
   constructor(props) {
      super(props);
      this.state = {
         offset: 0,
         open: false,
         perPage: 9,
         currentPage: 0,
         exportData: []
      };
      this.handlePageClick = this.handlePageClick.bind(this);
      this.toggleModal = this.toggleModal.bind(props);
   }

   componentDidMount() {
      this.receivedData();
   }

   handlePageClick = (e) => {
      const selectedPage = e.selected;
      const offset = selectedPage * this.state.perPage;

      this.setState(
         {
            currentPage: selectedPage,
            offset: offset,
         },
         () => {
            this.receivedData();
         }
      );
   };

   toggleModal = (props) => {
      this.setState({ open: !this.state.open });
      localStorage.setItem("appId", props.appId);
      localStorage.setItem("id", props.appId);
   };

   receivedData = async () => {
      // const data = await MainMqrisService.getDataSettled(6)
      const data = []
      const slice = data.slice(
         this.state.offset,
         this.state.offset + this.state.perPage
      );
      const postData = slice.map((user) => {
         let status_code_user = null;

         if (user[6] === 1) {
            status_code_user = "To Be Checked";
         } else if (user[6] === 2) {
            status_code_user = "To Be Approved";
         } else if (user[6] === 3) {
            status_code_user = "To BE Checked for PTEN";
         } else if (user[6] === 4) {
            status_code_user = "To BE Approved for PTEN";
         } else if (user[6] === 5) {
            status_code_user = "Ready for Export";
         } else if (user[6] === 6) {
            status_code_user = "Settled";
         } else if (user[6] === 7) {
            status_code_user = "Reject Checked";
         } else if (user[6] === 8) {
            status_code_user = "Reject Approved";
         } else if (user[6] === 9) {
            status_code_user = "Reject Checked PTEN";
         } else if (user[6] === 10) {
            status_code_user = "Reject Approved PTEN";
         } else if (user[6] === 11) {
            status_code_user = "Ready to Create MBSS";
         }

         return (
            <React.Fragment>
               <tr>
                  <td>{user.appId}</td>
                  <td>{user.identitasUsaha.usaha_nama50}</td>
                  <td>{user.identitasUsaha.usaha_tipemerchant}</td>
                  <td></td>
                  <td>{user.initial.initial_mpan}</td>
                  <td>{user.initial.initial_mid}</td>
                  <td>{status_code_user}</td>
                  <td>
                     <button
                        type="button"
                        class="btn btn-success"
                        onClick={() => this.toggleModal(user)}
                     >
                        <i class="fas fa-search"></i>
                     </button>
                  </td>
               </tr>
            </React.Fragment>
         );
      });
      this.setState({
         pageCount: Math.ceil(data.length / this.state.perPage),
         postData,
      });
   }

   render() {
      return (
         <div className="container-fluid">
            <Modal
               isOpen={this.state.open}
               onRequestClose={this.toggleModal}
               contentLabel="My dialog"
               className="modal-bg"
            >
               <ModalTemplate />
            </Modal>

            <div className="card shadow mb-4">
               <div className="card-header py-3">
                  <h6 className="m-0 font-weight-bold text-primary">
                     Upload QR Merchants
                  </h6>
               </div>

               <div className="card-body">
                  <div className="tableFixHead table-responsive">
                     <table id="myTable" className="table table-striped">
                        <thead className="thead-dark">
                           <tr>
                              <th>AppId</th>
                              <th>Merchant Name</th>
                              <th>Merchant Type</th>
                              <th>NMID</th>
                              <th>MPAN</th>
                              <th>MID</th>
                              <th>Status</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>{this.state.postData}</tbody>
                     </table>
                  </div>
                  <ReactPaginate
                     previousLabel={"Prev"}
                     nextLabel={"Next"}
                     breakLabel={"..."}
                     breakClassName={"break-me"}
                     pageCount={this.state.pageCount}
                     marginPagesDisplayed={2}
                     pageRangeDisplayed={5}
                     onPageChange={this.handlePageClick}
                     containerClassName={"pagination"}
                     subContainerClassName={"pages pagination"}
                     activeClassName={"active"} />
               </div>
            </div>
         </div>
      );
   }
}