import axios from "axios";
import React, { useState, useContext } from "react";
import MainMqrisService from "../services/MainMqrisService";
import Swal from "sweetalert2";

export default function Changepass() {
   const [usernameBody, setUsernameBody] = useState();
   const [newpasswordBody, setnewPasswordBody] = useState();
   const [validasipasswordBody, setValidasiPasswordBody] = useState();

   let urlpassword = MainMqrisService.getpasswordUser();
   function getpassword() {
      if (newpasswordBody === validasipasswordBody) {
         axios({
            url: urlpassword + usernameBody + "/" + newpasswordBody,
            method: "GET",
         })
            .then((res) => {
               Swal.fire({
                  title: "Reset Password",
                  text: "Reset Password Berhasil",
                  icon: "success",
                  confirmButtonText: "OK",
               }).then(function (isconfirm) {
                  if (isconfirm) {
                     localStorage.setItem("isLoginUser", null);
                     localStorage.setItem("isChangePassword", null);
                     window.location.reload();
                  }
               });
            })
            .catch((err) => {
               console.log(err);
            });
      } else {
         Swal.fire({
            title: "Reset Password",
            text: "Password Tidak Cocok",
            icon: "error",
            confirmButtonText: "OK",
         });
      }
   }

   function testing() {
      if (newpasswordBody === validasipasswordBody) {
         // console.log("password sama");
      } else {
         // console.log("password beda");
      }
   }

   function backtologin() {
      localStorage.setItem("isLoginUser", null);
      localStorage.setItem("isChangePassword", null);
      window.location.reload();
   }

   return (
      <body>
         <div class="container">
            <div class="row justify-content-center">
               <div class="col-xl-10 col-lg-12 col-md-9 login-page">
                  <div class="card o-hidden border-0 shadow-lg my-5">
                     <div class="card-body p-0">
                        <div class="row">
                           <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                           <div class="col-lg-6">
                              <div class="p-5">
                                 <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">
                                       Change Password
                                    </h1>
                                 </div>
                                 <form class="user">
                                    <div class="form-group">
                                       <input
                                          type="text"
                                          class="form-control form-control-user"
                                          id="usernameBody"
                                          aria-describedby="emailHelp"
                                          placeholder="Username"
                                          onChange={(e) =>
                                             setUsernameBody(e.target.value)
                                          }
                                       />
                                    </div>
                                    <div class="form-group">
                                       <input
                                          type="password"
                                          class="form-control form-control-user"
                                          id="newPassword"
                                          placeholder=" New Password"
                                          onChange={(e) =>
                                             setnewPasswordBody(e.target.value)
                                          }
                                       />
                                    </div>
                                    <div class="form-group">
                                       <input
                                          type="password"
                                          class="form-control form-control-user"
                                          id="validasiPassword"
                                          placeholder=" Confirm New Password"
                                          onChange={(e) =>
                                             setValidasiPasswordBody(
                                                e.target.value
                                             )
                                          }
                                       />
                                    </div>
                                    <a
                                       onClick={() => getpassword()}
                                       class="btn btn-primary btn-user btn-block"
                                    >
                                       Change
                                    </a>

                                    <a
                                       onClick={() => backtologin()}
                                       class="w3-button w3-black"
                                    >
                                       Back to Login
                                    </a>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </body>
   );
}
