import React from "react";
import Nav from "./Nav";
import Topbar from "./Topbar";
import Footer from "./Footer";

export default function Dashboard() {
    return (
        <div id="wrapper">
            <Nav />
            <div id="content-wrapper" className="d-flex flex-column">
                <div id="content">
                    <Topbar />

                </div>
                <Footer />
            </div>
        </div >
    )
}