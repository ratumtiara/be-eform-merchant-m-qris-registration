import axios from 'axios';
import React, { useState, useContext } from 'react';
import MainMqrisService from '../services/MainMqrisService';
import { multiStepContext } from '../StepContext';
import { sha256 } from 'js-sha256';

export default function Login() {
    const { page, setPage } = useContext(multiStepContext);
    const [usernameBody, setUsernameBody] = useState()
    const [passwordBody, setPasswordBody] = useState()
    let urlLogin = MainMqrisService.getLoginUser()

    function hash(string) {
        return sha256.hex(string);
    }

    function getLogin() {
        axios({
            url: urlLogin + usernameBody,
            method: 'GET'
        })
            .then((res) => {
                var user_password_hash = (hash(passwordBody))
                if (res.data[0].username == usernameBody && res.data[0].user_password == user_password_hash) {
                    localStorage.setItem('isLoginUser', 1)
                    localStorage.setItem('username', res.data[0].username)

                    window.location.reload()
                    setPage(1)
                } else {
                    console.log("password salah");
                }
            })
            .catch((err) => {
                console.log(err);
            })
    }

    function getChangePassword() {
        localStorage.setItem("isChangePassword", 2)
        window.location.reload()
    }

    return (
        <body>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-10 col-lg-12 col-md-9 login-page">
                        <div class="card o-hidden border-0 shadow-lg my-5">
                            <div class="card-body p-0">
                                <div class="row">
                                    <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                                    <div class="col-lg-6">
                                        <div class="p-5">
                                            <div class="text-center">
                                                <h1 class="h4 text-gray-900 mb-4">MQRIS Dashboard</h1>
                                            </div>
                                            <form class="user">
                                                <div class="form-group">
                                                    <input
                                                        type="text"
                                                        class="form-control form-control-user"
                                                        id="exampleInputEmail"
                                                        aria-describedby="emailHelp"
                                                        placeholder="Username"
                                                        onChange={(e) => setUsernameBody(e.target.value)}
                                                    />
                                                </div>
                                                <div class="form-group">
                                                    <input type="password" class="form-control form-control-user"
                                                        id="exampleInputPassword" placeholder="Password"
                                                        onChange={(e) => setPasswordBody(e.target.value)} />
                                                </div>
                                                <a
                                                    onClick={() => getLogin()}
                                                    class="btn btn-primary btn-user btn-block"
                                                >

                                                    Login
                                                </a >

                                                <a
                                                    onClick={() => getChangePassword()}
                                                    class="w3-button w3-black"

                                                >
                                                    {/* Change Password */}
                                                </a>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </body>
    )
}