import axios from "axios";
import React, { useState } from "react";
import MainMqrisService from "../services/MainMqrisService";
import Swal from "sweetalert2";

export default function Changepass() {
   const [usernameBody, setUsernameBody] = useState();
   const [newpasswordBody, setnewPasswordBody] = useState();
   const [validasipasswordBody, setValidasiPasswordBody] = useState();

   let urlpassword = MainMqrisService.getpasswordUser();

   function getpassword() {
      if (newpasswordBody === validasipasswordBody) {
         axios({
            url: urlpassword + usernameBody + "/" + newpasswordBody,
            method: "GET",
         })
            .then((res) => {
               Swal.fire({
                  title: "Reset Password",
                  text: "Reset Password Berhasil",
                  icon: "success",
                  confirmButtonText: "OK",
               }).then(function (isconfirm) {
                  if (isconfirm) {
                     window.location.reload();
                  }
               });
            })
            .catch((err) => {
               console.log(err);
            });
      } else {
         Swal.fire({
            title: "Reset Password",
            text: "Password Tidak Cocok",
            icon: "error",
            confirmButtonText: "OK",
         });
      }
   }

   return (
      <>
         <div className='containerOne'>
            <div className='tableOne'>
               <p style={{ 'font-weight': 600, 'color': 'Indigo', 'margin-bottom': '24px' }}>Account Setting</p>
               <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
               <div className='row-modal'>
                  <div style={{ 'width': '50%' }}>
                     <p className='tableTitle'>Username</p>
                  </div>
                  {
                     <div style={{ 'width': '50%' }}>
                        <p className='tableContent'>
                           <input class="form-control-sm" onchange={(e) => setUsernameBody(e.target.value)} value={usernameBody} />
                        </p>
                     </div>
                  }
               </div>
               <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
               <div className='row-modal'>
                  <div style={{ 'width': '50%' }}>
                     <p className='tableTitle'> New Password</p>
                  </div>
                  {
                     <div style={{ 'width': '50%' }}>
                        <p className='tableContent'>
                           <input class="form-control-sm" onchange={(e) => setnewPasswordBody(e.target.value)} value={newpasswordBody} />
                        </p>
                     </div>
                  }
               </div>
               <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
               <div className='row-modal'>
                  <div style={{ 'width': '50%' }}>
                     <p className='tableTitle'> Confirm New Password</p>
                  </div>
                  {
                     <div style={{ 'width': '50%' }}>
                        <p className='tableContent'>
                           <input class="form-control-sm" onchange={(e) => setValidasiPasswordBody(e.target.value)} value={validasipasswordBody} />
                        </p>
                     </div>
                  }
               </div>
            </div>
            <div className='tableOne'>
               <div className='box-btn-modal'>
                  <button type="button" class="btn btn-danger box-btn-modal-detail" onClick={() => window.location.reload()}>Close</button>
                  <button type="button" class="btn btn-primary box-btn-modal-detail" onClick={() => getpassword()}>CHANGE PASSWORD</button>
               </div>
            </div>
         </div>
      </>
   );
}