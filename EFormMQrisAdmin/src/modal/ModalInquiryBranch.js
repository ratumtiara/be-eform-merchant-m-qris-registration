import axios from 'axios';
import React, { useEffect, useState } from 'react';
import MainMqrisService from '../services/MainMqrisService';

export default function ModalTemplate() {

  // Informasi Umum
  const [appId, setAppId] = useState()
  const [initial_nmid, setinitial_nmid] = useState()
  const [initial_mpan, setinitial_mpan] = useState()
  const [initial_mid, setinitial_mid] = useState()
  const [initial_mqris, setinitial_mqris] = useState()
  const [usaha_tipemerchant, setusaha_tipemerchant] = useState()
  const [usaha_klasifikasi, setusaha_klasifikasi] = useState()
  const [usaha_mdr, setusaha_mdr] = useState()
  const [usaha_mcc, setusaha_mcc] = useState()
  const [usaha_tipeqr, setusaha_tipeqr] = useState()
  const [pemilik_ktp, setpemilik_ktp] = useState()
  const [pemilik_nama, setpemilik_nama] = useState()
  const [pribadi_jalan, setpribadi_jalan] = useState()
  const [pribadi_provinsi, setpribadi_provinsi] = useState()
  const [pribadi_kotakab, setpribadi_kotakab] = useState()
  const [pribadi_kecamatan, setpribadi_kecamatan] = useState()
  const [pribadi_kelurahan, setpribadi_kelurahan] = useState()
  const [pribadi_kodepos, setpribadi_kodepos] = useState()
  const [pemilik_handpone, setpemilik_handpone] = useState()
  const [pemilik_email, setpemilik_email] = useState()

  const [usaha_nama50, setusaha_nama50] = useState()
  const [usaha_nama25, setusaha_nama25] = useState()
  const [usaha_terminal, setusaha_terminal] = useState()
  const [usaha_siup, setusaha_siup] = useState()
  const [alamat_jalan, setalamat_jalan] = useState()
  const [alamat_provinsi, setalamat_provinsi] = useState()
  const [alamat_kotakab, setalamat_kotakab] = useState()
  const [alamat_kecamatan, setalamat_kecamatan] = useState()
  const [alamat_kelurahan, setalamat_kelurahan] = useState()
  const [alamat_kodepos, setalamat_kodepos] = useState()
  const [alamat_kordinat, setalamat_kordinat] = useState()
  const [usaha_bankterdekat, setusaha_bankterdekat] = useState()
  const [pic_nama, setpic_nama] = useState()
  const [pic_handphone, setpic_handphone] = useState()
  const [pic_web, setpic_web] = useState()
  const [profil_average, setprofil_average] = useState()
  const [profil_volume, setprofil_volume] = useState()
  const [usaha_kodereferal, setusaha_kodereferal] = useState()

  const [reasonReject, setreasonReject] = useState()
  const [getKTP, setgetKTP] = useState()
  const [getNPWP, setgetNPWP] = useState()
  const [getLegal, setgetLegal] = useState()
  const [getFotoTempat2, setgetFotoTempat2] = useState()
  const [getFotoTempat1, setgetFotoTempat1] = useState()

  useEffect(() => {
    async function getData() {
      let dataUser = await MainMqrisService.getAppId()
      let apiKTP = await MainMqrisService.getKTP()
      let apiNPWP = await MainMqrisService.getNPWP()
      let apiLegal = await MainMqrisService.getLegal()
      let apiTempat1 = await MainMqrisService.getTempat1()
      let apiTempat2 = await MainMqrisService.getTempat2()

      setAppId(dataUser[0].appId)
      setinitial_nmid(dataUser[0].initial.initial_nmid)
      setinitial_mpan(dataUser[0].initial.initial_mpan)
      setinitial_mid(dataUser[0].initial.initial_mid)
      setinitial_mqris(dataUser[0].initial.initial_mqris)
      setusaha_tipemerchant(dataUser[0].identitasUsaha.usaha_tipemerchant)
      setusaha_klasifikasi(dataUser[0].identitasUsaha.usaha_klasifikasi)
      setusaha_mdr(dataUser[0].identitasUsaha.usaha_mdr)
      setusaha_mcc(dataUser[0].identitasUsaha.usaha_mcc)
      setusaha_tipeqr(dataUser[0].identitasUsaha.usaha_tipeqr)
      setpemilik_ktp(dataUser[0].identitasPemilik.pemilik_ktp)
      setpemilik_nama(dataUser[0].identitasPemilik.pemilik_nama)
      setpribadi_jalan(dataUser[0].alamatNasabah.pribadi_jalan)
      setpribadi_provinsi(dataUser[0].alamatNasabah.pribadi_provinsi)
      setpribadi_kotakab(dataUser[0].alamatNasabah.pribadi_kotakab)
      setpribadi_kecamatan(dataUser[0].alamatNasabah.pribadi_kecamatan)
      setpribadi_kelurahan(dataUser[0].alamatNasabah.pribadi_kelurahan)
      setpribadi_kodepos(dataUser[0].alamatNasabah.pribadi_kodepos)
      setpemilik_handpone(dataUser[0].identitasPemilik.pemilik_handpone)
      setpemilik_email(dataUser[0].identitasPemilik.pemilik_email)

      setusaha_nama50(dataUser[0].identitasUsaha.usaha_nama50)
      setusaha_nama25(dataUser[0].identitasUsaha.usaha_nama25)
      setusaha_terminal(dataUser[0].identitasUsaha.usaha_terminal)
      setusaha_siup(dataUser[0].identitasUsaha.usaha_siup)
      setalamat_jalan(dataUser[0].alamatNasabah.alamat_jalan)
      setalamat_provinsi(dataUser[0].alamatNasabah.alamat_provinsi)
      setalamat_kotakab(dataUser[0].alamatNasabah.alamat_kotakab)
      setalamat_kecamatan(dataUser[0].alamatNasabah.alamat_kecamatan)
      setalamat_kelurahan(dataUser[0].alamatNasabah.alamat_kelurahan)
      setalamat_kodepos(dataUser[0].alamatNasabah.alamat_kodepos)
      setalamat_kordinat(dataUser[0].alamatNasabah.alamat_kordinat)
      setusaha_bankterdekat(dataUser[0].identitasUsaha.usaha_bankterdekat)
      setpic_nama(dataUser[0].picUsaha.pic_nama)
      setpic_handphone(dataUser[0].picUsaha.pic_handphone)
      setpic_web(dataUser[0].picUsaha.pic_web)
      setprofil_average(dataUser[0].profilPendapatanUsaha.profil_average)
      setprofil_volume(dataUser[0].profilPendapatanUsaha.profil_volume)
      setusaha_kodereferal(dataUser[0].identitasUsaha.usaha_kodereferal)
      setreasonReject(dataUser[0].reason_reject)

      setgetKTP(apiKTP + dataUser[0].appId)
      setgetNPWP(apiNPWP + dataUser[0].appId)
      setgetLegal(apiLegal + dataUser[0].appId)
      setgetFotoTempat1(apiTempat1 + dataUser[0].appId)
      setgetFotoTempat2(apiTempat2 + dataUser[0].appId)
    }
    getData()
  }, [])

  return (
    <>
      {/* Informasi Umum */}
      <div className='containerOne'>

        {/* Informasi Umum */}
        <div className='tableOne'>
          <p style={{ 'font-weight': 600, 'color': 'Indigo', 'margin-bottom': '24px' }}>Informasi Umum</p>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>AppId</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{appId}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Sudah punya QRIS Merchant Sebelumnya?</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{initial_mqris}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>NMID</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{initial_nmid}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>MPAN</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{initial_mpan}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>MID</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{initial_mid}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Tipe Merchant</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{usaha_tipemerchant}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Klasifikasi Usaha</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{usaha_klasifikasi}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>MDR</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{usaha_mdr}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Kategori Usaha</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{usaha_mcc}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Tipe QRIS</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{usaha_tipeqr}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Nomor KTP Pemilik Usaha</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{pemilik_ktp}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Nama Lengkap Pemilik Usaha</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{pemilik_nama}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Alamat Sesuai KTP</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{pribadi_jalan}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Provinsi</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{pribadi_provinsi}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Kabupaten / Kota</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{pribadi_kotakab}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Kecamatan</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{pribadi_kecamatan}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Kelurahan</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{pribadi_kelurahan}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Kode Pos</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{pribadi_kodepos}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Nomor Telepon Pemilik Usaha</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{pemilik_handpone}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Email Pemilik Usaha</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{pemilik_email}</p>
            </div>
          </div>

        </div>

        <div><br /></div>

        {/* Data Institusi  */}
        <div className='tableOne'>
          <p style={{ 'font-weight': 600, 'color': 'Indigo', 'margin-bottom': '24px' }}>Data Institusi</p>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Nama Toko/Merchant/Institusi</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{usaha_nama50}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Nama Toko/Merchant/Institusi (Akan tampil pada saat scan QRIS)</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{usaha_nama25}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Jumlah Kasir/Loket</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{usaha_terminal}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Nomor Legalitas Usaha</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{usaha_siup}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Alamat Toko</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{alamat_jalan}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Provinsi</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{alamat_provinsi}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Kabupaten / Kota</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{alamat_kotakab}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Kecamatan</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{alamat_kecamatan}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Kelurahan</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{alamat_kelurahan}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Kode Pos</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{alamat_kodepos}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Titik Koordinat</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{alamat_kordinat}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Kantor Cabang Bank Muamalat tedekat Dari Tempat Usaha</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{usaha_bankterdekat}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Penanggung Jawab Toko</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{pic_nama}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Nomor HP Penanggung Jawab Toko</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{pic_handphone}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Website Merchant (Opsional)</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{pic_web}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Rata-rata Transaksi Harian (Opsional)</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{profil_average}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Rata-rata Omzet Harian (Opsional)</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{profil_volume}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Kode Referensi (Opsional)</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{usaha_kodereferal}</p>
            </div>
          </div>

        </div>

        <div><br /></div>

        {
          usaha_tipemerchant == "1" ?
            < div className='tableOne'>
              <p style={{ 'font-weight': 600, 'color': 'Indigo', 'margin-bottom': '24px' }}>Upload Dokumen</p>

              <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
              <div className='row-modal'>
                <div style={{ 'width': '50%' }}>
                  <p className='tableTitle'>KTP</p>
                </div>
                <div style={{ 'width': '50%' }}>
                  <img className='img-inquiry' src={getKTP}></img>
                </div>
              </div>

              <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
              <div className='row-modal'>
                <div style={{ 'width': '50%' }}>
                  <p className='tableTitle'>Foto tempat usaha tampak Depan </p>
                </div>
                <div style={{ 'width': '50%' }}>
                  <img className='img-inquiry' src={getFotoTempat1}></img>
                </div>
              </div>

              <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
              <div className='row-modal'>
                <div style={{ 'width': '50%' }}>
                  <p className='tableTitle'>Foto tempat usaha tampak Lain </p>
                </div>
                <div style={{ 'width': '50%' }}>
                  <img className='img-inquiry' src={getFotoTempat2}></img>
                </div>
              </div>
            </div>
            :
            <div className='tableOne'>
              <p style={{ 'font-weight': 600, 'color': 'Indigo', 'margin-bottom': '24px' }}>Upload Dokumen</p>

              <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
              <div className='row-modal'>
                <div style={{ 'width': '50%' }}>
                  <p className='tableTitle'>KTP</p>
                </div>
                <div style={{ 'width': '50%' }}>
                  <img className='img-inquiry' src={getKTP}></img>
                </div>
              </div>

              <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
              <div className='row-modal'>
                <div style={{ 'width': '50%' }}>
                  <p className='tableTitle'>Foto tempat usaha tampak Depan </p>
                </div>
                <div style={{ 'width': '50%' }}>
                  <img className='img-inquiry' src={getFotoTempat1}></img>
                </div>
              </div>

              <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
              <div className='row-modal'>
                <div style={{ 'width': '50%' }}>
                  <p className='tableTitle'>Foto tempat usaha tampak Lain </p>
                </div>
                <div style={{ 'width': '50%' }}>
                  <img className='img-inquiry' src={getFotoTempat2}></img>
                </div>
              </div>

              <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
              <div className='row-modal'>
                <div style={{ 'width': '50%' }}>
                  <p className='tableTitle'>NPWP</p>
                </div>
                <div style={{ 'width': '50%' }}>
                  <img className='img-inquiry' src={getNPWP}></img>
                </div>
              </div>

              <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
              <div className='row-modal'>
                <div style={{ 'width': '50%' }}>
                  <p className='tableTitle'>Legalitas Usaha</p>
                </div>
                <div style={{ 'width': '50%' }}>
                  <img className='img-inquiry' src={getLegal}></img>
                </div>
              </div>

            </div>
        }

        <div><br /></div>

        <div className='tableOne'>
          <div class="form-group">
            <p style={{ 'font-weight': 600, 'color': 'Indigo', 'margin-bottom': '24px' }}>Reason for Reject</p>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" defaultValue={reasonReject} disabled></textarea>
          </div>
        </div>

      </div>
    </>
  );
};