import axios from 'axios';
import React, { useEffect, useState } from 'react';
import MainMqrisService from '../services/MainMqrisService';
import Swal from 'sweetalert2'

export default function ModalTemplate(props) {
    const [appId, setAppId] = useState()
    const [usaha_norekmuamalat, setusaha_norekmuamalat] = useState()
    const [acquirerName] = useState("BANK MUAMALAT")
    const [usaha_nama25, setusaha_nama25] = useState()
    const [usaha_tipemerchant, setusaha_tipemerchant] = useState()
    const [ccy] = useState("360")
    const [alamat_kelurahan, setalamat_kelurahan] = useState()
    const [alamat_kotakab, setalamat_kotakab] = useState()
    const [countryCode] = useState("ID")
    const [alamat_kodepos, setalamat_kodepos] = useState()
    const [pemilik_ktp, setpemilik_ktp] = useState()
    const [pemilik_npwp, setpemilik_npwp] = useState()
    const [username, setUsername] = useState()
    const [created_date, setcreated_date] = useState()

    useEffect(() => {
        async function getData() {
            let dataUser = await MainMqrisService.getAppId()
            let dataLogin = await MainMqrisService.getUsername()
            setUsername(dataLogin[0].username)
            setusaha_norekmuamalat(dataUser[0].identitasUsaha.usaha_norekmuamalat)
            setusaha_nama25(dataUser[0].identitasUsaha.usaha_nama25)
            setusaha_tipemerchant(dataUser[0].identitasUsaha.usaha_tipemerchant)
            setalamat_kelurahan(dataUser[0].alamatNasabah.alamat_kelurahan)
            setalamat_kotakab(dataUser[0].alamatNasabah.alamat_kotakab)
            setalamat_kodepos(dataUser[0].alamatNasabah.alamat_kodepos)
            setpemilik_ktp(dataUser[0].identitasPemilik.pemilik_ktp)
            setpemilik_npwp(dataUser[0].identitasPemilik.pemilik_npwp)
            setAppId(dataUser[0].appId)
            setcreated_date(dataUser[0].created_date)
        }
        getData()
    }, [])

    const btnClose = () => {
        props.modalClose()
    }

    const successMBSS = () => {
        props.modalClose()
        Swal.fire({
            title: 'MBSS',
            text: 'Created Successfully',
            icon: 'success',
            confirmButtonText: 'OK'
        })
            .then((data) => {
                if (data.isConfirmed) {
                    window.location.reload()
                }
            })
    }

    const btnSave = () => {
        MainMqrisService.postMBSSMerchant({
            accountno: usaha_norekmuamalat,
            acquirername: acquirerName,
            ccy,
            countrycode: countryCode,
            merchantcity: alamat_kotakab,
            merchantlocation: alamat_kelurahan,
            merchantname: usaha_nama25,
            merchanttype: usaha_tipemerchant,
            postalcode: alamat_kodepos,
            version: 1
        })
        MainMqrisService.masterPut({
            appId: appId,
            created_date: created_date,
            username: username,
            status_code: 12,
            swalSuccess: successMBSS
        })
    }

    return (
        <>
            <div className='containerOne'>
                <div className='tableOne'>
                    <p style={{ 'font-weight': 600, 'color': 'Indigo', 'margin-bottom': '24px' }}>Merchant Qris Detail</p>
                    <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
                    <div className='row-modal'>
                        <div style={{ 'width': '50%' }}>
                            <p className='tableTitle'>Account No</p>
                        </div>
                        <div style={{ 'width': '50%' }}>
                            <p className='tableContent'>{usaha_norekmuamalat}</p>
                        </div>
                    </div>
                    <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
                    <div className='row-modal'>
                        <div style={{ 'width': '50%' }}>
                            <p className='tableTitle'>Acquirer Name</p>
                        </div>
                        <div style={{ 'width': '50%' }}>
                            <p className='tableContent'>{acquirerName}</p>
                        </div>
                    </div>
                    <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
                    <div className='row-modal'>
                        <div style={{ 'width': '50%' }}>
                            <p className='tableTitle'>Merchant Name</p>
                        </div>
                        <div style={{ 'width': '50%' }}>
                            <p className='tableContent'>{usaha_nama25}</p>
                        </div>
                    </div>
                    <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
                    <div className='row-modal'>
                        <div style={{ 'width': '50%' }}>
                            <p className='tableTitle'>Merchant Type</p>
                        </div>
                        <div style={{ 'width': '50%' }}>
                            <p className='tableContent'>{usaha_tipemerchant}</p>
                        </div>
                    </div>
                    <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
                    <div className='row-modal'>
                        <div style={{ 'width': '50%' }}>
                            <p className='tableTitle'>CCY</p>
                        </div>
                        <div style={{ 'width': '50%' }}>
                            <p className='tableContent'>{ccy}</p>
                        </div>
                    </div>
                    <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
                    <div className='row-modal'>
                        <div style={{ 'width': '50%' }}>
                            <p className='tableTitle'>Merchant Location</p>
                        </div>
                        <div style={{ 'width': '50%' }}>
                            <p className='tableContent'>{alamat_kelurahan}</p>
                        </div>
                    </div>
                    <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
                    <div className='row-modal'>
                        <div style={{ 'width': '50%' }}>
                            <p className='tableTitle'>Merchant City</p>
                        </div>
                        <div style={{ 'width': '50%' }}>
                            <p className='tableContent'>{alamat_kotakab}</p>
                        </div>
                    </div>
                    <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
                    <div className='row-modal'>
                        <div style={{ 'width': '50%' }}>
                            <p className='tableTitle'>Country Code</p>
                        </div>
                        <div style={{ 'width': '50%' }}>
                            <p className='tableContent'>{countryCode}</p>
                        </div>
                    </div>
                    <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
                    <div className='row-modal'>
                        <div style={{ 'width': '50%' }}>
                            <p className='tableTitle'>Postal Code</p>
                        </div>
                        <div style={{ 'width': '50%' }}>
                            <p className='tableContent'>{alamat_kodepos}</p>
                        </div>
                    </div>
                    <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
                    <div className='row-modal'>
                        <div style={{ 'width': '50%' }}>
                            <p className='tableTitle'>KTP Number</p>
                        </div>
                        <div style={{ 'width': '50%' }}>
                            <p className='tableContent'>{pemilik_ktp}</p>
                        </div>
                    </div>
                    <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
                    <div className='row-modal'>
                        <div style={{ 'width': '50%' }}>
                            <p className='tableTitle'>NPWP Number</p>
                        </div>
                        <div style={{ 'width': '50%' }}>
                            <p className='tableContent'>{pemilik_npwp}</p>
                        </div>
                    </div>
                </div>
                <div><br /></div>
                <div className='tableOne'>
                    <div className='box-btn-modal'>
                        <button type="button" class="btn btn-light box-btn-modal-detail" onClick={() => btnClose()}>CLOSE</button>
                        <button type="button" class="btn btn-primary box-btn-modal-detail" onClick={() => btnSave()} >SAVE</button>
                    </div>
                </div>
            </div>
        </>
    );
};