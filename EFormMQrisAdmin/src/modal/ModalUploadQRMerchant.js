import axios from 'axios';
import React, { useEffect, useState } from 'react';
import MainMqrisService from '../services/MainMqrisService';
import Swal from 'sweetalert2';

export default function ModalTemplate(props) {
  const [appId, setAppId] = useState()
  const [usaha_nama50, setusaha_nama50] = useState()
  const [fileenam, setfileenam] = useState()

  useEffect(() => {
    async function getData() {
      let dataUser = await MainMqrisService.getAppId()
      setAppId(dataUser[0].appId)
      setusaha_nama50(dataUser[0].identitas.usaha_nama50)
    }
    getData()
  }, [])

  const handleUpload = (file) => {
    setfileenam(file);
  };

  function btnUpload() {
    const url_upload_qr = MainMqrisService.uploadQris()

    var FormData = require("form-data");
    var formDataQris = new FormData();

    formDataQris.append("fileenam", fileenam)
    axios({
      url: url_upload_qr,
      method: "POST",
      headers: {
        "Content-Type": "multipart/form-data",
      },
      params: {
        appId
      },
      data: formDataQris
    })
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err)
      })
  }

  return (
    <>
      <div className='containerOne'>
        <div className='tableOne'>
          <p style={{ 'font-weight': 600, 'color': 'Indigo', 'margin-bottom': '24px' }}>Data Merchant</p>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>AppId</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{appId}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>Nama Toko/Merchant/Institusi</p>
            </div>
            <div style={{ 'width': '50%' }}>
              <p className='tableContent'>{usaha_nama50}</p>
            </div>
          </div>

          <hr style={{ 'margin': '0px', 'border': '1px solid Ghost' }} />
          <div className='row-modal'>
            <div style={{ 'width': '50%' }}>
              <p className='tableTitle'>QR</p>
            </div>
            <input
              type="file"
              id="KTP"
              name="imgktp_upload"
              onChange={(e) => handleUpload(e.target.files[0])}
            />
          </div>
        </div>

        <div className='tableOne'>
          <div className='box-btn-modal'>
            <button type="button" class="btn btn-primary box-btn-modal-detail" onClick={() => btnUpload()}>UPLOAD</button>
          </div>
        </div>
      </div>
    </>
  );
}