import React, { useEffect, useState, useContext } from 'react';
import { multiStepContext } from '../StepContext';
import MainMqrisService from '../services/MainMqrisService';

export default function Nav() {
    const { setPage } = useContext(multiStepContext);
    const [fullname, setfullname] = useState()
    const [grupUser, setgrupUser] = useState()
    const [jabatanUser, setjabatanUser] = useState()

    const btnClick = (props) => {
        if (props === 1) {
            setPage(1)
        } else if (props === 2.1) {
            setPage(2.1)
        } else if (props === 2.2) {
            setPage(2.2)
        } else if (props === 2.3) {
            setPage(2.3)
        } else if (props === 3.1) {
            setPage(3.1)
        } else if (props === 3.2) {
            setPage(3.2)
        } else if (props === 4.1) {
            setPage(4.1)
        } else if (props === 5.1) {
            setPage(5.1)
        }
    }

    useEffect(() => {
        async function getData() {
            let dataUser = await MainMqrisService.getUsername()
            let jabatan = dataUser[0].user_jabatan
            setgrupUser(dataUser[0].user_grup)
            setfullname(dataUser[0].user_fullname)
            if (jabatan == '1') {
                setjabatanUser('Admin Check')
            } else if (jabatan == '2') {
                setjabatanUser('Admin Approve')
            } else if (jabatan == '3') {
                setjabatanUser('HO')
            }
        }
        getData()
    }, [])

    return (
        <ul className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
            <li className="nav-item">
                <a className="nav-link collapsed" data-target="#collapseThree"
                    aria-expanded="false" aria-controls="collapseTwo">
                    <div className='nav-roleAdmin'>
                        <div className='nav-roleAdmin-logo'>
                            <i className="fas fa-user fa-3x img-nav"></i>
                        </div>
                        <div className='nav-roleAdmin-user'>
                            <p className='user-nav'>{fullname}</p>
                            <p className='user-nav'>{jabatanUser}</p>
                        </div>
                    </div>
                </a>
            </li>
            <hr className="sidebar-divider" />
            {grupUser == 1 ?
                //Kondisi Jika Cabang
                <div>
                    <li className="nav-item">
                        <a className="nav-link collapsed" data-target="#collapseThree"
                            aria-expanded="false" aria-controls="collapseTwo">
                            <i className="fas fa-poll"></i>
                            <span className='span-icon-navbar' onClick={() => btnClick(1)}>INQUIRY INSTRUCTION</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                            aria-expanded="true" aria-controls="collapseTwo">
                            <i className="fas fa-crown"></i>
                            <span className='span-icon-navbar'>MERCHANTS</span>
                        </a>
                        <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                            <div className="bg-white py-2 collapse-inner rounded">
                                <li className='collapse-item nav-font-custom' onClick={() => btnClick(2.1)}>
                                    Check
                                </li>
                                <li className='collapse-item nav-font-custom' onClick={() => btnClick(2.2)}>
                                    Approve
                                </li>
                            </div>
                        </div>
                    </li>
                </div>
                :
                // Kondisi Jika Pusat
                <div>
                    <li className="nav-item">
                        <a className="nav-link collapsed" data-target="#collapseThree"
                            aria-expanded="false" aria-controls="collapseTwo">
                            <i className="fas fa-poll"></i>
                            <span className='span-icon-navbar' onClick={() => btnClick(1)}>INQUIRY INSTRUCTION</span>
                        </a>
                    </li>
                    {/* <li className="nav-item">
                        <a className="nav-link collapsed" data-target="#qrUtilities"
                            aria-expanded="false" aria-controls="qrUtilities">
                            <i className="fas fa-upload"></i>
                            <span className='span-icon-navbar' onClick={() => btnClick(4.1)}>UPLOAD QR MERCHANTS</span>
                        </a>
                    </li> */}
                    <li className="nav-item">
                        <a className="nav-link collapsed" data-target="#qrUtilities"
                            aria-expanded="false" aria-controls="qrUtilities">
                            <i className="fas fa-pen"></i>
                            <span className='span-icon-navbar' onClick={() => btnClick(5.1)}>UPDATE NMID</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
                            aria-expanded="true" aria-controls="collapseUtilities">
                            <i className="fas fa-server"></i>
                            <span className='span-icon-navbar'>MBSS</span>
                        </a>
                        <div id="collapseUtilities" className="collapse" aria-labelledby="headingUtilities"
                            data-parent="#accordionSidebar">
                            <div className="bg-white py-2 collapse-inner rounded">
                                <li className='collapse-item nav-font-custom' onClick={() => btnClick(3.1)}>
                                    Merchant
                                </li>
                                <li className='collapse-item nav-font-custom' onClick={() => btnClick(3.2)}>
                                    PAN
                                </li>
                            </div>
                        </div>
                    </li>
                </div>
            }
        </ul>
    )
} 