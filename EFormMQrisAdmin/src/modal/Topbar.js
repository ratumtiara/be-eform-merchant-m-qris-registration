import React, { useState } from 'react';
import Modal from 'react-modal'
import ModalChangePassword from './ModalChangePassword'

export default function Topbar() {
    const [isOpenEdit, setIsOpenEdit] = useState();

    function logoutUser() {
        localStorage.clear();
        sessionStorage.clear();
        window.location.reload()
    }

    const toggleModalEdit = () => {
        setIsOpenEdit(true);
    }

    return (
        <nav className="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

            <button id="sidebarToggleTop" className="btn btn-link d-md-none rounded-circle mr-3">
                <i className="fa fa-bars"></i>
            </button>
            <Modal
                isOpen={isOpenEdit}
                onRequestClose={toggleModalEdit}
                contentLabel="My dialog"
                className="modal-bg"
            >
                <ModalChangePassword />
            </Modal>
            <ul className="navbar-nav ml-auto">
                <li className="nav-item dropdown no-arrow" >
                    <a className="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                        data-toggle="dropdown" aria-hashpopup="true" aria-expanded="false" onClick={() => toggleModalEdit()}>
                        <div className="icon-setting-01">
                            <i class="fas fa-key fa-lg color-icon" ></i>
                        </div>
                    </a>
                </li>
                <li className="nav-item dropdown no-arrow">
                    <a className="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onClick={() => logoutUser()}>
                        <div className='icon-logout-01'>
                            <i class="fas fa-sign-out-alt fa-lg color-icon"></i>
                        </div>
                        <div>
                            <span className="mr-2 d-none d-lg-inline txt-logout">Log Out</span>
                        </div>
                    </a>
                </li>
            </ul>
        </nav>
    )
}