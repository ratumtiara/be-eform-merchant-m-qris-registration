import InquiryInstructionBranch from '../component/InquiryInstructionBranch';
import InquiryInstructionKPO from '../component/InquiryInstructionKPO';
import MBSSMerchants from '../component/MBSSMerchants';
import MBSSPan from '../component/MBSSPan';
import MerchantsApprove from '../component/MerchantsApprove';
import MerchantsCheck from '../component/MerchantsCheck';
import UploadQRMerchant from '../component/UploadQRMerchant'
import InQuiryInstruction from '../component/InquiryInstruction';
import UpdateNMID from '../component/UpdateNMID';

import Nav from '../modal/Nav';
import Topbar from '../modal/Topbar';
import React, { useContext } from "react";
import { multiStepContext } from './../StepContext';

export default function UserForm() {
    const { page, setPage } = useContext(multiStepContext);

    function showPage(page) {

        switch (page) {
            case 1:
                sessionStorage.setItem('currentPage', 1)
                return (
                    <div id="wrapper">
                        <Nav />
                        <div id="content-wrapper" className="d-flex flex-column">
                            <div id="content">
                                <Topbar />
                                <InQuiryInstruction />
                            </div>
                        </div>
                    </div>
                )
            case 1.1:
                sessionStorage.setItem('currentPage', 1.1)
                return (
                    <div id="wrapper">
                        <Nav />
                        <div id="content-wrapper" className="d-flex flex-column">
                            <div id="content">
                                <Topbar />
                                <InquiryInstructionKPO />
                            </div>
                        </div>
                    </div>
                )
            case 1.2:
                sessionStorage.setItem('currentPage', 1)
                return (
                    <div id="wrapper">
                        <Nav />
                        <div id="content-wrapper" className="d-flex flex-column">
                            <div id="content">
                                <Topbar />
                                <InquiryInstructionBranch />
                            </div>
                        </div>
                    </div>
                )
            case 2.1:
                sessionStorage.setItem('currentPage', 2.1)
                return (
                    <div id="wrapper">
                        <Nav />
                        <div id="content-wrapper" className="d-flex flex-column">
                            <div id="content">
                                <Topbar />
                                <MerchantsCheck />
                            </div>
                        </div>
                    </div>
                )
            case 2.2:
                sessionStorage.setItem('currentPage', 2.2)
                return (
                    <div id="wrapper">
                        <Nav />
                        <div id="content-wrapper" className="d-flex flex-column">
                            <div id="content">
                                <Topbar />
                                <MerchantsApprove />
                            </div>
                        </div>
                    </div>
                )
            case 3.1:
                sessionStorage.setItem('currentPage', 3.1)
                return (
                    <div id="wrapper">
                        <Nav />
                        <div id="content-wrapper" className="d-flex flex-column">
                            <div id="content">
                                <Topbar />
                                <MBSSMerchants />
                            </div>
                        </div>
                    </div>
                )
            case 3.2:
                sessionStorage.setItem('currentPage', 3.2)
                return (
                    <div id="wrapper">
                        <Nav />
                        <div id="content-wrapper" className="d-flex flex-column">
                            <div id="content">
                                <Topbar />
                                <MBSSPan />
                            </div>
                        </div>
                    </div>
                )
            case 4.1:
                sessionStorage.setItem('currentPage', 4.1)
                return (
                    <div id="wrapper">
                        <Nav />
                        <div id="content-wrapper" className="d-flex flex-column">
                            <div id="content">
                                <Topbar />
                                <UploadQRMerchant />
                            </div>
                        </div>
                    </div>
                )
            case 5.1:
                sessionStorage.setItem('currentPage', 5.1)
                return (
                    <div id="wrapper">
                        <Nav />
                        <div id="content-wrapper" className="d-flex flex-column">
                            <div id="content">
                                <Topbar />
                                <UpdateNMID />
                            </div>
                        </div>
                    </div>
                )
        }
    }

    return (
        <>
            {showPage(page)}
        </>
    )
}