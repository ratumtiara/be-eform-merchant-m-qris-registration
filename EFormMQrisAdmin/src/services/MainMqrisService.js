import axios from "axios";

const API = 'http://localhost'
// const API = 'http://10.55.54.8'

// ------------------------------------------------ Branch
let URL_DEV_1 = API + ':16410/mqris/eform/app_master/get/getInquiryBranch/' // Get Inquiry Branch
let URL_DEV_1_1 = API + ':16410/mqris/eform/app_master/get/getInquiryBranchFilter/' // Get Inquiry Branch Filter
let URL_DEV_1_2 = API + ':16410/mqris/eform/app_master/get/getDataMerchantCheck/' // Get Data Merchant Check
let URL_DEV_1_6 = API + ':16410/mqris/eform/app_master/get/getDataMerchantApprove/' // Get Data Merchant Approve
let URL_DEV_1_7 = API + ':16410/mqris/eform/app_master/get/getDataMerchantsCheckBM/' // Get Data Merchant Approve
let URL_DEV_1_3 = API + ':16410/mqris/eform/app_master/get/getDataMerchantFilter/' // Get Data Merchant Filter
let URL_DEV_1_4 = API + ':16410/mqris/eform/form_identitas_usaha/put/' // Put Identitas Usaha By AppId
let URL_DEV_1_5 = API + ':2010/mqris/mailservice/respond/' // Send Email Reject
async function getInquiryBranch(props) {
  const { user_branch, username, user_jabatan } = props
  if (user_jabatan === '1') {
    let value = await fetch(URL_DEV_1 + user_branch + '/' + username)
    value = await value.json()
    return value
  } else if (user_jabatan === '2') {
    let value = await fetch(URL_DEV_1 + user_branch)
    value = await value.json()
    return value
  }
}
async function getInquiryBranchFilter(props) {
  const {
    branch,
    paramValue
  } = props
  let value = await fetch(URL_DEV_1_1 + branch + '/' + paramValue)
  value = await value.json()
  return value
}
async function getDataMerchantsCheck(props) {
  const {
    user_branch, username
  } = props
  let value = await fetch(URL_DEV_1_2 + user_branch + '/' + username)
  value = await value.json()
  console.log(value);
  return value
}
function checkMerchant(props) {
  const {
    user_jabatan,
    appId,
    usaha_bankterdekat,
    usaha_klasifikasi,
    usaha_kodereferal,
    usaha_mcc,
    usaha_mdr,
    usaha_nama25,
    usaha_nama50,
    usaha_namasesuainorek,
    usaha_norekmuamalat,
    usaha_siup,
    usaha_terminal,
    usaha_tipemerchant,
    usaha_tipeqr,
    created_date,
    username,
    swalSuccess
  } = props
  if (user_jabatan === '1') {
    masterPut({
      appId,
      created_date,
      username,
      status_code: 2,
      swalSuccess
    })
  } else if (user_jabatan === '2') {
    updateIdentitasUsaha({
      appId,
      usaha_bankterdekat,
      usaha_klasifikasi,
      usaha_kodereferal,
      usaha_mcc,
      usaha_mdr,
      usaha_nama25,
      usaha_nama50,
      usaha_namasesuainorek,
      usaha_norekmuamalat,
      usaha_siup,
      usaha_terminal,
      usaha_tipemerchant,
      usaha_tipeqr,
      swalSuccess
    })
  }
}
function rejectCheckMerchant(props) {
  const {
    appId,
    pemilik_email,
    respond_admin,
    username,
    created_date,
    swalSuccess
  } = props

  sendEmailReject({
    appId,
    pemilik_email,
    respond_admin
  })
  masterPut({
    appId,
    created_date,
    username,
    status_code: 6,
    swalSuccess
  })
}
function approveMerchant(props) {
  const {
    appId,
    initial_mid,
    initial_mpan,
    initial_mqris,
    usaha_bankterdekat,
    usaha_klasifikasi,
    usaha_kodereferal,
    usaha_mcc,
    usaha_mdr,
    usaha_nama25,
    usaha_nama50,
    usaha_namasesuainorek,
    usaha_norekmuamalat,
    usaha_siup,
    usaha_terminal,
    usaha_tipemerchant,
    usaha_tipeqr,
    username,
    created_date,
    swalSuccess
  } = props
  updateIdentitasUsaha({
    appId,
    usaha_bankterdekat,
    usaha_klasifikasi,
    usaha_kodereferal,
    usaha_mcc,
    usaha_mdr,
    usaha_nama25,
    usaha_nama50,
    usaha_namasesuainorek,
    usaha_norekmuamalat,
    usaha_siup,
    usaha_terminal,
    usaha_tipemerchant,
    usaha_tipeqr,
  })
  updateInitial({
    appId,
    initial_mid,
    initial_mpan,
    initial_mqris,
  })
  masterPut({
    appId,
    created_date,
    username,
    status_code: 3,
    swalSuccess
  })
}
function rejectApproveMerchant(props) {
  const {
    appId,
    pemilik_email,
    respond_admin,
    username,
    created_date,
    swalSuccess
  } = props

  sendEmailReject({
    appId,
    pemilik_email,
    respond_admin
  })
  masterPut({
    appId,
    created_date,
    username,
    status_code: 7,
    swalSuccess
  })

}
async function getDataMerchantsApprove(props) {
  const {
    user_branch
  } = props
  let value = await fetch(URL_DEV_1_6 + user_branch)
  value = await value.json()
  return value
}
async function getDataMerchantsCheckBM(props) {
  const {
    user_branch
  } = props
  let value = await fetch(URL_DEV_1_7 + user_branch)
  value = await value.json()
  return value
}
async function getDataFilterMerchant(props) {
  const {
    branch,
    paramValue,
    statusCode
  } = props
  let value = await fetch(URL_DEV_1_3 + branch + '/' + paramValue + '/' + statusCode)
  value = await value.json()
  return value
}
function updateIdentitasUsaha(props) {
  const {
    appId,
    usaha_bankterdekat,
    usaha_klasifikasi,
    usaha_kodereferal,
    usaha_mcc,
    usaha_mdr,
    usaha_nama25,
    usaha_nama50,
    usaha_namasesuainorek,
    usaha_norekmuamalat,
    usaha_siup,
    usaha_terminal,
    usaha_tipemerchant,
    usaha_tipeqr,
    swalSuccess
  } = props
  axios({
    url: URL_DEV_1_4 + appId,
    method: 'PUT',
    data: {
      appId,
      usaha_bankterdekat,
      usaha_klasifikasi,
      usaha_kodereferal,
      usaha_mcc,
      usaha_mdr,
      usaha_nama25,
      usaha_nama50,
      usaha_namasesuainorek,
      usaha_norekmuamalat,
      usaha_siup,
      usaha_terminal,
      usaha_tipemerchant,
      usaha_tipeqr,
    }
  })
    .then((res) => { swalSuccess() })
    .catch((err) => { console.log(err) })
}
function sendEmailReject(props) {
  const {
    appId,
    pemilik_email,
    respond_admin
  } = props;
  axios({
    url: URL_DEV_1_5,
    method: "POST",
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "application/json",
    },
    data: {
      from: "email.testing.sageri@gmail.com",
      pemilik_email,
      appId,
      respond_admin,
      respond_url: "https://dev.bankmuamalat.co.id/mapdb/mqris/revisi",
      model: {
        additionalProp1: "string",
        additionalProp2: "string",
        additionalProp3: "string"
      }
    }
  })
    .then((res) => { console.log(res) })
    .catch((err) => { console.log(err) })
}

// ------------------------------------------------ KPO
let URL_DEV_2 = API + ':16410/mqris/eform/app_master/get/getInquiryKPO' // Get Inquiry KPO
let URL_DEV_2_1 = API + ':16410/mqris/eform/app_master/get/getInquiryKPOFilter/' // Get Inquiry KPO Filter
let URL_DEV_2_2 = API + ':16410/mqris/eform/app_master/get/getStatusCode/3' // Get Data Export KPO
let URL_DEV_2_3 = API + ':16410/mqris/eform/app_master/get/getAllJpa' // Get Data Export ALL
let URL_DEV_2_4 = API + ':16410/mqris/eform/app_master/get/getStatusCode/4' // Get MBSS Merchant
let URL_DEV_2_5 = API + ':16430/mqris/eformqris/post' // Post MBSS Merchant
let URL_DEV_2_6 = API + ':16430/mqris/eformqris/app_master/get/' // Get MBSS Pan
let URL_DEV_2_7 = API + ':19176/readqrissticker' // Post MBSS Pan
let URL_DEV_2_8 = API + ':16410/mqris/eform/app_master/get/getUpdateNMID' // Get Update NMID
let URL_DEV_2_9 = API + ':16430/mqris/eformqris/app_master/getNMIDMerchantPan/' // Get NMID From merchantpan By panno
let URL_DEV_2_10 = API + ':16410/mqris/eform/form_initial/get/getAppId/' // Get Initial By AppId
let URL_DEV_2_11 = API + ':16410/mqris/eform/form_initial/put/' // Put Initial By AppId

async function getInquiryKPO() {
  let value = await fetch(URL_DEV_2)
  value = await value.json()
  return value
}
async function getInquiryKPOFilter(props) {
  const {
    paramValue
  } = props

  let value = await fetch(URL_DEV_2_1 + paramValue)
  value = await value.json()
  return value
}
async function getDataExportKPO() {
  let value = await fetch(URL_DEV_2_2)
  value = await value.json()
  return value
}
async function getDataExportAll() {
  let value = await fetch(URL_DEV_2_3)
  value = await value.json()
  return value
}
async function getMBSSMerchant() {
  let value = await fetch(URL_DEV_2_4)
  value = await value.json()
  return value
}
function postMBSSMerchant(props) {
  const {
    accountno,
    acquirername,
    ccy,
    countrycode,
    merchantcity,
    merchantlocation,
    merchantname,
    merchanttype,
    postalcode,
    version,
    successMBSS
  } = props
  axios({
    url: URL_DEV_2_5,
    method: 'POST',
    data: {
      accountno,
      acquirername,
      ccy,
      countrycode,
      merchantcity,
      merchantlocation,
      merchantname,
      merchanttype,
      postalcode,
      version
    }
  })
    .then((res) => {
      successMBSS()
    })
    .catch((err) => {
      console.log(err);
    })
}
async function getMBSSPan() {
  let value = await fetch(URL_DEV_2_6)
  value = await value.json()
  return value
}
function postMBSSPan() {
  axios({
    url: URL_DEV_2_7,
    method: 'POST',
  })
    .then((res) => {
      window.location.reload()
    })
    .catch((err) => {
      console.log(err);
    })
}
async function getUpdateNMID() {
  let value = await fetch(URL_DEV_2_8)
  value = await value.json()
  return value
}
function postUpdateNMID(props) {
  const { appId, panno, swalUpdateNMIDNew, dataUserLogin } = props
  axios({
    url: URL_DEV_2_9 + panno,
    method: 'GET'
  })
    .then((res) => {
      let dataNMID = res.data[0]
      // Get Data Initial
      axios({
        url: URL_DEV_2_10 + appId,
        method: 'GET'
      })
        .then((res) => {
          axios({
            url: URL_DEV_2_11 + appId,
            method: 'PUT',
            data: {
              "appId": appId,
              "initial_mqris": res.data[0].initial_mqris,
              "initial_nmid": dataNMID,
              "initial_mpan": res.data[0].initial_mpan,
              "initial_mid": res.data[0].initial_mid
            }
          })
            .then((res) => {
              masterPut({
                appId: appId,
                username: dataUserLogin[0].username,
                status_code: 5,
                swalSuccess: swalUpdateNMIDNew
              })
            })
            .catch((err) => {
              console.log(err);
            })
        })
        .catch((err) => {
          console.log(err);
        })
    })
    .catch((err) => {
      console.log(err);
    })
}
function updateInitial(props) {
  const {
    appId,
    initial_mid,
    initial_mpan,
    initial_mqris
  } = props
  axios({
    url: URL_DEV_2_11 + appId,
    method: 'PUT',
    data: {
      "appId": appId,
      "initial_mid": initial_mid,
      "initial_mpan": initial_mpan,
      "initial_mqris": initial_mqris,
    }
  })
    .then((res) => { console.log(res) })
    .catch((err) => { console.log(err) })
}
// ------------------------------------------------
let URL_DEV_30 = API + ':16420/mqris/eformdoc/upload_foto_1/get/' //
let URL_DEV_31 = API + ':16420/mqris/eformdoc/upload_foto_2/get/' //
let URL_DEV_32 = API + ':16420/mqris/eformdoc/upload_ktp/get/' //
let URL_DEV_33 = API + ':16420/mqris/eformdoc/upload_legal/get/' //
let URL_DEV_34 = API + ':16420/mqris/eformdoc/upload_npwp/get/' //
async function getTempat1() {
  return URL_DEV_30
}
async function getTempat2() {
  return URL_DEV_31
}
async function getKTP() {
  return URL_DEV_32
}
async function getLegal() {
  return URL_DEV_33
}
async function getNPWP() {
  return URL_DEV_34
}
// ------------------------------------------------
let URL_DEV_APPID = API + ':16410/mqris/eform/app_master/get/getAppId/' // Get AppId 
let URL_DEV_02 = API + ':16410/mqris/eform/app_master/put/' // Put Master By AppId
let URL_DEV_40 = API + ':16410/mqris/eform/user/login/' // Get One Login
let URL_DEV_41 = API + ':16410/mqris/eform/user/changepassword/' // Change Password
async function getAppId() {
  let appId = localStorage.getItem('appId')
  let value = await fetch(URL_DEV_APPID + appId)
  value = await value.json()
  return value
}
function masterPut(props) {
  const {
    appId,
    created_date,
    username,
    status_code,
    swalSuccess
  } = props
  axios({
    url: URL_DEV_02 + appId,
    method: 'PUT',
    data: {
      "appId": appId,
      "status_code": status_code,
      "created_date": created_date,
      "modified_by": username,
      "modified_date": Date.now(),
    }
  })
    .then((res) => { swalSuccess() })
    .catch((err) => { console.log(err); })
}
function getLoginUser() {
  return URL_DEV_40
}
async function getUsername() {
  let dataUser = localStorage.getItem('username')
  let value = await fetch(URL_DEV_40 + dataUser)
  value = await value.json()
  return value
}
function getpasswordUser() {
  return URL_DEV_41
}

export default {
  getAppId,
  getLoginUser,
  masterPut,
  getUsername,
  updateIdentitasUsaha,
  getpasswordUser,
  sendEmailReject,
  getKTP,
  getNPWP,
  getLegal,
  getTempat1,
  getTempat2,
  getInquiryKPOFilter,
  getDataFilterMerchant,
  getInquiryKPO,
  getInquiryBranch,
  updateInitial,
  getDataExportKPO,
  getDataExportAll,
  getMBSSMerchant,
  postMBSSMerchant,
  getMBSSPan,
  postMBSSPan,
  getUpdateNMID,
  postUpdateNMID,
  getInquiryBranchFilter,
  getDataMerchantsCheck,
  getDataMerchantsApprove,
  getDataMerchantsCheckBM,
  checkMerchant,
  approveMerchant,
  rejectCheckMerchant,
  rejectApproveMerchant
}