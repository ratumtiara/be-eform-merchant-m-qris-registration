package com.bmi.EFormMQrisMBSS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EFormMQrisMbssApplication {

	public static void main(String[] args) {
		SpringApplication.run(EFormMQrisMbssApplication.class, args);
	}

}
