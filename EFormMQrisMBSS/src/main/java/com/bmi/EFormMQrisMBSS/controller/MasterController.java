package com.bmi.EFormMQrisMBSS.controller;

import java.util.List;

import com.bmi.EFormMQrisMBSS.entity.MerchantEntity;
import com.bmi.EFormMQrisMBSS.repository.MasterRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/mqris/eformqris")
public class MasterController {

    @Autowired
    private MasterRepository masterRepository;

    @PostMapping("/post")
    public ResponseEntity<MerchantEntity> post(@RequestBody MerchantEntity merchantEntity) {
        return new ResponseEntity<>(masterRepository.save(merchantEntity), HttpStatus.OK);
    }

    @GetMapping("/app_master/get/{noRekening}")
    public List<?> getOnePan(@PathVariable String noRekening) {
        return masterRepository.getOneMerhantPan(noRekening);
    }

    @GetMapping("/app_master/get/")
    public List<?> getAllPan() {
        return masterRepository.getAllMerchantPan();
    }

    @GetMapping("/app_master/getNMIDMerchantPan/{panno}")
    public List<?> getNMIDMerchantPan(@PathVariable String panno) {
        return masterRepository.getNMIDMerchantPan(panno);
    }
}