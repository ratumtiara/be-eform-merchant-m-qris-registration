package com.bmi.EFormMQrisMBSS.entity;

import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.lang.NonNull;

@Entity
@Table(name = "merchant")
public class MerchantEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @NonNull
    private Long id;

    @Column(name = "created")
    private Date created = new Date();

    @Column(name = "updated")
    private Date updated = new Date();

    @Column(name = "deleted")
    private LocalDateTime deleted;

    @Column(name = "version")
    private Long version;

    @Column(name = "accountno")
    private String accountno;

    @Column(name = "acquirername")
    private String acquirername;

    @Column(name = "ccy")
    private String ccy;

    @Column(name = "countrycode")
    private String countrycode;

    @Column(name = "merchantcity")
    private String merchantcity;

    @Column(name = "merchantlocation")
    private String merchantlocation;

    @Column(name = "merchantname")
    private String merchantname;

    @Column(name = "merchanttype")
    private String merchanttype;

    @Column(name = "postalcode")
    private String postalcode;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public LocalDateTime getDeleted() {
        return this.deleted;
    }

    public void setDeleted(LocalDateTime deleted) {
        this.deleted = deleted;
    }

    public Long getVersion() {
        return this.version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getAccountno() {
        return this.accountno;
    }

    public void setAccountno(String accountno) {
        this.accountno = accountno;
    }

    public String getAcquirername() {
        return this.acquirername;
    }

    public void setAcquirername(String acquirername) {
        this.acquirername = acquirername;
    }

    public String getCcy() {
        return this.ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getCountrycode() {
        return this.countrycode;
    }

    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode;
    }

    public String getMerchantcity() {
        return this.merchantcity;
    }

    public void setMerchantcity(String merchantcity) {
        this.merchantcity = merchantcity;
    }

    public String getMerchantlocation() {
        return this.merchantlocation;
    }

    public void setMerchantlocation(String merchantlocation) {
        this.merchantlocation = merchantlocation;
    }

    public String getMerchantname() {
        return this.merchantname;
    }

    public void setMerchantname(String merchantname) {
        this.merchantname = merchantname;
    }

    public String getMerchanttype() {
        return this.merchanttype;
    }

    public void setMerchanttype(String merchanttype) {
        this.merchanttype = merchanttype;
    }

    public String getPostalcode() {
        return this.postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }
}