package com.bmi.EFormMQrisMBSS.repository;

import java.util.List;

import com.bmi.EFormMQrisMBSS.entity.MerchantEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MasterRepository extends JpaRepository<MerchantEntity, Long> {

    @Query(value = "select * from  merchant m where accountno = ?1", nativeQuery = true)
    List<?> getOneMerhantPan(String noRekening);

    @Query(value = "select id,accountno, criteria,merchantid,panno,acqdomain,tagid from merchantpan m order by id DESC", nativeQuery = true)
    List<?> getAllMerchantPan();

    @Query(value = "select distinct merchantid from merchantpan m where panno = ?1", nativeQuery = true)
    List<?> getNMIDMerchantPan(String panno);
}