package com.bmi.qris;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QrisApplication {

	public static void main(String[] args) {
		SpringApplication.run(QrisApplication.class, args);
	}
}