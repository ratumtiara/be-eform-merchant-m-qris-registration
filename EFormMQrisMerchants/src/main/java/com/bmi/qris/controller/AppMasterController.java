package com.bmi.qris.controller;

import java.util.List;

import javax.validation.Valid;

import com.bmi.qris.entity.AppMasterEntity;
import com.bmi.qris.repository.AppMasterRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/mqris/eform/app_master")
public class AppMasterController {

    @Autowired
    private AppMasterRepository appMasterRepository;

    RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory(5000));

    final String resGetNoRekening = "http://localhost:16410/mqris/eform/app_master/get/getNorekening/";
    final String resProvinsi = "http://localhost:16410/provinsi/getAll";
    final String resKabKota = "http://localhost:16410/kotakab/getOne/";
    final String resKecamatan = "http://localhost:16410/kecamatan/getOne/";
    final String resKelurahan = "http://localhost:16410/kelurahan/getOne/";
    final String resKodePos = "http://localhost:16410/kodepos/getOne/";
    final String resKCUBMI = "http://localhost:16410/daftar_kcu/getOne/";

    private ClientHttpRequestFactory getClientHttpRequestFactory(int timeout) {
        RequestConfig config = RequestConfig.custom().setConnectTimeout(timeout).setConnectionRequestTimeout(timeout)
                .setSocketTimeout(timeout).build();
        CloseableHttpClient client = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
        return new HttpComponentsClientHttpRequestFactory(client);
    }

    // Post Master
    @PostMapping("/post")
    public ResponseEntity<AppMasterEntity> create(@RequestBody AppMasterEntity newAppMaster) {
        try {
            return new ResponseEntity<>(appMasterRepository.save(newAppMaster), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // Put Master
    @PutMapping("/put/{appId}")
    public @Valid AppMasterEntity update(@PathVariable(value = "appId") Long appId,
            @RequestBody AppMasterEntity appMaster) {
        return appMasterRepository.save(appMaster);
    }

    // Main Get One
    @GetMapping("/get/{param1}/{value}")
    public List<?> getOne(@PathVariable Long param1, @PathVariable String value) {
        if (param1 == 1) { // Get No Rekening
            @SuppressWarnings("unchecked")
            List<String> lRet = restTemplate.getForEntity(resGetNoRekening + value, List.class).getBody();
            return lRet;
        } else if (param1 == 2) { // Provinsi
            @SuppressWarnings("unchecked")
            List<String> lRet = restTemplate.getForEntity(resProvinsi, List.class).getBody();
            return lRet;
        } else if (param1 == 3) { // Kabupaten Kota
            @SuppressWarnings("unchecked")
            List<String> lRet = restTemplate.getForEntity(resKabKota + value, List.class).getBody();
            return lRet;
        } else if (param1 == 4) { // Kecamatan
            @SuppressWarnings("unchecked")
            List<String> lRet = restTemplate.getForEntity(resKecamatan + value, List.class).getBody();
            return lRet;
        } else if (param1 == 5) { // Kelurahan
            @SuppressWarnings("unchecked")
            List<String> lRet = restTemplate.getForEntity(resKelurahan + value, List.class).getBody();
            return lRet;
        } else if (param1 == 6) { // KCU BMI
            @SuppressWarnings("unchecked")
            List<String> lRet = restTemplate.getForEntity(resKCUBMI + value, List.class).getBody();
            return lRet;
        } else {
            System.out.println(false);
            return null;
        }
    }

    // Main Get Two
    @GetMapping("/get/{param1}/{value1}/{value2}")
    public List<?> getTwo(@PathVariable Long param1, @PathVariable String value1, @PathVariable String value2) {
        if (param1 == 1) { // Kode Pos
            @SuppressWarnings("unchecked")
            List<String> lRet = restTemplate.getForEntity(resKodePos + value1 + '/' + value2, List.class)
                    .getBody();
            return lRet;
        } else {
            System.out.println(false);
            return null;
        }
    }

    // Get Inquiry KPO
    @GetMapping("/get")
    public List<?> getAll() {
        return appMasterRepository.getAll();
    }

    // Get Inquiry KPO Filter
    @GetMapping("/get/getInquiryKPO")
    public List<?> getInquiryKPO() {
        return appMasterRepository.getInquiryKPO();
    }

    // Get Inquiry KPO Filter
    @GetMapping("/get/getInquiryKPOFilter/{value}")
    public List<?> getInquiryKPOFilter(@PathVariable String value) {
        return appMasterRepository.getInquiryKPOFilter(value);
    }

    // Get All JPA
    @GetMapping("/get/getAllJpa")
    public List<AppMasterEntity> getAllJpa() {
        return appMasterRepository.getAllJPA();
    }

    // Get App Id
    @GetMapping("/get/getAppId/{appId}")
    public List<AppMasterEntity> getAppId(@PathVariable Long appId) {
        return appMasterRepository.getAppId(appId);
    }

    // Get Status Code JPA
    @GetMapping("/get/getStatusCode/{status_code}")
    public List<AppMasterEntity> getStatusCode(@PathVariable long status_code) {
        return appMasterRepository.getStatusCode(status_code);
    }

    // Get No Rekening
    @GetMapping("/get/getNorekening/{usaha_norekmuamalat}")
    public List<?> getNorekening(@PathVariable Long usaha_norekmuamalat) {
        return appMasterRepository.getNoRekening(usaha_norekmuamalat);
    }

    // Get Update NMID
    @GetMapping("/get/getUpdateNMID")
    public List<?> getUpdateNMID() {
        return appMasterRepository.getUpdateNMID();
    }

    // ------------------------------------------------
    // Get Inquiry Branch BM
    @GetMapping("/get/getInquiryBranch/{usaha_bankterdekat}")
    public List<?> getInquiryBranch(@PathVariable String usaha_bankterdekat) {
        return appMasterRepository.getInquiryBranchBM(usaha_bankterdekat);
    }

    // Get Inquiry Branch CS RM
    @GetMapping("/get/getInquiryBranch/{usaha_bankterdekat}/{usaha_kodereferal}")
    public List<?> getInquiryBranch(@PathVariable String usaha_bankterdekat, @PathVariable String usaha_kodereferal) {
        return appMasterRepository.getInquiryBranchCSRM(usaha_bankterdekat, usaha_kodereferal);
    }

    // Get Inquiry Branch Filter
    @GetMapping("/get/getInquiryBranchFilter/{usaha_bankterdekat}/{value}")
    public List<?> getInquiryBranchFilter(@PathVariable String usaha_bankterdekat, @PathVariable String value) {
        return appMasterRepository.getInquiryBranchFilter(usaha_bankterdekat, value);
    }

    // Get Data Merchant Check
    @GetMapping("/get/getDataMerchantCheck/{usaha_bankterdekat}/{usaha_kodereferal}")
    public List<?> getDataMerchantCheck(@PathVariable String usaha_bankterdekat,
            @PathVariable String usaha_kodereferal) {
        return appMasterRepository.getDataMerchantCheck(usaha_bankterdekat, usaha_kodereferal);
    }

    // Get Data Merchant Approve
    @GetMapping("/get/getDataMerchantApprove/{usaha_bankterdekat}")
    public List<?> getDataMerchantApprove(@PathVariable String usaha_bankterdekat) {
        return appMasterRepository.getDataMerchantApprove(usaha_bankterdekat);
    }

    // Get Data Merchant Check BM
    @GetMapping("/get/getDataMerchantsCheckBM/{usaha_bankterdekat}")
    public List<?> getDataMerchantsCheckBM(@PathVariable String usaha_bankterdekat) {
        return appMasterRepository.getDataMerchantsCheckBM(usaha_bankterdekat);
    }

    // Get Data Merchants Filter
    @GetMapping("/get/getDataMerchantFilter/{usaha_bankterdekat}/{value}/{status_code}")
    public List<?> getDataMerchantFilter(@PathVariable String usaha_bankterdekat, @PathVariable String value,
            @PathVariable Long status_code) {
        return appMasterRepository.getDataMerchantFilter(usaha_bankterdekat, value, status_code);
    }
}