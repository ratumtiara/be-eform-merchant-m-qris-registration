package com.bmi.qris.controller;

import com.bmi.qris.entity.AlamatNasabahEntity;
import com.bmi.qris.repository.AlamatNasabahRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/mqris/eform/form_alamat_nasabah")
public class FormAlamatNasabahController {

    @Autowired
    private AlamatNasabahRepository alamatNasabahRepository;

    @PostMapping("/post")
    public ResponseEntity<AlamatNasabahEntity> create(@RequestBody AlamatNasabahEntity newAlamatNasabah) {
        try {
            return new ResponseEntity<>(alamatNasabahRepository.save(newAlamatNasabah), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/put/{appId}")
    public ResponseEntity<AlamatNasabahEntity> update(@PathVariable("appId") Long appId,
            @RequestBody AlamatNasabahEntity aNasabah) {
        Optional<AlamatNasabahEntity> alamatNasabahData = alamatNasabahRepository.findById(appId);

        if (alamatNasabahData.isPresent()) {
            AlamatNasabahEntity alamatNasabah = alamatNasabahData.get();

            alamatNasabah.setAlamat_jalan(aNasabah.getAlamat_jalan());
            alamatNasabah.setAlamat_provinsi(aNasabah.getAlamat_provinsi());
            alamatNasabah.setAlamat_kotakab(aNasabah.getAlamat_kotakab());
            alamatNasabah.setAlamat_kecamatan(aNasabah.getAlamat_kecamatan());
            alamatNasabah.setAlamat_kelurahan(aNasabah.getAlamat_kelurahan());
            alamatNasabah.setAlamat_kodepos(aNasabah.getAlamat_kodepos());
            alamatNasabah.setAlamat_kordinat(aNasabah.getAlamat_kordinat());
            alamatNasabah.setPribadi_jalan(aNasabah.getPribadi_jalan());
            alamatNasabah.setPribadi_provinsi(aNasabah.getPribadi_provinsi());
            alamatNasabah.setPribadi_kotakab(aNasabah.getPribadi_kotakab());
            alamatNasabah.setPribadi_kecamatan(aNasabah.getPribadi_kecamatan());
            alamatNasabah.setPribadi_kelurahan(aNasabah.getPribadi_kelurahan());
            alamatNasabah.setPribadi_kodepos(aNasabah.getPribadi_kodepos());

            return new ResponseEntity<>(alamatNasabahRepository.save(alamatNasabah), HttpStatus.OK);

        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}