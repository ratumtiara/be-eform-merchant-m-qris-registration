package com.bmi.qris.controller;

import com.bmi.qris.entity.*;
import com.bmi.qris.repository.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/mqris/eform/form_identitas_pemilik")
public class FormIdentitasPemilikController {

    @Autowired
    private IdentitasPemilikRepository identitasPemilikRepository;

    @PostMapping("/post")
    public ResponseEntity<IdentitasPemilikEntity> create(@RequestBody IdentitasPemilikEntity newIdentitasPemilik) {
        try {
            return new ResponseEntity<>(identitasPemilikRepository.save(newIdentitasPemilik), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/put/{appId}")
    public ResponseEntity<IdentitasPemilikEntity> update(@PathVariable("appId") Long appId,
            @RequestBody IdentitasPemilikEntity identitasPemilik) {
        Optional<IdentitasPemilikEntity> identitasPemilikData = identitasPemilikRepository.findById(appId);

        if (identitasPemilikData.isPresent()) {
            IdentitasPemilikEntity identitasPemilik2 = identitasPemilikData.get();

            identitasPemilik2.setPemilik_email(identitasPemilik.getPemilik_email());
            identitasPemilik2.setPemilik_handpone(identitasPemilik.getPemilik_handpone());
            identitasPemilik2.setPemilik_ktp(identitasPemilik.getPemilik_ktp());
            identitasPemilik2.setPemilik_nama(identitasPemilik.getPemilik_nama());
            identitasPemilik2.setPemilik_npwp(identitasPemilik.getPemilik_npwp());

            return new ResponseEntity<>(identitasPemilikRepository.save(identitasPemilik2), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}