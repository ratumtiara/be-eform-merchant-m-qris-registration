package com.bmi.qris.controller;

import com.bmi.qris.entity.*;
import com.bmi.qris.repository.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/mqris/eform/form_identitas_usaha")
public class FormIdentitasUsahaController {

    @Autowired
    private IdentitasUsahaRepository identitasUsahaRepository;

    @PostMapping("/post")
    public ResponseEntity<IdentitasUsahaEntity> create(@RequestBody IdentitasUsahaEntity createIdentitasUsaha) {
        try {
            return new ResponseEntity<>(identitasUsahaRepository.save(createIdentitasUsaha), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/put/{appId}")
    public ResponseEntity<IdentitasUsahaEntity> update(@PathVariable("appId") Long appId,
            @RequestBody IdentitasUsahaEntity identitasUsaha) {
        Optional<IdentitasUsahaEntity> identitasUsahaData = identitasUsahaRepository.findById(appId);

        if (identitasUsahaData.isPresent()) {
            IdentitasUsahaEntity identitasUsaha2 = identitasUsahaData.get();

            identitasUsaha2.setUsaha_nama50(identitasUsaha.getUsaha_nama50());
            identitasUsaha2.setUsaha_nama25(identitasUsaha.getUsaha_nama25());
            identitasUsaha2.setUsaha_norekmuamalat(identitasUsaha.getUsaha_norekmuamalat());
            identitasUsaha2.setUsaha_namasesuainorek(identitasUsaha.getUsaha_namasesuainorek());
            identitasUsaha2.setUsaha_klasifikasi(identitasUsaha.getUsaha_klasifikasi());
            identitasUsaha2.setUsaha_mdr(identitasUsaha.getUsaha_mdr());
            identitasUsaha2.setUsaha_mcc(identitasUsaha.getUsaha_mcc());
            identitasUsaha2.setUsaha_terminal(identitasUsaha.getUsaha_terminal());
            identitasUsaha2.setUsaha_siup(identitasUsaha.getUsaha_siup());
            identitasUsaha2.setUsaha_tipeqr(identitasUsaha.getUsaha_tipeqr());
            identitasUsaha2.setUsaha_tipemerchant(identitasUsaha.getUsaha_tipemerchant());
            identitasUsaha2.setUsaha_kodereferal(identitasUsaha.getUsaha_kodereferal());
            identitasUsaha2.setUsaha_bankterdekat(identitasUsaha.getUsaha_bankterdekat());

            return new ResponseEntity<>(identitasUsahaRepository.save(identitasUsaha2), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}