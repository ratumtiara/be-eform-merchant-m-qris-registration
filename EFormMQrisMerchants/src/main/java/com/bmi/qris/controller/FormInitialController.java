package com.bmi.qris.controller;

import com.bmi.qris.entity.*;
import com.bmi.qris.repository.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/mqris/eform/form_initial")
public class FormInitialController {

    @Autowired
    private InitialRepository initialRepository;

    @PostMapping("/post")
    public ResponseEntity<InitialEntity> create(@RequestBody InitialEntity createInitial) {
        try {
            return new ResponseEntity<>(initialRepository.save(createInitial), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/put/{appId}")
    public @Valid InitialEntity update(@PathVariable(value = "appId") Long appId, @RequestBody InitialEntity initial) {
        System.out.println(initial);
        return initialRepository.save(initial);
    }

    @GetMapping("/get/getAppId/{appId}")
    public List<InitialEntity> getAppId(@PathVariable Long appId) {
        return initialRepository.getAppId(appId);
    }
}