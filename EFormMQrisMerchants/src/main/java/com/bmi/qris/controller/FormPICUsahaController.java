package com.bmi.qris.controller;

import com.bmi.qris.entity.*;
import com.bmi.qris.repository.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/mqris/eform/form_pic_usaha")
public class FormPICUsahaController {

    @Autowired
    private PICUsahaRepository picUsahaRepository;

    @PostMapping("/post")
    public ResponseEntity<PICUsahaEntity> create(@RequestBody PICUsahaEntity createPICUsaha) {
        try {
            return new ResponseEntity<>(picUsahaRepository.save(createPICUsaha), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/put/{appId}")
    public ResponseEntity<PICUsahaEntity> update(@PathVariable("appId") Long appId,
            @RequestBody PICUsahaEntity picUsaha) {
        Optional<PICUsahaEntity> picUsahaData = picUsahaRepository.findById(appId);

        if (picUsahaData.isPresent()) {
            PICUsahaEntity picUsaha2 = picUsahaData.get();

            picUsaha2.setPic_handphone(picUsaha.getPic_handphone());
            picUsaha2.setPic_nama(picUsaha.getPic_nama());
            picUsaha2.setPic_web(picUsaha.getPic_web());

            return new ResponseEntity<>(picUsahaRepository.save(picUsaha2), HttpStatus.OK);

        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}