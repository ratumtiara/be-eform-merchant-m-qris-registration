package com.bmi.qris.controller;

import com.bmi.qris.entity.ProfilPendapatanUsahaEntity;
import com.bmi.qris.repository.ProfilPendapatanUsahaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/mqris/eform/form_profil_pendapatan")
public class FormProfilPendapatanController {

    @Autowired
    private ProfilPendapatanUsahaRepository profilPendapatanUsahaRepository;

    @PostMapping("/post")
    public ResponseEntity<ProfilPendapatanUsahaEntity> create(
            @RequestBody ProfilPendapatanUsahaEntity newProfilPendapatanUsaha) {
        try {
            return new ResponseEntity<>(profilPendapatanUsahaRepository.save(newProfilPendapatanUsaha), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/put/{appId}")
    public ResponseEntity<ProfilPendapatanUsahaEntity> update(@PathVariable("appId") Long appId,
            @RequestBody ProfilPendapatanUsahaEntity profilPendapatanUsaha) {
        Optional<ProfilPendapatanUsahaEntity> profilPendapatanUsahaData = profilPendapatanUsahaRepository
                .findById(appId);

        if (profilPendapatanUsahaData.isPresent()) {
            ProfilPendapatanUsahaEntity profilPendapatanUsaha2 = profilPendapatanUsahaData.get();

            profilPendapatanUsaha2.setProfil_average(profilPendapatanUsaha.getProfil_average());
            profilPendapatanUsaha2.setProfil_volume(profilPendapatanUsaha.getProfil_volume());

            return new ResponseEntity<>(profilPendapatanUsahaRepository.save(profilPendapatanUsaha2), HttpStatus.OK);

        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}