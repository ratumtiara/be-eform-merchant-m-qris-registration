package com.bmi.qris.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.*;

import com.bmi.qris.entity.KCUEntity;
import com.bmi.qris.repository.KCURepository;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/daftar_kcu")
public class KCUController {
    @Autowired
    private KCURepository kcuRepository;

    @GetMapping("/getOne/{id_provinsi}")
    public List<KCUEntity> getOneId(@PathVariable String id_provinsi) {
        return kcuRepository.getOne(id_provinsi);
    }
}
