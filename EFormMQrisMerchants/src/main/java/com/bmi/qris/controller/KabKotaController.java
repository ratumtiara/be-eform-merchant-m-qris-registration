package com.bmi.qris.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.*;

import com.bmi.qris.entity.KabKotaEntity;
import com.bmi.qris.repository.KabKotaRepository;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/kotakab")
public class KabKotaController {

    @Autowired
    private KabKotaRepository kabKotaRepository;

    @GetMapping("/getOne/{kd_prov}")
    public List<KabKotaEntity> getOne(@PathVariable String kd_prov) {
        return kabKotaRepository.getKotaKab(kd_prov);
    }
}