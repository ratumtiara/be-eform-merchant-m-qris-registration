package com.bmi.qris.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.*;

import com.bmi.qris.entity.KecamatanEntity;
import com.bmi.qris.repository.KecamatanRepository;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/kecamatan")
public class KecamatanController {
    @Autowired
    private KecamatanRepository kecamatanRepository;

    @GetMapping("/getOne/{kd_kabkot}")
    public List<KecamatanEntity> getOne(@PathVariable String kd_kabkot) {
        return kecamatanRepository.getKecamatan(kd_kabkot);
    }
}