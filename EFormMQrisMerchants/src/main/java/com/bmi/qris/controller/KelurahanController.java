package com.bmi.qris.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.*;

import com.bmi.qris.entity.KelurahanEntity;
import com.bmi.qris.repository.KelurahanRepository;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/kelurahan")
public class KelurahanController {
    @Autowired
    private KelurahanRepository kelurahanRepository;

    @GetMapping("/getOne/{kd_kecamatan}")
    public List<KelurahanEntity> getOne(@PathVariable String kd_kecamatan) {
        return kelurahanRepository.getKelurahan(kd_kecamatan);
    }
}