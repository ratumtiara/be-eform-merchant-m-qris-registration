package com.bmi.qris.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

import com.bmi.qris.entity.KodePosEntity;
import com.bmi.qris.repository.KodePosRepository;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/kodepos")
public class KodePosController {
    @Autowired
    private KodePosRepository kodePosRepository;

    @GetMapping("/getOne/{province_code}/{sub_district}")
    public List<KodePosEntity> getOne(@PathVariable String province_code, @PathVariable String sub_district) {
        return kodePosRepository.getProvinceDistrict(province_code, sub_district);
    }
}