package com.bmi.qris.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.*;

import com.bmi.qris.entity.ProvinsiEntity;
import com.bmi.qris.repository.ProvinsiRepository;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/provinsi")
public class ProvinsiController {
    @Autowired
    private ProvinsiRepository provinsiRepository;

    @GetMapping("/getAll")
    public List<ProvinsiEntity> getOneId() {
        return provinsiRepository.findAll();
    }
}