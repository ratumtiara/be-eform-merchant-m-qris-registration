package com.bmi.qris.controller;

import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bmi.qris.repository.UserRepository;
import com.bmi.qris.entity.UserEntity;

@RestController
@CrossOrigin()
@RequestMapping("/mqris/eform/user")
public class UserController {

    @Autowired
    private UserRepository userAdminRepository;

    @GetMapping("/changepassword/{username}/{password}")
    public String changepassword(@PathVariable String username, @PathVariable String password) {
        String passHash = DigestUtils.sha256Hex(password);
        userAdminRepository.changePassByUsername(username, passHash);
        String result = "Done Change, Username : " + username + " | Password : " + password;
        return result;
    }

    @GetMapping("/login/{username}")
    public List<UserEntity> findByUsernameUser(@PathVariable String username) {
        return userAdminRepository.findByUsernameUser(username);
    }
}