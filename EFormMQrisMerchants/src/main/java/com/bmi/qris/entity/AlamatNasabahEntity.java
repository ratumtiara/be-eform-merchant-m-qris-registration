package com.bmi.qris.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "form_alamat_nasabah")

public class AlamatNasabahEntity {
    @Id
    @Column(name = "appId")
    private Long appId;

    @Column(name = "pribadi_jalan")
    private String pribadi_jalan;

    @Column(name = "pribadi_provinsi")
    private String pribadi_provinsi;

    @Column(name = "pribadi_kotakab")
    private String pribadi_kotakab;

    @Column(name = "pribadi_kecamatan")
    private String pribadi_kecamatan;

    @Column(name = "pribadi_kelurahan")
    private String pribadi_kelurahan;

    @Column(name = "pribadi_kodepos")
    private Integer pribadi_kodepos;

    @Column(name = "alamat_jalan")
    private String alamat_jalan;

    @Column(name = "alamat_provinsi")
    private String alamat_provinsi;

    @Column(name = "alamat_kotakab")
    private String alamat_kotakab;

    @Column(name = "alamat_kecamatan")
    private String alamat_kecamatan;

    @Column(name = "alamat_kelurahan")
    private String alamat_kelurahan;

    @Column(name = "alamat_kodepos")
    private Integer alamat_kodepos;

    @Column(name = "alamat_kordinat")
    private String alamat_kordinat;

    public Long getAppId() {
        return this.appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public String getPribadi_jalan() {
        return this.pribadi_jalan;
    }

    public void setPribadi_jalan(String pribadi_jalan) {
        this.pribadi_jalan = pribadi_jalan;
    }

    public String getPribadi_provinsi() {
        return this.pribadi_provinsi;
    }

    public void setPribadi_provinsi(String pribadi_provinsi) {
        this.pribadi_provinsi = pribadi_provinsi;
    }

    public String getPribadi_kotakab() {
        return this.pribadi_kotakab;
    }

    public void setPribadi_kotakab(String pribadi_kotakab) {
        this.pribadi_kotakab = pribadi_kotakab;
    }

    public String getPribadi_kecamatan() {
        return this.pribadi_kecamatan;
    }

    public void setPribadi_kecamatan(String pribadi_kecamatan) {
        this.pribadi_kecamatan = pribadi_kecamatan;
    }

    public String getPribadi_kelurahan() {
        return this.pribadi_kelurahan;
    }

    public void setPribadi_kelurahan(String pribadi_kelurahan) {
        this.pribadi_kelurahan = pribadi_kelurahan;
    }

    public Integer getPribadi_kodepos() {
        return this.pribadi_kodepos;
    }

    public void setPribadi_kodepos(Integer pribadi_kodepos) {
        this.pribadi_kodepos = pribadi_kodepos;
    }

    public String getAlamat_jalan() {
        return this.alamat_jalan;
    }

    public void setAlamat_jalan(String alamat_jalan) {
        this.alamat_jalan = alamat_jalan;
    }

    public String getAlamat_provinsi() {
        return this.alamat_provinsi;
    }

    public void setAlamat_provinsi(String alamat_provinsi) {
        this.alamat_provinsi = alamat_provinsi;
    }

    public String getAlamat_kotakab() {
        return this.alamat_kotakab;
    }

    public void setAlamat_kotakab(String alamat_kotakab) {
        this.alamat_kotakab = alamat_kotakab;
    }

    public String getAlamat_kecamatan() {
        return this.alamat_kecamatan;
    }

    public void setAlamat_kecamatan(String alamat_kecamatan) {
        this.alamat_kecamatan = alamat_kecamatan;
    }

    public String getAlamat_kelurahan() {
        return this.alamat_kelurahan;
    }

    public void setAlamat_kelurahan(String alamat_kelurahan) {
        this.alamat_kelurahan = alamat_kelurahan;
    }

    public Integer getAlamat_kodepos() {
        return this.alamat_kodepos;
    }

    public void setAlamat_kodepos(Integer alamat_kodepos) {
        this.alamat_kodepos = alamat_kodepos;
    }

    public String getAlamat_kordinat() {
        return this.alamat_kordinat;
    }

    public void setAlamat_kordinat(String alamat_kordinat) {
        this.alamat_kordinat = alamat_kordinat;
    }

}