package com.bmi.qris.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity()
@Table(name = "form_app_master", uniqueConstraints = { @UniqueConstraint(columnNames = { "appId" }) })
public class AppMasterEntity {

    @Id
    private Long appId;

    @Column(name = "status_code")
    private Long status_code;

    @Column(name = "isExport")
    private Boolean isExport;

    @Column(name = "reason_reject")
    private String reason_reject;

    @Column(name = "created_date")
    private Date created_date;

    @Column(name = "modified_by")
    private String modified_by;

    @Column(name = "modified_date")
    private Date modified_date;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "appId", referencedColumnName = "appId")
    public InitialEntity initial;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "appId", referencedColumnName = "appId")
    private AlamatNasabahEntity alamatNasabah;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "appId", referencedColumnName = "appId")
    private IdentitasPemilikEntity identitasPemilik;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "appId", referencedColumnName = "appId")
    private IdentitasUsahaEntity identitasUsaha;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "appId", referencedColumnName = "appId")
    private ProfilPendapatanUsahaEntity profilPendapatanUsaha;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "appId", referencedColumnName = "appId")
    private PICUsahaEntity picUsaha;

    public Long getAppId() {
        return this.appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public Long getStatus_code() {
        return this.status_code;
    }

    public void setStatus_code(Long status_code) {
        this.status_code = status_code;
    }

    public Boolean isIsExport() {
        return this.isExport;
    }

    public Boolean getIsExport() {
        return this.isExport;
    }

    public void setIsExport(Boolean isExport) {
        this.isExport = isExport;
    }

    public String getReason_reject() {
        return this.reason_reject;
    }

    public void setReason_reject(String reason_reject) {
        this.reason_reject = reason_reject;
    }

    public Date getCreated_date() {
        return this.created_date;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    public String getModified_by() {
        return this.modified_by;
    }

    public void setModified_by(String modified_by) {
        this.modified_by = modified_by;
    }

    public Date getModified_date() {
        return this.modified_date;
    }

    public void setModified_date(Date modified_date) {
        this.modified_date = modified_date;
    }

    public InitialEntity getInitial() {
        return this.initial;
    }

    public void setInitial(InitialEntity initial) {
        this.initial = initial;
    }

    public AlamatNasabahEntity getAlamatNasabah() {
        return this.alamatNasabah;
    }

    public void setAlamatNasabah(AlamatNasabahEntity alamatNasabah) {
        this.alamatNasabah = alamatNasabah;
    }

    public IdentitasPemilikEntity getIdentitasPemilik() {
        return this.identitasPemilik;
    }

    public void setIdentitasPemilik(IdentitasPemilikEntity identitasPemilik) {
        this.identitasPemilik = identitasPemilik;
    }

    public IdentitasUsahaEntity getIdentitasUsaha() {
        return this.identitasUsaha;
    }

    public void setIdentitasUsaha(IdentitasUsahaEntity identitasUsaha) {
        this.identitasUsaha = identitasUsaha;
    }

    public ProfilPendapatanUsahaEntity getProfilPendapatanUsaha() {
        return this.profilPendapatanUsaha;
    }

    public void setProfilPendapatanUsaha(ProfilPendapatanUsahaEntity profilPendapatanUsaha) {
        this.profilPendapatanUsaha = profilPendapatanUsaha;
    }

    public PICUsahaEntity getPicUsaha() {
        return this.picUsaha;
    }

    public void setPicUsaha(PICUsahaEntity picUsaha) {
        this.picUsaha = picUsaha;
    }
}