package com.bmi.qris.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "form_identitas_pemilik")
public class IdentitasPemilikEntity {

    @Id
    @Column(name = "appId")
    private Long appId;

    @Column(name = "pemilik_ktp")
    private Long pemilik_ktp;

    @Column(name = "pemilik_nama")
    private String pemilik_nama;

    @Column(name = "pemilik_handpone")
    private String pemilik_handpone;

    @Column(name = "pemilik_npwp")
    private Long pemilik_npwp;

    @Column(name = "pemilik_email")
    private String pemilik_email;

    public Long getAppId() {
        return this.appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public Long getPemilik_ktp() {
        return this.pemilik_ktp;
    }

    public void setPemilik_ktp(Long pemilik_ktp) {
        this.pemilik_ktp = pemilik_ktp;
    }

    public String getPemilik_nama() {
        return this.pemilik_nama;
    }

    public void setPemilik_nama(String pemilik_nama) {
        this.pemilik_nama = pemilik_nama;
    }

    public String getPemilik_handpone() {
        return this.pemilik_handpone;
    }

    public void setPemilik_handpone(String pemilik_handpone) {
        this.pemilik_handpone = pemilik_handpone;
    }

    public Long getPemilik_npwp() {
        return this.pemilik_npwp;
    }

    public void setPemilik_npwp(Long pemilik_npwp) {
        this.pemilik_npwp = pemilik_npwp;
    }

    public String getPemilik_email() {
        return this.pemilik_email;
    }

    public void setPemilik_email(String pemilik_email) {
        this.pemilik_email = pemilik_email;
    }

}