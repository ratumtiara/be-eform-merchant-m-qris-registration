package com.bmi.qris.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "form_identitas_usaha")
public class IdentitasUsahaEntity {

    @Id
    @Column(name = "appId")
    private Long appId;

    @Column(name = "usaha_nama50")
    private String usaha_nama50;

    @Column(name = "usaha_nama25")
    private String usaha_nama25;

    @Column(name = "usaha_tipemerchant")
    private String usaha_tipemerchant;

    @Column(name = "usaha_klasifikasi")
    private String usaha_klasifikasi;

    @Column(name = "usaha_terminal")
    private Long usaha_terminal;

    @Column(name = "usaha_mdr")
    private String usaha_mdr;

    @Column(name = "usaha_siup")
    private Long usaha_siup;

    @Column(name = "usaha_mcc")
    private String usaha_mcc;

    @Column(name = "usaha_tipeqr")
    private String usaha_tipeqr;

    @Column(name = "usaha_bankterdekat")
    private String usaha_bankterdekat;

    @Column(name = "usaha_kodereferal")
    private String usaha_kodereferal;

    @Column(name = "usaha_norekmuamalat")
    private Long usaha_norekmuamalat;

    @Column(name = "usaha_namasesuainorek")
    private String usaha_namasesuainorek;

    public Long getAppId() {
        return this.appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public String getUsaha_nama50() {
        return this.usaha_nama50;
    }

    public void setUsaha_nama50(String usaha_nama50) {
        this.usaha_nama50 = usaha_nama50;
    }

    public String getUsaha_nama25() {
        return this.usaha_nama25;
    }

    public void setUsaha_nama25(String usaha_nama25) {
        this.usaha_nama25 = usaha_nama25;
    }

    public String getUsaha_tipemerchant() {
        return this.usaha_tipemerchant;
    }

    public void setUsaha_tipemerchant(String usaha_tipemerchant) {
        this.usaha_tipemerchant = usaha_tipemerchant;
    }

    public String getUsaha_klasifikasi() {
        return this.usaha_klasifikasi;
    }

    public void setUsaha_klasifikasi(String usaha_klasifikasi) {
        this.usaha_klasifikasi = usaha_klasifikasi;
    }

    public Long getUsaha_terminal() {
        return this.usaha_terminal;
    }

    public void setUsaha_terminal(Long usaha_terminal) {
        this.usaha_terminal = usaha_terminal;
    }

    public String getUsaha_mdr() {
        return this.usaha_mdr;
    }

    public void setUsaha_mdr(String usaha_mdr) {
        this.usaha_mdr = usaha_mdr;
    }

    public Long getUsaha_siup() {
        return this.usaha_siup;
    }

    public void setUsaha_siup(Long usaha_siup) {
        this.usaha_siup = usaha_siup;
    }

    public String getUsaha_mcc() {
        return this.usaha_mcc;
    }

    public void setUsaha_mcc(String usaha_mcc) {
        this.usaha_mcc = usaha_mcc;
    }

    public String getUsaha_tipeqr() {
        return this.usaha_tipeqr;
    }

    public void setUsaha_tipeqr(String usaha_tipeqr) {
        this.usaha_tipeqr = usaha_tipeqr;
    }

    public String getUsaha_bankterdekat() {
        return this.usaha_bankterdekat;
    }

    public void setUsaha_bankterdekat(String usaha_bankterdekat) {
        this.usaha_bankterdekat = usaha_bankterdekat;
    }

    public String getUsaha_kodereferal() {
        return this.usaha_kodereferal;
    }

    public void setUsaha_kodereferal(String usaha_kodereferal) {
        this.usaha_kodereferal = usaha_kodereferal;
    }

    public Long getUsaha_norekmuamalat() {
        return this.usaha_norekmuamalat;
    }

    public void setUsaha_norekmuamalat(Long usaha_norekmuamalat) {
        this.usaha_norekmuamalat = usaha_norekmuamalat;
    }

    public String getUsaha_namasesuainorek() {
        return this.usaha_namasesuainorek;
    }

    public void setUsaha_namasesuainorek(String usaha_namasesuainorek) {
        this.usaha_namasesuainorek = usaha_namasesuainorek;
    }
}