package com.bmi.qris.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "form_initial")
public class InitialEntity {

    @Id
    @Column(name = "appId")
    private Long appId;

    @Column(name = "initial_mqris")
    private String initial_mqris;

    @Column(name = "initial_nmid")
    private String initial_nmid;

    @Column(name = "initial_mpan")
    private String initial_mpan;

    @Column(name = "initial_mid")
    private String initial_mid;

    public Long getAppId() {
        return this.appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public String getInitial_mqris() {
        return this.initial_mqris;
    }

    public void setInitial_mqris(String initial_mqris) {
        this.initial_mqris = initial_mqris;
    }

    public String getInitial_nmid() {
        return this.initial_nmid;
    }

    public void setInitial_nmid(String initial_nmid) {
        this.initial_nmid = initial_nmid;
    }

    public String getInitial_mpan() {
        return this.initial_mpan;
    }

    public void setInitial_mpan(String initial_mpan) {
        this.initial_mpan = initial_mpan;
    }

    public String getInitial_mid() {
        return this.initial_mid;
    }

    public void setInitial_mid(String initial_mid) {
        this.initial_mid = initial_mid;
    }
}