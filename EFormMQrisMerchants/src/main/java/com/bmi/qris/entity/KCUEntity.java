package com.bmi.qris.entity;

import javax.persistence.*;

@Entity
@Table(name = "form_kcu")
public class KCUEntity {
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "id_provinsi")
    private String id_provinsi;

    @Column(name = "cabang")
    private String cabang;

    @Column(name = "provinsi")
    private String provinsi;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_provinsi() {
        return this.id_provinsi;
    }

    public void setId_provinsi(String id_provinsi) {
        this.id_provinsi = id_provinsi;
    }

    public String getCabang() {
        return this.cabang;
    }

    public void setCabang(String cabang) {
        this.cabang = cabang;
    }

    public String getProvinsi() {
        return this.provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }
}