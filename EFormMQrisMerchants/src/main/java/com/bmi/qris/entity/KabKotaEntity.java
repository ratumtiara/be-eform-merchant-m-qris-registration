package com.bmi.qris.entity;

import javax.persistence.*;

@Entity
@Table(name = "data_kotakab")
public class KabKotaEntity {
    @Id
    @Column(name = "kd_kabkot")
    private String kd_kabkot;

    @Column(name = "kd_prov")
    private String kd_prov;

    @Column(name = "nm_kabkot")
    private String nm_kabkot;

    public String getKd_kabkot() {
        return this.kd_kabkot;
    }

    public void setKd_kabkot(String kd_kabkot) {
        this.kd_kabkot = kd_kabkot;
    }

    public String getKd_prov() {
        return this.kd_prov;
    }

    public void setKd_prov(String kd_prov) {
        this.kd_prov = kd_prov;
    }

    public String getNm_kabkot() {
        return this.nm_kabkot;
    }

    public void setNm_kabkot(String nm_kabkot) {
        this.nm_kabkot = nm_kabkot;
    }
}