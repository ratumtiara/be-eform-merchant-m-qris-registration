package com.bmi.qris.entity;

import javax.persistence.*;

@Entity
@Table(name = "data_kecamatan")
public class KecamatanEntity {
    @Id
    @Column(name = "kd_kecamatan")
    private String kd_kecamatan;

    @Column(name = "kd_kabkot")
    private String kd_kabkot;

    @Column(name = "kd_prov")
    private String kd_prov;

    @Column(name = "nm_kecamatan")
    private String nm_kecamatan;

    public String getKd_kecamatan() {
        return this.kd_kecamatan;
    }

    public void setKd_kecamatan(String kd_kecamatan) {
        this.kd_kecamatan = kd_kecamatan;
    }

    public String getKd_kabkot() {
        return this.kd_kabkot;
    }

    public void setKd_kabkot(String kd_kabkot) {
        this.kd_kabkot = kd_kabkot;
    }

    public String getKd_prov() {
        return this.kd_prov;
    }

    public void setKd_prov(String kd_prov) {
        this.kd_prov = kd_prov;
    }

    public String getNm_kecamatan() {
        return this.nm_kecamatan;
    }

    public void setNm_kecamatan(String nm_kecamatan) {
        this.nm_kecamatan = nm_kecamatan;
    }

}
