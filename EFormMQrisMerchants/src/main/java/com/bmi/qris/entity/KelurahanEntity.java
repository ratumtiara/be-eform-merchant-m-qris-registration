package com.bmi.qris.entity;

import javax.persistence.*;

@Entity
@Table(name = "data_kelurahan")
public class KelurahanEntity {
    @Id
    @Column(name = "kd_kelurahan")
    private String kd_kelurahan;

    @Column(name = "kd_kecamatan")
    private String kd_kecamatan;

    @Column(name = "kd_kabkot")
    private String kd_kabkot;

    @Column(name = "kd_prov")
    private String kd_prov;

    @Column(name = "nm_kelurahan")
    private String nm_kelurahan;

    public String getKd_kelurahan() {
        return this.kd_kelurahan;
    }

    public void setKd_kelurahan(String kd_kelurahan) {
        this.kd_kelurahan = kd_kelurahan;
    }

    public String getKd_kecamatan() {
        return this.kd_kecamatan;
    }

    public void setKd_kecamatan(String kd_kecamatan) {
        this.kd_kecamatan = kd_kecamatan;
    }

    public String getKd_kabkot() {
        return this.kd_kabkot;
    }

    public void setKd_kabkot(String kd_kabkot) {
        this.kd_kabkot = kd_kabkot;
    }

    public String getKd_prov() {
        return this.kd_prov;
    }

    public void setKd_prov(String kd_prov) {
        this.kd_prov = kd_prov;
    }

    public String getNm_kelurahan() {
        return this.nm_kelurahan;
    }

    public void setNm_kelurahan(String nm_kelurahan) {
        this.nm_kelurahan = nm_kelurahan;
    }
}