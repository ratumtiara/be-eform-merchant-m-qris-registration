package com.bmi.qris.entity;

import javax.persistence.*;

@Entity
@Table(name = "data_kodepos")
public class KodePosEntity {
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "urban")
    private String urban;

    @Column(name = "sub_district")
    private String sub_district;

    @Column(name = "city")
    private String city;

    @Column(name = "province_code")
    private String province_code;

    @Column(name = "postal_code")
    private String postal_code;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrban() {
        return this.urban;
    }

    public void setUrban(String urban) {
        this.urban = urban;
    }

    public String getSub_district() {
        return this.sub_district;
    }

    public void setSub_district(String sub_district) {
        this.sub_district = sub_district;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince_code() {
        return this.province_code;
    }

    public void setProvince_code(String province_code) {
        this.province_code = province_code;
    }

    public String getPostal_code() {
        return this.postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }
}