package com.bmi.qris.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "form_pic_usaha")
public class PICUsahaEntity {

    @Id
    @Column(name = "appId")
    private Long appId;

    @Column(name = "pic_nama")
    private String pic_nama;

    @Column(name = "pic_handphone")
    private String pic_handphone;

    @Column(name = "pic_web")
    private String pic_web;

    public Long getAppId() {
        return this.appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public String getPic_nama() {
        return this.pic_nama;
    }

    public void setPic_nama(String pic_nama) {
        this.pic_nama = pic_nama;
    }

    public String getPic_handphone() {
        return this.pic_handphone;
    }

    public void setPic_handphone(String pic_handphone) {
        this.pic_handphone = pic_handphone;
    }

    public String getPic_web() {
        return this.pic_web;
    }

    public void setPic_web(String pic_web) {
        this.pic_web = pic_web;
    }
}