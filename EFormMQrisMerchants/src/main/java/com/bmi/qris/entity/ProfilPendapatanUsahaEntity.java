package com.bmi.qris.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "form_profil_pendapatan_usaha")
public class ProfilPendapatanUsahaEntity {

    @Id
    @Column(name = "appId")
    private Long appId;

    @Column(name = "profil_average")
    private Long profil_average;

    @Column(name = "profil_volume")
    private Long profil_volume;

    public Long getAppId() {
        return this.appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public Long getProfil_average() {
        return this.profil_average;
    }

    public void setProfil_average(Long profil_average) {
        this.profil_average = profil_average;
    }

    public Long getProfil_volume() {
        return this.profil_volume;
    }

    public void setProfil_volume(Long profil_volume) {
        this.profil_volume = profil_volume;
    }
}