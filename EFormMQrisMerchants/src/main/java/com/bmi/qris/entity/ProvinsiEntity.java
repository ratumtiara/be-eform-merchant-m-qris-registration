package com.bmi.qris.entity;

import javax.persistence.*;

@Entity
@Table(name = "data_provinsi")
public class ProvinsiEntity {
    @Id
    @Column(name = "kd_prov")
    private String kd_prov;

    @Column(name = "nm_prov")
    private String nm_prov;

    public String getKd_prov() {
        return this.kd_prov;
    }

    public void setKd_prov(String kd_prov) {
        this.kd_prov = kd_prov;
    }

    public String getNm_prov() {
        return this.nm_prov;
    }

    public void setNm_prov(String nm_prov) {
        this.nm_prov = nm_prov;
    }
}