package com.bmi.qris.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "form_user_admin")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "user_departemen")
    private String user_departemen;

    @Column(name = "user_email")
    private String user_email;

    @Column(name = "user_fullname")
    private String user_fullname;

    @Column(name = "user_jabatan")
    private String user_jabatan;

    @Column(name = "user_password")
    private String user_password;

    @Column(name = "username")
    private String username;

    @Column(name = "user_grup")
    private String user_grup;

    @Column(name = "user_branch")
    private String user_branch;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUser_departemen() {
        return this.user_departemen;
    }

    public void setUser_departemen(String user_departemen) {
        this.user_departemen = user_departemen;
    }

    public String getUser_email() {
        return this.user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_fullname() {
        return this.user_fullname;
    }

    public void setUser_fullname(String user_fullname) {
        this.user_fullname = user_fullname;
    }

    public String getUser_jabatan() {
        return this.user_jabatan;
    }

    public void setUser_jabatan(String user_jabatan) {
        this.user_jabatan = user_jabatan;
    }

    public String getUser_password() {
        return this.user_password;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUser_grup() {
        return this.user_grup;
    }

    public void setUser_grup(String user_grup) {
        this.user_grup = user_grup;
    }

    public String getUser_branch() {
        return this.user_branch;
    }

    public void setUser_branch(String user_branch) {
        this.user_branch = user_branch;
    }
}