package com.bmi.qris.repository;

import com.bmi.qris.entity.AlamatNasabahEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlamatNasabahRepository extends JpaRepository<AlamatNasabahEntity, Long> {
}