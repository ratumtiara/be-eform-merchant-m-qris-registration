package com.bmi.qris.repository;

import com.bmi.qris.entity.AppMasterEntity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AppMasterRepository extends JpaRepository<AppMasterEntity, Long> {

    @Query("select u from AppMasterEntity u where u.appId = ?1")
    List<AppMasterEntity> getAppId(Long appId);

    @Query("select u from AppMasterEntity u where u.status_code = ?1")
    List<AppMasterEntity> getStatusCode(Long status_code);

    @Query(value = "SELECT DISTINCT a.app_id,usaha_nama50,usaha_tipemerchant,usaha_klasifikasi,usaha_mdr,usaha_bankterdekat,status_code,reason_reject,usaha_kodereferal FROM form_app_master a, form_identitas_usaha b WHERE a.app_id = b.app_id order by a.status_code ASC", nativeQuery = true)
    List<?> getAll();
    
    @Query(value = "SELECT DISTINCT a.app_id,usaha_nama50,usaha_tipemerchant,usaha_klasifikasi,usaha_mdr,usaha_bankterdekat,status_code,reason_reject,usaha_kodereferal FROM form_app_master a, form_identitas_usaha b WHERE a.app_id = b.app_id order by status_code desc, usaha_kodereferal desc", nativeQuery = true)
    List<?> getInquiryKPO();

    @Query(value = "SELECT * FROM form_app_master", nativeQuery = true)
    List<AppMasterEntity> getAllJPA();

    @Query(value = "SELECT DISTINCT a.app_id,usaha_nama50,usaha_tipemerchant,usaha_klasifikasi,usaha_mdr,usaha_bankterdekat,status_code,reason_reject,usaha_kodereferal FROM form_app_master a, form_identitas_usaha b WHERE b.usaha_kodereferal = ?1 and a.app_id = b.app_id order by a.status_code ASC", nativeQuery = true)
    List<?> getRef(String usaha_kodereferal);

    @Query(value = "SELECT usaha_norekmuamalat FROM form_identitas_usaha fiu WHERE usaha_norekmuamalat = ?1", nativeQuery = true)
    List<?> getNoRekening(Long usaha_norekmuamalat);

    @Query(value = "SELECT DISTINCT fam.app_id, fiu.usaha_nama25, fiu.usaha_kodereferal, fi.initial_mid, fi.initial_mpan, fi.initial_nmid FROM form_app_master fam inner join form_identitas_usaha fiu on fam.app_id = fiu.app_id inner join form_initial fi on fiu.app_id = fi.app_id where fi.initial_nmid is null", nativeQuery = true)
    List<?> getUpdateNMID();

    @Query(value = "SELECT DISTINCT a.app_id,usaha_nama50,usaha_tipemerchant,usaha_klasifikasi,usaha_mdr,usaha_bankterdekat,status_code,reason_reject,usaha_kodereferal FROM form_app_master a INNER JOIN form_identitas_usaha b ON a.app_id=b.app_id WHERE lower(b.usaha_nama50) LIKE CONCAT('%', ?1, '%') or lower(b.usaha_tipemerchant) LIKE CONCAT('%', ?1, '%') or lower(b.usaha_klasifikasi) LIKE CONCAT('%', ?1, '%') or lower(b.usaha_bankterdekat) LIKE CONCAT('%', ?1, '%') or lower(b.usaha_kodereferal) LIKE CONCAT('%', ?1, '%')", nativeQuery = true)
    List<?> getInquiryKPOFilter(String value);

    // ------------------------------------------------
    // Get Inquiry Branch BM
    @Query(value = "select distinct fam.app_id ,usaha_nama50 ,usaha_tipemerchant ,usaha_klasifikasi ,usaha_mdr ,usaha_bankterdekat ,usaha_kodereferal ,status_code ,reason_reject from form_app_master fam inner join form_identitas_usaha fiu  on fam.app_id = fiu.app_id  where usaha_bankterdekat = ?1 order by fam.status_code desc, fiu.usaha_kodereferal desc", nativeQuery = true)
    List<?> getInquiryBranchBM(String usaha_bankterdekat);

    // Get Inquiry Branch CS RM
    @Query(value = "select distinct fam.app_id ,usaha_nama50 ,usaha_tipemerchant ,usaha_klasifikasi ,usaha_mdr ,usaha_bankterdekat ,usaha_kodereferal ,status_code ,reason_reject from form_app_master fam inner join form_identitas_usaha fiu  on fam.app_id = fiu.app_id  where usaha_bankterdekat = ?1 and usaha_kodereferal = ?2 order by fam.status_code desc, fiu.usaha_kodereferal desc", nativeQuery = true)
    List<?> getInquiryBranchCSRM(String usaha_bankterdekat, String usaha_kodereferal);

    // Get Inquiry Branch Filter
    @Query(value = "SELECT DISTINCT a.app_id,usaha_nama50,usaha_tipemerchant,usaha_klasifikasi,usaha_mdr,usaha_bankterdekat,status_code,reason_reject,usaha_kodereferal FROM form_app_master a INNER JOIN form_identitas_usaha b ON a.app_id=b.app_id and b.usaha_bankterdekat = ?1 WHERE lower(b.usaha_nama50) LIKE CONCAT('%', ?2, '%') or lower(b.usaha_tipemerchant) LIKE CONCAT('%', ?2, '%') or lower(b.usaha_klasifikasi) LIKE CONCAT('%', ?2, '%') or lower(b.usaha_bankterdekat) LIKE CONCAT('%', ?2, '%') or lower(b.usaha_kodereferal) LIKE CONCAT('%', ?2, '%')", nativeQuery = true)
    List<?> getInquiryBranchFilter(String usaha_bankterdekat, String value);

    // Get Data Merchant Check
    @Query(value = "select distinct fam.app_id ,usaha_nama50 ,usaha_tipemerchant ,usaha_klasifikasi ,usaha_mdr ,usaha_bankterdekat ,usaha_kodereferal ,status_code ,reason_reject from form_app_master fam inner join form_identitas_usaha fiu on fam.app_id = fiu.app_id and status_code = '1' where usaha_bankterdekat = ?1 and usaha_kodereferal = ?2", nativeQuery = true)
    List<?> getDataMerchantCheck(String usaha_bankterdekat, String usaha_kodereferal);

    // Get Data Merchant Approve
    @Query(value = "select distinct fam.app_id ,usaha_nama50 ,usaha_tipemerchant ,usaha_klasifikasi ,usaha_mdr ,usaha_bankterdekat ,usaha_kodereferal ,status_code ,reason_reject from form_app_master fam inner join form_identitas_usaha fiu on fam.app_id = fiu.app_id and usaha_bankterdekat = ?1 where status_code = '2' order by usaha_kodereferal desc", nativeQuery = true)
    List<?> getDataMerchantApprove(String usaha_bankterdekat);

    // Get Data Merchant Check BM
    @Query(value = "select distinct fam.app_id ,usaha_nama50 ,usaha_tipemerchant ,usaha_klasifikasi ,usaha_mdr ,usaha_bankterdekat ,usaha_kodereferal ,status_code ,reason_reject from form_app_master fam inner join form_identitas_usaha fiu on fam.app_id = fiu.app_id and usaha_bankterdekat = ?1 where status_code = '1' order by usaha_kodereferal desc", nativeQuery = true)
    List<?> getDataMerchantsCheckBM(String usaha_bankterdekat);

    // Get Data Merchant Filter
    @Query(value = "SELECT DISTINCT a.app_id,usaha_nama50,usaha_tipemerchant,usaha_klasifikasi,usaha_mdr,usaha_bankterdekat,status_code,reason_reject,usaha_kodereferal FROM form_app_master a INNER JOIN form_identitas_usaha b ON a.app_id=b.app_id and a.status_code = ?3 WHERE b.usaha_bankterdekat = ?1 and b.usaha_kodereferal LIKE CONCAT('%', ?2, '%') or b.usaha_nama50 LIKE CONCAT('%', ?2, '%') or b.usaha_tipemerchant LIKE CONCAT('%', ?2, '%')", nativeQuery = true)
    List<?> getDataMerchantFilter(String usaha_bankterdekat, String value, Long status_code);

}