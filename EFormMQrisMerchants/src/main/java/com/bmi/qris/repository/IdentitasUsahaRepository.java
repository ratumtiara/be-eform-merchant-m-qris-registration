package com.bmi.qris.repository;

import com.bmi.qris.entity.*;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IdentitasUsahaRepository extends JpaRepository<IdentitasUsahaEntity, Long> {

}