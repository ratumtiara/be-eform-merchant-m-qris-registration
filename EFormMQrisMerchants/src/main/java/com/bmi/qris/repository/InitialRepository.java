package com.bmi.qris.repository;

import java.util.List;

import com.bmi.qris.entity.*;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface InitialRepository extends JpaRepository<InitialEntity, Long> {

    @Query(value = "select * from form_initial fi where app_id = ?1", nativeQuery = true)
    List<InitialEntity> getAppId(Long appId);
}