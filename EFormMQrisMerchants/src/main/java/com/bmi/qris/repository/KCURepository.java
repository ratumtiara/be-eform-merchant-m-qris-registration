package com.bmi.qris.repository;

import java.util.List;

import com.bmi.qris.entity.KCUEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface KCURepository extends JpaRepository<KCUEntity, Long> {
    @Query(value = "select * from form_kcu where id_provinsi = ?1", nativeQuery = true)
    List<KCUEntity> getOne(String id_provinsi);
}