package com.bmi.qris.repository;

import java.util.List;

import com.bmi.qris.entity.KabKotaEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface KabKotaRepository extends JpaRepository<KabKotaEntity, Long> {
    @Query("select u from KabKotaEntity u where u.kd_prov = ?1")
    List<KabKotaEntity> getKotaKab(String kd_prov);
}
