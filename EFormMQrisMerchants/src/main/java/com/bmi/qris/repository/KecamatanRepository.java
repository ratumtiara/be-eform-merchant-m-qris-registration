package com.bmi.qris.repository;

import java.util.List;

import com.bmi.qris.entity.KecamatanEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface KecamatanRepository extends JpaRepository<KecamatanEntity, Long> {
    @Query("select u from KecamatanEntity u where u.kd_kabkot = ?1")
    List<KecamatanEntity> getKecamatan(String kd_kabkot);
}