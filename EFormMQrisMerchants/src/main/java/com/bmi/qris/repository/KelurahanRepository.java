package com.bmi.qris.repository;

import java.util.List;

import com.bmi.qris.entity.KelurahanEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface KelurahanRepository extends JpaRepository<KelurahanEntity, Long> {
    @Query("select u from KelurahanEntity u where u.kd_kecamatan = ?1")
    List<KelurahanEntity> getKelurahan(String kd_kecamatan);
}