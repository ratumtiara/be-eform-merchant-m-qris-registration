package com.bmi.qris.repository;

import java.util.List;

import com.bmi.qris.entity.KodePosEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface KodePosRepository extends JpaRepository<KodePosEntity, Long> {
    @Query(value = "select * from data_kodepos where province_code = ?1 and sub_district = ?2", nativeQuery = true)
    List<KodePosEntity> getProvinceDistrict(String province_code, String sub_district);
}