package com.bmi.qris.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bmi.qris.entity.ProfilPendapatanUsahaEntity;

@Repository
public interface ProfilPendapatanUsahaRepository extends JpaRepository<ProfilPendapatanUsahaEntity, Long> {

}