package com.bmi.qris.repository;

import java.util.List;

import com.bmi.qris.entity.ProvinsiEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProvinsiRepository extends JpaRepository<ProvinsiEntity, Long> {
    @Query("select u from ProvinsiEntity u where u.kd_prov = ?1")
    List<ProvinsiEntity> getProvinsi(String kd_prov);
}
