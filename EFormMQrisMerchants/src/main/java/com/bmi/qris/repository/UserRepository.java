package com.bmi.qris.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import com.bmi.qris.entity.UserEntity;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    @Query("select u from UserEntity u where u.username = ?1")
    List<UserEntity> findByUsernameUser(String username);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "update form_user_admin set user_password = ?2 where username = ?1", nativeQuery = true)
    void changePassByUsername(@Param("username") String username, @Param("password") String user_password);
}