package com.bmi.QrisDoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QrisDocApplication {

	public static void main(String[] args) {
		SpringApplication.run(QrisDocApplication.class, args);
	}

}
