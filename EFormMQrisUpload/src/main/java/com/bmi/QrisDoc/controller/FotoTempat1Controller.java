package com.bmi.QrisDoc.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.MediaType;

import com.bmi.QrisDoc.service.FotoTempat1Services;
import com.bmi.QrisDoc.message.FotoTempat1FileResponse;
import com.bmi.QrisDoc.message.FotoTempat1MessageResponse;
import com.bmi.QrisDoc.entity.FotoTempat1;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/mqris/eformdoc")
public class FotoTempat1Controller {

    @Autowired
    private FotoTempat1Services fotoTempat1Services;

    @GetMapping("/upload_foto_1/get/")
    public ResponseEntity<List<FotoTempat1FileResponse>> getListFiles() {
        List<FotoTempat1FileResponse> files = fotoTempat1Services.getAllFiles().map(dbFile -> {
            String fileDownloadUri = ServletUriComponentsBuilder
                    .fromCurrentContextPath()
                    .path("/filesFoto1/")
                    .path(dbFile.getAppId())
                    .toUriString();
            return new FotoTempat1FileResponse(
                    dbFile.getAppId(),
                    fileDownloadUri,
                    dbFile.getType(),
                    dbFile.getData().length);
        }).collect(Collectors.toList());
        return ResponseEntity.status(HttpStatus.OK).body(files);
    }

    @GetMapping("/upload_foto_1/get/{appId}")
    public ResponseEntity<byte[]> downLoadSingleFiles(@PathVariable String appId) {
        FotoTempat1 fotoTempat1 = fotoTempat1Services.getFile(appId);
        String mimeType = fotoTempat1.getType();

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(mimeType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fotoTempat1.getAppId() + ".jpg")
                .body(fotoTempat1.getData());
    }

    @PostMapping("/upload_foto_1/post/")
    public ResponseEntity<FotoTempat1MessageResponse> uploadFile(@RequestParam String appId,
            @RequestParam MultipartFile filetiga) {
        String message = "";
        try {
            fotoTempat1Services.store(appId, filetiga);
            message = "Upload file success: " + filetiga.getOriginalFilename();
            return ResponseEntity.status(HttpStatus.OK).body(new FotoTempat1MessageResponse(message));
        } catch (Exception e) {
            message = "Could not upload the file: " + filetiga.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new FotoTempat1MessageResponse(message));
        }
    }

    @PutMapping("/update_foto_1/put/")
    public ResponseEntity<FotoTempat1MessageResponse> updateFile(@RequestParam String appId,
            @RequestParam MultipartFile filetiga) {
        String message = "";
        try {
            fotoTempat1Services.store(appId, filetiga);
            message = "Update file success: " + filetiga.getOriginalFilename();
            return ResponseEntity.status(HttpStatus.OK).body(new FotoTempat1MessageResponse(message));
        } catch (Exception e) {
            message = "Could not update the file: " + filetiga.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new FotoTempat1MessageResponse(message));
        }
    }

}