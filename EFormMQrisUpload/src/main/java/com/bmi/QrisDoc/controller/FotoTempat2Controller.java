package com.bmi.QrisDoc.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.MediaType;

import com.bmi.QrisDoc.service.FotoTempat2Services;
import com.bmi.QrisDoc.message.FotoTempat2FileResponse;
import com.bmi.QrisDoc.message.FotoTempat2MessageResponse;
import com.bmi.QrisDoc.entity.FotoTempat2;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/mqris/eformdoc")
public class FotoTempat2Controller {

    @Autowired
    private FotoTempat2Services fotoTempat2Services;

    @GetMapping("/upload_foto_2/get/")
    public ResponseEntity<List<FotoTempat2FileResponse>> getListFiles() {
        List<FotoTempat2FileResponse> files = fotoTempat2Services.getAllFiles().map(dbFile -> {
            String fileDownloadUri = ServletUriComponentsBuilder
                    .fromCurrentContextPath()
                    .path("/filesFoto2/")
                    .path(dbFile.getAppId())
                    .toUriString();
            return new FotoTempat2FileResponse(
                    dbFile.getAppId(),
                    fileDownloadUri,
                    dbFile.getType(),
                    dbFile.getData().length);
        }).collect(Collectors.toList());
        return ResponseEntity.status(HttpStatus.OK).body(files);
    }

    @GetMapping("/upload_foto_2/get/{appId}")
    public ResponseEntity<byte[]> downLoadSingleFiles(@PathVariable String appId) {
        FotoTempat2 fotoTempat2 = fotoTempat2Services.getFile(appId);
        String mimeType = fotoTempat2.getType();

        return ResponseEntity.ok()
        .contentType(MediaType.parseMediaType(mimeType))
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fotoTempat2.getAppId() + ".jpg")
        .body(fotoTempat2.getData());
    }

    @PostMapping("/upload_foto_2/post/")
    public ResponseEntity<FotoTempat2MessageResponse> uploadFile(@RequestParam String appId,
    @RequestParam MultipartFile fileempat) {
        String message = "";
        try {
            fotoTempat2Services.store(appId, fileempat);
            message = "Upload file success: " + fileempat.getOriginalFilename();
            return ResponseEntity.status(HttpStatus.OK).body(new FotoTempat2MessageResponse(message));
        } catch (Exception e) {
            message = "Could not upload the file: " + fileempat.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new FotoTempat2MessageResponse(message));
        }
    }

    @PutMapping("/upload_foto_2/put/")
    public ResponseEntity<FotoTempat2MessageResponse> updateFile(@RequestParam String appId,
    @RequestParam MultipartFile fileempat) {
        String message = "";
        try {
            fotoTempat2Services.store(appId, fileempat);

            message = "Update file success: " + fileempat.getOriginalFilename();
            return ResponseEntity.status(HttpStatus.OK).body(new FotoTempat2MessageResponse(message));
        } catch (Exception e) {
            message = "Could not updadte the file: " + fileempat.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new FotoTempat2MessageResponse(message));
        }
    }

    

}