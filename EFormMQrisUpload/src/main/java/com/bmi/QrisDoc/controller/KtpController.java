package com.bmi.QrisDoc.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.MediaType;

import com.bmi.QrisDoc.service.KtpServices;
import com.bmi.QrisDoc.message.KtpFileResponse;
import com.bmi.QrisDoc.message.KtpMessageResponse;
import com.bmi.QrisDoc.entity.KtpUpload;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/mqris/eformdoc")
public class KtpController {

  @Autowired
  private KtpServices ktpService;

  @PostMapping("/upload_ktp/post/")
  public ResponseEntity<KtpMessageResponse> uploadFile(@RequestParam String appId,
      @RequestParam MultipartFile filesatu) {
    String message = "";
    try {
      ktpService.store(appId, filesatu);
      message = "Upload file success: " + filesatu.getOriginalFilename();
      return ResponseEntity.status(HttpStatus.OK).body(new KtpMessageResponse(message));
    } catch (Exception e) {
      message = "Could not upload the file: " + filesatu.getOriginalFilename() + "!";
      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new KtpMessageResponse(message));
    }
  }

  @GetMapping("/upload_ktp/get/{appId}")
  public ResponseEntity<byte[]> getFile(@PathVariable String appId) {
    KtpUpload ktpUpload = ktpService.getFile(appId);
    String mimeType = ktpUpload.getType();

    return ResponseEntity.ok()
        .contentType(MediaType.parseMediaType(mimeType))
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + ktpUpload.getAppId() + ".jpg")
        .body(ktpUpload.getData());
  }

  @GetMapping("/upload_ktp/get/")
  public ResponseEntity<List<KtpFileResponse>> getListFiles() {
    List<KtpFileResponse> files = ktpService.getAllFiles().map(dbFile -> {
      String fileDownloadUri = ServletUriComponentsBuilder
          .fromCurrentContextPath()
          .path("/filesKtp/")
          .path(dbFile.getAppId())
          .toUriString();
      return new KtpFileResponse(
          dbFile.getAppId(),
          fileDownloadUri,
          dbFile.getType(),
          dbFile.getData().length);
    }).collect(Collectors.toList());
    return ResponseEntity.status(HttpStatus.OK).body(files);
  }

  @PutMapping("/update_ktp/put/")
  public ResponseEntity<KtpMessageResponse> updateFile(@RequestParam String appId,
  @RequestParam MultipartFile filesatu) {
    String message = "";
    try {
      ktpService.store(appId, filesatu);

      message = "Update file success: " + filesatu.getOriginalFilename();
      return ResponseEntity.status(HttpStatus.OK).body(new KtpMessageResponse(message));
    } catch (Exception e) {
      message = "Could not update the file: " + filesatu.getOriginalFilename() + "!";
      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new KtpMessageResponse(message));
    }
  }

}