package com.bmi.QrisDoc.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.MediaType;

import com.bmi.QrisDoc.service.LegalServices;
import com.bmi.QrisDoc.message.LegalFileResponse;
import com.bmi.QrisDoc.message.LegalMessageResponse;
import com.bmi.QrisDoc.entity.LegalUpload;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/mqris/eformdoc")
public class LegalController {

  @Autowired
  private LegalServices legalServices;

  @PostMapping("/upload_legal/post/")
  public ResponseEntity<LegalMessageResponse> uploadFile(@RequestParam String appId,
      @RequestParam MultipartFile filelima) {
    String message = "";
    try {
      legalServices.store(appId, filelima);

      message = "Upload file success: " + filelima.getOriginalFilename();
      return ResponseEntity.status(HttpStatus.OK).body(new LegalMessageResponse(message));
    } catch (Exception e) {
      message = "Could not upload the file: " + filelima.getOriginalFilename() + "!";
      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new LegalMessageResponse(message));
    }
  }

  @GetMapping("/upload_legal/get/{appId}")
  public ResponseEntity<byte[]> getFile(@PathVariable String appId) {
    LegalUpload legalUpload = legalServices.getFile(appId);
    String mimeType = legalUpload.getType();

    return ResponseEntity.ok()
        .contentType(MediaType.parseMediaType(mimeType))
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + legalUpload.getAppId() + ".pdf")
        .body(legalUpload.getData());
  }

  @GetMapping("/upload_legal/get/")
  public ResponseEntity<List<LegalFileResponse>> getListFiles() {
    List<LegalFileResponse> files = legalServices.getAllFiles().map(dbFile -> {
      String fileDownloadUri = ServletUriComponentsBuilder
          .fromCurrentContextPath()
          .path("/fileLegal/")
          .path(dbFile.getAppId())
          .toUriString();
      return new LegalFileResponse(
          dbFile.getAppId(),
          fileDownloadUri,
          dbFile.getType(),
          dbFile.getData().length);
    }).collect(Collectors.toList());
    return ResponseEntity.status(HttpStatus.OK).body(files);
  }

  @PutMapping("/update_legal/put/")
  public ResponseEntity<LegalMessageResponse> updateFile(@RequestParam String appId,
  @RequestParam MultipartFile filelima) {
    String message = "";
    try {
      legalServices.store(appId, filelima);
      message = "Update file success: " + filelima.getOriginalFilename();
      return ResponseEntity.status(HttpStatus.OK).body(new LegalMessageResponse(message));
    } catch (Exception e) {
      message = "Could not update the file: " + filelima.getOriginalFilename() + "!";
      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new LegalMessageResponse(message));
    }
  }
}