package com.bmi.QrisDoc.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.MediaType;

import com.bmi.QrisDoc.service.NpwpServices;
import com.bmi.QrisDoc.message.NpwpMessageResponse;
import com.bmi.QrisDoc.message.NpwpFileResponse;
import com.bmi.QrisDoc.entity.NpwpUpload;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/mqris/eformdoc")
public class NpwpController {

    @Autowired
    private NpwpServices npwpService;

    @PostMapping("/upload_npwp/post/")
    public ResponseEntity<NpwpMessageResponse> uploadFile(@RequestParam String appId,
            @RequestParam MultipartFile filedua) {
        String message = "";
        try {
            npwpService.store(appId, filedua);
            message = "Upload file success: " + filedua.getOriginalFilename();
            return ResponseEntity.status(HttpStatus.OK).body(new NpwpMessageResponse(message));
        } catch (Exception e) {
            message = "Could not upload the file: " + filedua.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new NpwpMessageResponse(message));
        }
    }

    @GetMapping("/upload_npwp/get/{appId}")
    public ResponseEntity<byte[]> getFile(@PathVariable String appId) {
        NpwpUpload npwpUpload = npwpService.getFile(appId);
        String mimeType = npwpUpload.getType();

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(mimeType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + npwpUpload.getAppId() + ".jpg")
                .body(npwpUpload.getData());
    }

    @GetMapping("/upload_npwp/get/")
    public ResponseEntity<List<NpwpFileResponse>> getListFiles() {
        List<NpwpFileResponse> files = npwpService.getAllFiles().map(dbFile -> {
            String fileDownloadUri = ServletUriComponentsBuilder
                    .fromCurrentContextPath()
                    .path("/filesNpwp/")
                    .path(dbFile.getAppId())
                    .toUriString();
            return new NpwpFileResponse(
                    dbFile.getAppId(),
                    fileDownloadUri,
                    dbFile.getType(),
                    dbFile.getData().length);
        }).collect(Collectors.toList());
        return ResponseEntity.status(HttpStatus.OK).body(files);
    }

    @PutMapping("/update_npwp/put/")
    public ResponseEntity<NpwpMessageResponse> updateFile(@RequestParam String appId,
            @RequestParam MultipartFile filedua) {
        String message = "";
        try {
            npwpService.store(appId, filedua);
            message = "Update file success: " + filedua.getOriginalFilename();
            return ResponseEntity.status(HttpStatus.OK).body(new NpwpMessageResponse(message));
        } catch (Exception e) {
            message = "Could not update the file: " + filedua.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new NpwpMessageResponse(message));
        }
    }

}