package com.bmi.QrisDoc.controller;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.MediaType;

import com.bmi.QrisDoc.service.QRcodeService;
import com.bmi.QrisDoc.message.QRcodeFileResponse;
import com.bmi.QrisDoc.message.QRcodeMessageResponse;
import com.bmi.QrisDoc.entity.QRcode;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/mqris/eformdoc")
public class QRcodeController {
    @Autowired
    private QRcodeService qrcodeService;

    @PostMapping("/upload_qr_code/post/")
    public ResponseEntity<QRcodeMessageResponse> uploadFile(@RequestParam String appId,
            @RequestParam MultipartFile fileenam) {
        String message = "";
        try {
            qrcodeService.store(appId, fileenam);

            message = "Upload file success: " + fileenam.getOriginalFilename();
            return ResponseEntity.status(HttpStatus.OK).body(new QRcodeMessageResponse(message));
        } catch (Exception e) {
            message = "Could not upload the file: " + fileenam.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new QRcodeMessageResponse(message));
        }
    }

    @GetMapping("/upload_qr_code/get/{appId}")
    public ResponseEntity<byte[]> downLoadSingleFiles(@PathVariable String appId) {
        QRcode qrcode = qrcodeService.getFile(appId);
        String mimeType = qrcode.getType();

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(mimeType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + qrcode.getAppId() + ".jpg")
                .body(qrcode.getData());
    }

    @GetMapping("/upload_qr_code/get/")
    public ResponseEntity<List<QRcodeFileResponse>> getListFiles() {
        List<QRcodeFileResponse> files = qrcodeService.getAllFiles().map(dbFile -> {
            String fileDownloadUri = ServletUriComponentsBuilder
                    .fromCurrentContextPath()
                    .path("/filesQRcode/")
                    .path(dbFile.getAppId())
                    .toUriString();
            return new QRcodeFileResponse(
                    dbFile.getAppId(),
                    fileDownloadUri,
                    dbFile.getType(),
                    dbFile.getData().length);
        }).collect(Collectors.toList());
        return ResponseEntity.status(HttpStatus.OK).body(files);
    }

    @PostMapping("/update_qr_code/put/")
    public ResponseEntity<QRcodeMessageResponse> updateFile(@RequestParam String appId,
            @RequestParam MultipartFile fileenam) {
        String message = "";
        try {
            qrcodeService.store(appId, fileenam);

            message = "Update file success: " + fileenam.getOriginalFilename();
            return ResponseEntity.status(HttpStatus.OK).body(new QRcodeMessageResponse(message));
        } catch (Exception e) {
            message = "Could not update the file: " + fileenam.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new QRcodeMessageResponse(message));
        }
    }
}