package com.bmi.QrisDoc.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Column;

@Entity
@Table(name = "legal_doc")

public class LegalUpload {
  @Id
  // @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "appId")
  private String appId;

  private String name;

  private String type;

  @Lob
  // @Type(type = "org.hibernate.type.ImageType")
  private byte[] data;

  public LegalUpload() {
  }

  public LegalUpload(String appId, String name, String type, byte[] data) {
    this.appId = appId;
    this.name = name;
    this.type = type;
    this.data = data;
  }

  public String getAppId() {
    return this.appId;
  }

  public void setAppId(String appId) {
    this.appId = appId;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getType() {
    return this.type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public byte[] getData() {
    return this.data;
  }

  public void setData(byte[] data) {
    this.data = data;
  }

}
