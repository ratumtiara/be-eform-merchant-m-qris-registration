package com.bmi.QrisDoc.message;

public class FotoTempat1MessageResponse {
    private String message;

    public FotoTempat1MessageResponse(String message) {
      this.message = message;
    }
  
    public String getMessage() {
      return message;
    }
  
    public void setMessage(String message) {
      this.message = message;
    } 
    
}
