package com.bmi.QrisDoc.message;

public class FotoTempat2MessageResponse {
    private String message;

    public FotoTempat2MessageResponse(String message) {
      this.message = message;
    }
  
    public String getMessage() {
      return message;
    }
  
    public void setMessage(String message) {
      this.message = message;
    } 
}
