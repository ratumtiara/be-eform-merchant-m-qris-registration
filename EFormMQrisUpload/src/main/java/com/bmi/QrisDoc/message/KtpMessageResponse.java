package com.bmi.QrisDoc.message;

public class KtpMessageResponse {
    private String message;

    public KtpMessageResponse(String message) {
      this.message = message;
    }
  
    public String getMessage() {
      return message;
    }
  
    public void setMessage(String message) {
      this.message = message;
    }   
}
