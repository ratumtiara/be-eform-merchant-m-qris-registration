package com.bmi.QrisDoc.message;

public class LegalFileResponse {
    private String appId;
    private String url;
    private String type;
    private long size;
  
    public LegalFileResponse(String appId, String url, String type, long size) {
      this.appId = appId;
      this.url = url;
      this.type = type;
      this.size = size;
    }
  
    public String getaApId() {
      return appId;
    }
  
    public void setAppId(String appId) {
      this.appId = appId;
    }
  
    public String getUrl() {
      return url;
    }
  
    public void setUrl(String url) {
      this.url = url;
    }
  
    public String getType() {
      return type;
    }
  
    public void setType(String type) {
      this.type = type;
    }
  
    public long getSize() {
      return size;
    }
  
    public void setSize(long size) {
      this.size = size;
    }
    
}
