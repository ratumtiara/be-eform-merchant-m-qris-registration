package com.bmi.QrisDoc.message;

public class LegalMessageResponse {
    private String message;

    public LegalMessageResponse(String message) {
      this.message = message;
    }
  
    public String getMessage() {
      return message;
    }
  
    public void setMessage(String message) {
      this.message = message;
    }   
    
}
