package com.bmi.QrisDoc.message;

public class NpwpMessageResponse {
    private String message;

    public NpwpMessageResponse(String message) {
      this.message = message;
    }
  
    public String getMessage() {
      return message;
    }
  
    public void setMessage(String message) {
      this.message = message;
    }
}
