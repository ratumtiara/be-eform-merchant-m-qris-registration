package com.bmi.QrisDoc.message;

public class QRcodeMessageResponse {
    private String message;

    public QRcodeMessageResponse(String message) {
      this.message = message;
    }
  
    public String getMessage() {
      return message;
    }
  
    public void setMessage(String message) {
      this.message = message;
    }
    
}
