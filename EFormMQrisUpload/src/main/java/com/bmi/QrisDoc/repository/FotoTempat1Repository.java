package com.bmi.QrisDoc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bmi.QrisDoc.entity.FotoTempat1;

@Repository
public interface FotoTempat1Repository extends JpaRepository <FotoTempat1, String>{
    
}
