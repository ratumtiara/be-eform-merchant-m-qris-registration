package com.bmi.QrisDoc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bmi.QrisDoc.entity.FotoTempat2;

@Repository
public interface FotoTempat2Repository extends JpaRepository <FotoTempat2, String>{
   
}
