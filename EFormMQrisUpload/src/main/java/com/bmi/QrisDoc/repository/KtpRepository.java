package com.bmi.QrisDoc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bmi.QrisDoc.entity.KtpUpload;

@Repository
public interface KtpRepository extends JpaRepository <KtpUpload, String>{
    
}
