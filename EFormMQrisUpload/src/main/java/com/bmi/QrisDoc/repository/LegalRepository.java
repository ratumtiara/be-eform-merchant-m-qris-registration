package com.bmi.QrisDoc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bmi.QrisDoc.entity.LegalUpload;

@Repository
public interface LegalRepository extends JpaRepository <LegalUpload, String>{
    
}
