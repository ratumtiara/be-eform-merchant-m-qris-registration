package com.bmi.QrisDoc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bmi.QrisDoc.entity.NpwpUpload;

@Repository
public interface NpwpRepository extends JpaRepository <NpwpUpload, String>{
    
}
