package com.bmi.QrisDoc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bmi.QrisDoc.entity.QRcode;

@Repository
public interface QRcodeRepository extends JpaRepository <QRcode, String> {
    
}
