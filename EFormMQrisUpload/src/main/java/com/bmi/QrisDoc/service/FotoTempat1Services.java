package com.bmi.QrisDoc.service;

import java.io.IOException;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.bmi.QrisDoc.entity.FotoTempat1;
import com.bmi.QrisDoc.repository.FotoTempat1Repository;

@Service

public class FotoTempat1Services {
    @Autowired
    private FotoTempat1Repository fotoTempat1Repository;

    public FotoTempat1 store(String appId, MultipartFile filetiga) throws IOException {

        String fileName = StringUtils.cleanPath(filetiga.getOriginalFilename());
        FotoTempat1 FotoTempat1 = new FotoTempat1(appId, fileName, filetiga.getContentType(), filetiga.getBytes());

        return fotoTempat1Repository.save(FotoTempat1);
    }

    public FotoTempat1 getFile(String appId) {
        return fotoTempat1Repository.findById(appId).get();
    }

    public Stream<FotoTempat1> getAllFiles() {
        return fotoTempat1Repository.findAll().stream();
    }
}
