package com.bmi.QrisDoc.service;

import java.io.IOException;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.bmi.QrisDoc.entity.FotoTempat2;
import com.bmi.QrisDoc.repository.FotoTempat2Repository;

@Service

public class FotoTempat2Services {
    @Autowired
    private FotoTempat2Repository fotoTempat2Repository;

    public FotoTempat2 store(String appId, MultipartFile fileempat) throws IOException {

        String fileName = StringUtils.cleanPath(fileempat.getOriginalFilename());
        FotoTempat2 FotoTempat2 = new FotoTempat2(appId, fileName, fileempat.getContentType(), fileempat.getBytes());

        return fotoTempat2Repository.save(FotoTempat2);
    }

    public FotoTempat2 getFile(String appId) {
        return fotoTempat2Repository.findById(appId).get();
    }

    public Stream<FotoTempat2> getAllFiles() {
        return fotoTempat2Repository.findAll().stream();
    }

}
