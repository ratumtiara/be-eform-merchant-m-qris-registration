package com.bmi.QrisDoc.service;

import java.io.IOException;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.bmi.QrisDoc.entity.KtpUpload;
import com.bmi.QrisDoc.repository.KtpRepository;

@Service
public class KtpServices {
    @Autowired
    private KtpRepository ktpRepository;

    public KtpUpload store(String appId, MultipartFile filesatu) throws IOException {

        String fileName = StringUtils.cleanPath(filesatu.getOriginalFilename());
        KtpUpload KtpUpload = new KtpUpload(appId, fileName, filesatu.getContentType(), filesatu.getBytes());

        return ktpRepository.save(KtpUpload);
    }

    public KtpUpload getFile(String appId) {
        return ktpRepository.findById(appId).get();
    }

    public Stream<KtpUpload> getAllFiles() {
        return ktpRepository.findAll().stream();
    }

}
