package com.bmi.QrisDoc.service;

import java.io.IOException;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.bmi.QrisDoc.entity.LegalUpload;
import com.bmi.QrisDoc.repository.LegalRepository;

@Service
public class LegalServices {
    @Autowired
    private LegalRepository legalRepository;

    public LegalUpload store(String appId, MultipartFile filelima) throws IOException {

        String fileName = StringUtils.cleanPath(filelima.getOriginalFilename());
        LegalUpload LegalUpload = new LegalUpload(appId, fileName, filelima.getContentType(), filelima.getBytes());

        return legalRepository.save(LegalUpload);
    }

    public LegalUpload getFile(String appId) {
        return legalRepository.findById(appId).get();
    }

    public Stream<LegalUpload> getAllFiles() {
        return legalRepository.findAll().stream();
    }

}
