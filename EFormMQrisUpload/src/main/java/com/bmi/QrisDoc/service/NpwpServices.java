package com.bmi.QrisDoc.service;

import java.io.IOException;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.bmi.QrisDoc.entity.NpwpUpload;
import com.bmi.QrisDoc.repository.NpwpRepository;

@Service
public class NpwpServices 
{
    @Autowired
    private NpwpRepository npwpRepository;

    public NpwpUpload store(String appId , MultipartFile filedua) throws IOException {

        String fileName = StringUtils.cleanPath(filedua.getOriginalFilename());
        NpwpUpload npwpUpload = new NpwpUpload(appId,fileName, filedua.getContentType(), filedua.getBytes());
        
        return npwpRepository.save(npwpUpload);

    }

    public NpwpUpload getFile(String appId){
        return npwpRepository.findById(appId).get();
    }

    public Stream<NpwpUpload> getAllFiles(){
        return npwpRepository.findAll().stream();
    }
    
}
