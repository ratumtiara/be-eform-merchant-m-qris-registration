package com.bmi.QrisDoc.service;

import java.io.IOException;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.bmi.QrisDoc.entity.QRcode;
import com.bmi.QrisDoc.repository.QRcodeRepository;

@Service
public class QRcodeService {
    @Autowired
    private QRcodeRepository qrcodeRepository;

    public QRcode store(String appId, MultipartFile fileenam) throws IOException {

        String fileName = StringUtils.cleanPath(fileenam.getOriginalFilename());
        QRcode qrcode = new QRcode(appId, fileName, fileenam.getContentType(), fileenam.getBytes());

        return qrcodeRepository.save(qrcode);
    }

    public QRcode getFile(String appId) {
        return qrcodeRepository.findById(appId).get();
    }

    public Stream<QRcode> getAllFiles() {
        return qrcodeRepository.findAll().stream();
    }

}
