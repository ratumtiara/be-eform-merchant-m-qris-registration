package com.bmi.QrisDoc.validation;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import com.bmi.QrisDoc.message.QRcodeMessageResponse;

@ControllerAdvice
public class QRcodeValidation {
  @ExceptionHandler(MaxUploadSizeExceededException.class)
  public ResponseEntity<QRcodeMessageResponse> handleMaxSizeException(MaxUploadSizeExceededException exc) {
    return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new QRcodeMessageResponse("File too large!"));
  }
}
