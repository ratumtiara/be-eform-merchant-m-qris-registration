package com.example.techmagister.sendingemail.controller;

import com.example.techmagister.sendingemail.model.MailModel;
import com.example.techmagister.sendingemail.model.MailModelRespond;
import com.example.techmagister.sendingemail.service.SendingEmailService;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.IOException;

@CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = { RequestMethod.GET,
        RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT })
@RestController
@RequestMapping("/mqris/mailservice")
public class SendEmailController {

    @Autowired
    private SendingEmailService sendingEmailService;

    @PostMapping(value = "/sendmail/")
    public ResponseEntity<?> restPostLoanRequest(@RequestBody MailModel mailModel) {

        try {
            sendingEmailService.sendEmail(mailModel);
            return ResponseEntity.ok().body(mailModel.toString());
        } catch (MessagingException e) {
            e.printStackTrace();
            return ResponseEntity.ok().body(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.ok().body(e.getMessage());
        } catch (TemplateException e) {
            e.printStackTrace();
            return ResponseEntity.ok().body(e.getMessage());
        }

    }

    @PostMapping(value = "/respond/")
    public ResponseEntity<?> restPostLoanRequest(@RequestBody MailModelRespond mailModel) {

        try {
            sendingEmailService.sendEmailRespond(mailModel);
            return ResponseEntity.ok().body(mailModel.toString());
        } catch (MessagingException e) {
            e.printStackTrace();
            return ResponseEntity.ok().body(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.ok().body(e.getMessage());
        } catch (TemplateException e) {
            e.printStackTrace();
            return ResponseEntity.ok().body(e.getMessage());
        }

    }
}