package com.example.techmagister.sendingemail.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Map;

@Data
@ToString
@AllArgsConstructor
public class MailModel {

    private String from;
    private String pemilik_email;
    private String appId;
    private String pemilik_handphone;
    private String pemilik_nama;
    private String usaha_bankterdekat;
    private String usaha_mdr;
    private String usaha_klasifikasi;
    private String usaha_nama50;
    private String alamat_kodepos;
    private Map<String, String> model;
}
