package com.example.techmagister.sendingemail.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Map;

@Data
@ToString
@AllArgsConstructor
public class MailModelRespond {

    private String from;
    private String pemilik_email;
    private String appId;
    private String respond_admin;
    private String respond_url;
    private Map<String, String> model;
}
