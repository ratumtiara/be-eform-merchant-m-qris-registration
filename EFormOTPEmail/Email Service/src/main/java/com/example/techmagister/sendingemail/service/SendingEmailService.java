package com.example.techmagister.sendingemail.service;

import com.example.techmagister.sendingemail.model.MailModel;
import com.example.techmagister.sendingemail.model.MailModelRespond;

import freemarker.template.TemplateException;

import javax.mail.MessagingException;
import java.io.IOException;

public interface SendingEmailService {

    void sendEmail(MailModel mailModel) throws MessagingException, IOException, TemplateException;
    void sendEmailRespond(MailModelRespond mailModel) throws MessagingException, IOException, TemplateException;
}
