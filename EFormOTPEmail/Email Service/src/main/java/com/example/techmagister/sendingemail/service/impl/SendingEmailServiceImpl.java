package com.example.techmagister.sendingemail.service.impl;

import com.example.techmagister.sendingemail.model.MailModel;
import com.example.techmagister.sendingemail.model.MailModelRespond;
import com.example.techmagister.sendingemail.service.SendingEmailService;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


@Service
public class SendingEmailServiceImpl implements SendingEmailService {

    private static Logger log = LoggerFactory.getLogger(SendingEmailServiceImpl.class);

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    @Qualifier("emailConfigBean")
    private Configuration emailConfig;

    @Override
    public void sendEmail(MailModel mailModel) throws MessagingException, IOException, TemplateException {

        Map model = new HashMap();
        model.put("pemilik_email", mailModel.getPemilik_email());
        model.put("appId", mailModel.getAppId());
        model.put("pemilik_handphone", mailModel.getPemilik_handphone());
        model.put("pemilik_nama",mailModel.getPemilik_nama());
        model.put("usaha_bankterdekat",mailModel.getUsaha_bankterdekat());
        model.put("usaha_mdr", mailModel.getUsaha_mdr());
        model.put("usaha_klasifikasi", mailModel.getUsaha_klasifikasi());
        model.put("usaha_nama50", mailModel.getUsaha_nama50());
        model.put("alamat_kodepos",mailModel.getAlamat_kodepos());

        /**
         * Add below line if you need to create a token to verification emails and uncomment line:32 in "email.ftl"
         * model.put("token",UUID.randomUUID().toString());
         */

        mailModel.setModel(model);


        log.info("Sending Email to: " + mailModel.getPemilik_email());


        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
        mimeMessageHelper.addInline("logo.png", new ClassPathResource("classpath:/techmagisterLogo.png"));

        Template template = emailConfig.getTemplate("email.ftl");
        String html = FreeMarkerTemplateUtils.processTemplateIntoString(template, mailModel.getModel());

        mimeMessageHelper.setTo(mailModel.getPemilik_email());
        mimeMessageHelper.setText(html, true);
        mimeMessageHelper.setSubject("MQRIS Registration - PT Bank Muamalat Indonesia Tbk");
        mimeMessageHelper.setFrom(new InternetAddress("mbdin.noreply@bankmuamalat.co.id"));


        emailSender.send(message);

    }

    @Override
    public void sendEmailRespond(MailModelRespond mailModel) throws MessagingException, IOException, TemplateException {
        Map model = new HashMap();
        model.put("pemilik_email", mailModel.getPemilik_email());
        model.put("appId", mailModel.getAppId());
        model.put("respond_admin", mailModel.getRespond_admin());
        model.put("respond_url", mailModel.getRespond_url());

        /**
         * Add below line if you need to create a token to verification emails and uncomment line:32 in "email.ftl"
         * model.put("token",UUID.randomUUID().toString());
         */

        mailModel.setModel(model);


        log.info("Sending Email to: " + mailModel.getPemilik_email());


        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
        mimeMessageHelper.addInline("logo.png", new ClassPathResource("classpath:/techmagisterLogo.png"));

        Template template = emailConfig.getTemplate("email_respond.ftl");
        String html = FreeMarkerTemplateUtils.processTemplateIntoString(template, mailModel.getModel());

        mimeMessageHelper.setTo(mailModel.getPemilik_email());
        mimeMessageHelper.setText(html, true);
        mimeMessageHelper.setSubject("[ADMIN RESPONSE] MQRIS Registration - PT Bank Muamalat Indonesia Tbk");
        mimeMessageHelper.setFrom(new InternetAddress("mbdin.noreply@bankmuamalat.co.id"));


        emailSender.send(message);
       
    }
}
