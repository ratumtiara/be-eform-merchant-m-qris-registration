<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.0/css/line.css">


        <style>
            body, html {
                margin: 0px;
            }
            .basicP {
                margin-top: 24px;
                margin-bottom: 0px;
                font-family: Poppins;
                font-style: normal;
                font-weight: 400;
                font-size: 14px;
                color: #000;
            }
            .containerOne {
                width: 90%;
                text-align: center;
                margin-left: auto;
                margin-right: auto;
            }

            .header {
                margin-top: 24px;
                margin-bottom: 24px;
                display: flex;
                justify-content: space-between;
                align-items: center;
            }

            .textOne {
                text-align: start;
                margin-bottom: 32px;
            }
            /* .textOne p {
                margin-top: 24px;
                margin-bottom: 0px;
                font-family: Poppins;
                font-style: normal;
                font-weight: 400;
                font-size: 14px;
                color: #000;

            } */
            .tableOne {
                text-align: start;
            }
            .tableOne p {
                font-family: Poppins;
                font-style: normal;
                font-weight: 400;
                font-size: 14px;
            }
            
            .row {
                display: flex;
                margin-bottom: 8px;
            }

            .tableTitle {
                margin-top: 12px;
                margin-bottom: 12px;
                color: #595862;

            }

            .tableContent {
                margin-top: 12px;
                margin-bottom: 12px;
                font-weight: 600 !important;
                color: #2F3137;
            }

            .button {
                background-color: #eee;
                border: none;
                color: #595862;
                padding: 12px 54px;
                /* text-align: center; */
                text-decoration: none;
                cursor: pointer;
                font-family: Poppins;
                font-style: normal;
                font-weight: 500;
                font-size: 12px;
                /* align-items: center; */
                border-radius: 4px;
                vertical-align: middle;
                box-shadow: 0px 10px 20px rgba(0, 0, 0, 0.04), 0px 2px 6px rgba(0, 0, 0, 0.04), 0px 0px 1px rgba(0, 0, 0, 0.04);
                }

            .button:hover {
                background-color: #e5e5e5;
            }
            
            footer {
                background-color: #500778;
                display: flex;
                margin-top: 40px;
                /* flex-direction: row; */
            }

            .textFooter {
                width: 60%;
                margin-left: auto;
                margin-right: auto;
            }

            .textFooterP {
                font-family: Poppins;
                font-style: normal;
                font-weight: normal;
                font-size: 12px;
                color: #fff;
            }

            .circleButton {
                border-radius: 50%;
                width: 30px;
                height: 30px;
                border: 1px solid #fff;
                background-color: #500778;
            }

        </style>
    <body>
        <div class="containerOne">
            <div class="header">

                <script>
                    document.getElementById("demo").innerHTML = "The full URL of this page is:<br>" + window.location.href.split('?').shift();
                </script>
                    <img src="https://1.bp.blogspot.com/-Wat3PE2wHsI/YKtz70BbQ5I/AAAAAAAAFCo/VPVg0HHAaV0vSqdf0yQwPobVkvX7CUcJwCLcBGAsYHQ/s1600/Logo%2BBank%2BMuamalat.png" alt="" width="132px" height="40px" style="margin-left: auto;" />
            </div>
            <!-- <hr style="border: 1px solid #9E9E9E"/> -->
            <div class="textOne">
                <p class="basicP" style="font-weight: 600; color: #363636;">Assalamu'alaikum Warrahmatullahi Wabarakatuh</p>
                <p class="basicP">Hi <span class="basicP" style="font-weight: 600; color: #363636; margin-left: 20;">${pemilik_nama},</span> </p>
                <p class="basicP" style="margin-top: 5px;">Alhamdulillah, Data Mercant Anda telah Kami terima dan akan Kami validasi lebih lanjut.</p>
            </div>
            <div class="tableOne">
                <p style="font-weight: 600; color: #500778; margin-bottom: 24px;">Detail Pengajuan</p>
                <hr style="margin: 0px; border: 1px solid #CAC8CE;"/>
                <div class="row">
                    <div style="width: 50%;">
                        <p class="tableTitle">Nomor Tiket</p>
                    </div>
                    <div style="width: 50%;">
                        <p class="tableContent">${appId}</p>
                    </div>
                </div>
                <hr style="margin: 0px; border: 1px solid #CAC8CE;"/>
                <div class="row">
                    <div style="width: 50%;">
                        <p class="tableTitle">Nama Pemilik Usaha</p>
                    </div>
                    <div style="width: 50%;">
                        <p class="tableContent">${pemilik_nama}</p>
                    </div>
                </div>
                <hr style="margin: 0px; border: 1px solid #CAC8CE;"/>
                <div class="row">
                    <div style="width: 50%;">
                        <p class="tableTitle">Nomor <span style="font-style: italic;">Handphone</span></p>
                    </div>
                    <div style="width: 50%;">
                        <p class="tableContent">${pemilik_handphone}</p>
                    </div>
                </div>
                <hr style="margin: 0px; border: 1px solid #CAC8CE;"/>
                <div class="row">
                    <div style="width: 50%;">
                        <p class="tableTitle">Email</p>
                    </div>
                    <div style="width: 50%;">
                        <p class="tableContent">${pemilik_email}</p>
                    </div>
                </div>
                <hr style="margin: 0px; border: 1px solid #CAC8CE;"/>
                <div class="row">
                    <div style="width: 50%;">
                        <p class="tableTitle">Nama Toko/Merchant/Intitusi</p>
                    </div>
                    <div style="width: 50%;">
                        <p class="tableContent">${usaha_nama50}</p>
                    </div>
                </div>
                <hr style="margin: 0px; border: 1px solid #CAC8CE;"/>
                <div class="row">
                    <div style="width: 50%;">
                        <p class="tableTitle">Klasifikasi Usaha</p>
                    </div>
                    <div style="width: 50%;">
                        <p class="tableContent">${usaha_klasifikasi}</p>
                    </div>
                </div>
                <hr style="margin: 0px; border: 1px solid #CAC8CE;"/>
                <div class="row">
                    <div style="width: 50%;">
                        <p class="tableTitle">MDR</p>
                    </div>
                    <div style="width: 50%;">
                        <p class="tableContent">${usaha_mdr}</p>
                    </div>
                </div>
                <hr style="margin: 0px; border: 1px solid #CAC8CE;"/>
                <div class="row">
                    <div style="width: 50%;">
                        <p class="tableTitle">Kode Pos Tempat Usaha</p>
                    </div>
                    <div style="width: 50%;">
                        <p class="tableContent">${alamat_kodepos}</p>
                    </div>
                </div>
                <hr style="margin: 0px; border: 1px solid #CAC8CE;"/>
                <div class="row">
                    <div style="width: 50%;">
                        <p class="tableTitle">Cabang Bank Muamalat</p>
                    </div>
                    <div style="width: 50%;">
                        <p class="tableContent">${usaha_bankterdekat}</p>
                    </div>
                </div>
            </div>
            <div class="textOne">
                <p class="basicP" style="color: #000; font-size: 14px !important;">
                    Untuk Informasi selanjutnya akan kami sampaikan melalui email atau nomor handphone yang tertera di atas.
                </p>
                <p class="basicP" style="color: #000">
                    Bila terdapat pertanyaan silahkan hubungi kami di Official Whatsapp Bank Muamalat.                
                </p>
            </div>
            <button class="button" href="https://wa.me/6281280651800">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#6cc24a" class="bi bi-whatsapp" viewBox="0 0 16 16" style="margin-right: 8px;">
                    <path d="M13.601 2.326A7.854 7.854 0 0 0 7.994 0C3.627 0 .068 3.558.064 7.926c0 1.399.366 2.76 1.057 3.965L0 16l4.204-1.102a7.933 7.933 0 0 0 3.79.965h.004c4.368 0 7.926-3.558 7.93-7.93A7.898 7.898 0 0 0 13.6 2.326zM7.994 14.521a6.573 6.573 0 0 1-3.356-.92l-.24-.144-2.494.654.666-2.433-.156-.251a6.56 6.56 0 0 1-1.007-3.505c0-3.626 2.957-6.584 6.591-6.584a6.56 6.56 0 0 1 4.66 1.931 6.557 6.557 0 0 1 1.928 4.66c-.004 3.639-2.961 6.592-6.592 6.592zm3.615-4.934c-.197-.099-1.17-.578-1.353-.646-.182-.065-.315-.099-.445.099-.133.197-.513.646-.627.775-.114.133-.232.148-.43.05-.197-.1-.836-.308-1.592-.985-.59-.525-.985-1.175-1.103-1.372-.114-.198-.011-.304.088-.403.087-.088.197-.232.296-.346.1-.114.133-.198.198-.33.065-.134.034-.248-.015-.347-.05-.099-.445-1.076-.612-1.47-.16-.389-.323-.335-.445-.34-.114-.007-.247-.007-.38-.007a.729.729 0 0 0-.529.247c-.182.198-.691.677-.691 1.654 0 .977.71 1.916.81 2.049.098.133 1.394 2.132 3.383 2.992.47.205.84.326 1.129.418.475.152.904.129 1.246.08.38-.058 1.171-.48 1.338-.943.164-.464.164-.86.114-.943-.049-.084-.182-.133-.38-.232z"/>
                </svg>
                <a href="https://wa.me/6281280651800">0812 8065 1800</a>
            </button>
            <button class="button" style="margin-left: 25px;"  href="mailto:info@bankmuamalat.co.id">
                <i class="uil uil-envelope" style="color: #6ECBE9;">
                </i>
                <a href="mailto:info@bankmuamalat.co.id">info@bankmuamalat.co.id</a>
            </button>

            <div class="textOne" style="margin-bottom: 0px;">
                <p class="basicP" style="color: #000; font-size: 14px !important;">
                    PT. Bank Muamalat Indonesia, Tbk.
                </p>
                <p class="basicP" style="color: #000; font-weight: 600;">
                    Wassalamu'alaikum Warrahmatullahi Wabarakatuh           
                </p>
            </div>
        </div>

    </body>
</html>
