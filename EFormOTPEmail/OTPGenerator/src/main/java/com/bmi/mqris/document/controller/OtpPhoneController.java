package com.bmi.mqris.document.controller;

import java.util.Random;

import com.bmi.mqris.document.entity.OtpPhoneEntity;
import com.bmi.mqris.document.repository.OtpPhoneRepository;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.CrossOrigin;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/mqris/otp")
public class OtpPhoneController {
    public static String getRandomNumberString() {
        // It will generate 6 digit random Number.
        // from 0 to 999999
        Random rnd = new Random();
        int number = rnd.nextInt(999999);
        // this will convert any number sequence into 6 character.
        // String sha256hex = DigestUtils.sha256Hex("");
        return String.format("%06d", number);
    }

    @Autowired
    private OtpPhoneRepository otpPhoneRepository;

    @PostMapping("/create/{noTelp}")
    public OtpPhoneEntity createDataPem(@RequestBody OtpPhoneEntity newOtpPhone, @PathVariable String noTelp) {
        String temp_otp = getRandomNumberString();
        newOtpPhone.setNoTelp(noTelp);
        newOtpPhone.setOtpUser(DigestUtils.sha256Hex(temp_otp));
        String uri = ("https://smsgw.sprintasia.net/api/muamalat/msg_otp.php?user=9dqVOpsd3tsF&password=txLdMdCxgDcc&sender=Muamalatp&GSM="
                + noTelp + "&SMSText=OTP E-FROM QRIS MERCHANT. OTP " + temp_otp
                + ". Silahkan masukan kode OTP untuk melanjutkan pendaftaran");
        RestTemplate send = new RestTemplate();
        String respond = send.getForObject(uri, String.class);
        System.out.println(newOtpPhone + " | before hash : " + temp_otp + "| Respon Sprint Asia : " + respond);

        return otpPhoneRepository.save(newOtpPhone);
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<OtpPhoneEntity>> getAllOtpPhone(@RequestParam(required = false) String ListOtpPhone) {
        try {

            List<OtpPhoneEntity> otpPhones = new ArrayList<OtpPhoneEntity>();

            otpPhoneRepository.findAll().forEach(otpPhones::add);
            return new ResponseEntity<>(otpPhones, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/getOnePhoneNumber/{noTelp}")
    public Optional<OtpPhoneEntity> getOneAppId(@PathVariable String noTelp) {

        return otpPhoneRepository.findByNoTelp(noTelp);
    }

    // @GetMapping("/validate/{hash}/{db}")
    // public Optional<OtpPhoneEntity> validate(@PathVariable String hash,
    // @PathVariable String db) {
    // // OtpPhoneEntity.get
    // return otpPhoneRepository.findByNoTelp(db);
    // }
}