package com.bmi.mqris.document.entity;

import javax.persistence.*;

@Entity
@Table(name = "app_otp_phonenumber")
public class OtpPhoneEntity {

    @Id
    @Column(name = "noTelp")
    private String noTelp;

    @Column(name = "otpUser")
    private String otpUser;

    public String getNoTelp() {
        return noTelp;
    }

    public void setNoTelp(String noTelp) {
        this.noTelp = noTelp;
    }

    public String getOtpUser() {
        return otpUser;
    }

    public void setOtpUser(String otpUser) {
        this.otpUser = otpUser;
    }

    public OtpPhoneEntity() {
    }

    public OtpPhoneEntity(String noTelp, String otpUser) {
        this.noTelp = noTelp;
        this.otpUser = otpUser;
    }

    @Override
    public String toString() {
        return "{" +
                "noTelp='" + getNoTelp() + "'" +
                ", otpUser='" + getOtpUser() + "'" +
                "}";
    }

}