package com.bmi.mqris.document.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

import com.bmi.mqris.document.entity.OtpPhoneEntity;

@Repository
public interface OtpPhoneRepository extends JpaRepository<OtpPhoneEntity, Long> {
    Optional<OtpPhoneEntity> findById(Long id);

    Optional<OtpPhoneEntity> findByNoTelp(String noTelp);
}
