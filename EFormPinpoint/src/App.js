import logo from './logo.svg';
//import './App.css';
import { useEffect } from 'react';
import { MapContainer, useMap, TileLayer, Marker, Popup } from 'react-leaflet';
import swal from 'sweetalert';
import L from 'leaflet';
import markerIconPng from "leaflet/dist/images/marker-icon.png"
import markerShadow from 'leaflet/dist/images/marker-shadow.png'
//import tileLayer from '../util/tileLayer';
import "leaflet/dist/leaflet.css";
import 'leaflet-control-geocoder/dist/Control.Geocoder.css';
import 'leaflet-control-geocoder/dist/Control.Geocoder.js';
import 'leaflet-control-geocoder'
import './geoip.js'

var pos = L.GeoIP.getPosition();
//console.log(pos)
var lokasi = [pos.lat, pos.lng]
const style = {
  height: '100vh',
  width: '100%'
  };
const center = lokasi; //DKI JAKARTA
const myIcon = L.icon({
  iconUrl: markerIconPng,
  shadowUrl : markerShadow
  // ...
});
const GetCoordinates = () => {
  const map = useMap();

  useEffect(() => {
    if (!map) return;
    const info = L.DomUtil.create('div', 'legend');
    const g = L.Control.geocoder().addTo(map);
    const positon = L.Control.extend({
      options: {
        position: 'bottomleft'
      },

      onAdd: function () {
        info.textContent = 'Sageri Fikri Ramadhan';
        return info;
      }
    })

    var  theMarker = {};
    map.on('click', (e) => {
      
      let {lat, lng} = e.latlng;
      var url = `https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=${lat}&lon=${lng}`;
      if (theMarker !== undefined) {
        map.removeLayer(theMarker);
      }
      theMarker = L.marker(e.latlng,{icon:myIcon}).addTo(map);
      info.textContent = lat+", "+lng
      console.log(info.textContent);
      var xhr = new XMLHttpRequest();
      xhr.open("GET", url, false);
      xhr.onload = function () {
        var status = xhr.status;
        var result = ""
        if (status == 200) {
            var nominatim_res = JSON.parse(xhr.responseText);
            result = nominatim_res.display_name;
            swal("Koordinat Berhasil Disalin",result,"info");
            //console.log(result+"Nominatim");
            navigator.clipboard.writeText(info.textContent)
        } else {
            // console.log("Get Addres failed because its XMLHttpRequest got this response: " + xhr.status);
            swal("Kesalahan Ditemukan",xhr.status,"warning");
        }
    };
    xhr.send();
    })

    map.addControl(new positon());
   
  }, [map])


  return null

}

const MapWrapper = () => {
  return (
    <MapContainer center={center} zoom={13} scrollWheelZoom={true} style={style}>
    <TileLayer
      attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
      url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
    />

    <GetCoordinates />
      {/* <Marker position={[-6.200000, 106.816666]}>
    <Popup>
      {info.textContent}
    </Popup>
  </Marker> */}
    </MapContainer>
  )
}

export default MapWrapper;
